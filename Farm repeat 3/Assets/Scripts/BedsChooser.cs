﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.Interfaces;

public class BedsChooser : MonoBehaviour, Chooser
{
    /*    public static BedsChooser instance;
        RectTransform rectTransform;
        [SerializeField] GameObject dig;
        [SerializeField] GameObject pour;
        [SerializeField] GameObject plants;
        Beds localBeds;
        private void Awake()
        {
            instance = this;
            rectTransform = GetComponent<RectTransform>();
            gameObject.SetActive(false);
        }
        public void Activate(object obj)
        {
            localBeds = (Beds)obj;
            rectTransform.transform.position = localBeds.transform.position;
            if (localBeds.state==0)
            {
                dig.SetActive(true);
                pour.SetActive(false);
                plants.SetActive(false);
            }
            if (localBeds.state == 1)
            {
                dig.SetActive(false);
                pour.SetActive(true);
                plants.SetActive(false);
            }
            if (localBeds.state == 2)
            {
                dig.SetActive(false);
                pour.SetActive(false);
                plants.SetActive(true);
            }
            StartCoroutine(Zoom());
        }

        public void SetType(int type)
        {
            localBeds.SetType(type);
            gameObject.SetActive(false);
        }
        public void ChangeState() 
        {
            localBeds.ChangeStateValue();
            gameObject.SetActive(false);
        }


        public IEnumerator Zoom()
        {
            transform.localScale = Vector3.zero;
            while (transform.localScale.x<1f)
            {
                transform.localScale += new Vector3(0.01f, 0.01f);
                yield return new WaitForSeconds(0.01f);
            }
        }*/

    public static BedsChooser instance;
    RectTransform rectTransform;
    Beds localBeds;
    [SerializeField] GameObject dig;
    [SerializeField] GameObject pour;
    [SerializeField] GameObject plants;
    private void Awake()
    {
        instance = this;
        rectTransform = GetComponent<RectTransform>();
        gameObject.SetActive(false);
    }
    public void Activate(object obj)
    {
        localBeds = (Beds)obj;
        rectTransform.transform.position = localBeds.transform.position;
        if (localBeds.state==0)
        {
            dig.SetActive(true);
            pour.SetActive(false);
            plants.SetActive(false);
        }
        else if (localBeds.state == 1)
        {
            dig.SetActive(false);
            pour.SetActive(true);
            plants.SetActive(false);
        }
        else if (localBeds.state == 2)
        {
            dig.SetActive(false);
            pour.SetActive(false);
            plants.SetActive(true);
        }
    }
    public void ChangeState() 
    {
        localBeds.ChangeStateValue();
        gameObject.SetActive(false);
    }
    public void SetType(int type)
    {
        localBeds.SetType(type);
        gameObject.SetActive(false);
    }

    public IEnumerator Zoom()
    {
        transform.localScale = Vector3.zero;
        while (transform.localScale.x<1f)
        {
            transform.localScale += new Vector3(0.01f, 0.01f);
            yield return new WaitForSeconds(0.01f);
        }
    }
}
