﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.Interfaces;
using System;
using UnityEngine.UI;

public class Beds : MonoBehaviour, Entity
{
    /*    int number;
        int idbeds;
        public int state;
        int type;
        DateTime endTime;
        bool isGrowning = false;
        Image image;
        [SerializeField] Sprite growingSprite;
        [SerializeField] Sprite[] stateSprites;
        [SerializeField] Sprite[] typeSprites;

        public void Awake()
        {
            image = GetComponent<Image>();
            number = Convert.ToInt32(name);
            GetInformation();
            SpriteUpdate();
            Timer();
        }

        public void GetInformation()
        {
            List<string> temp = SQL.GetBedsInformation("0", 0);
            idbeds = Convert.ToInt32(temp[0]);
            if (temp[1]=="")
            {
                state = 0;
                type = 0;
                endTime = DateTime.UtcNow;
                SQL.InsertInformation("landing", "idbeds,endTime", $"{idbeds},'{endTime}'");
            }
            else 
            {
                state = Convert.ToInt32(temp[1]);
                type = Convert.ToInt32(temp[2]);
                endTime = DateTime.Parse(temp[3]);
            }
        }

        public void SpriteUpdate()
        {
            if (isGrowning)
            {
                image.sprite = growingSprite;
            }
            else if (state<3)
            {
                image.sprite = stateSprites[state];
            }
            else if (type!=0 && state==3)
            {
                image.sprite = typeSprites[type];
            }
        }

        public void ChangeState()
        {
            if (!isGrowning)
            {
                if (state<3)
                {
                    //ChooseBeds.instance.gameObject.SetActive(true);
                    //ChooseBeds.instance.Activate();
                }
                else 
                {
                    Clear();
                }
            }
        }
        public void ChangeStateValue()
        {
            state += 1;

            SpriteUpdate();
        }public void SetType(int type)
        {
            this.type = type;
            int delta = 10 * type;
            endTime = DateTime.UtcNow + new TimeSpan(0, 0, delta);
            UpdateInformation();
            Timer();
        }

        public void Timer()
        {
            if ((endTime-DateTime.UtcNow).Seconds>0)
            {
                isGrowning = true;
                SpriteUpdate();
                Invoke("End", (endTime - DateTime.UtcNow).Seconds);
            }
        }

        public void End()
        {
            isGrowning = false;
            state = 3;
            SpriteUpdate();
        }

        public void Clear()
        {
            //GameManager.instance.ChangeValue(type);
            if (type==1)
            {
                //GameManager.instance.p1 += 1;
                //SQL.UpdateInformtion("storage",$"p1 = {GameManager.instance.p1},$"login = '{GameManager.instance.login}'");
            }
            if (type == 2)
            {
                //GameManager.instance.p2 += 1;
                //SQL.UpdateInformtion("storage",$"p2 = {GameManager.instance.p2},$"login = '{GameManager.instance.login}'");
            }
            if (type == 3)
            {
                //GameManager.instance.p3 += 1;
                //SQL.UpdateInformtion("storage",$"p3 = {GameManager.instance.p3},$"login = '{GameManager.instance.login}'");
            }
            type = 0;
            state = 0;
            endTime = DateTime.UtcNow;
            UpdateInformation();
            SpriteUpdate();
        }public void UpdateInformation()
        {
            SQL.UpdateInformation("landing", $"type = {type}", $"idbeds = {idbeds}");
            SQL.UpdateInformation("landing", $"state = {state}", $"idbeds = {idbeds}");
            SQL.UpdateInformation("landing", $"endTime = '{endTime}'", $"idbeds = {idbeds}");
        }*/
    int idbeds;
    int number;
    public int state;
    int type;
    DateTime endTime;

    bool isGrowning = false;

    Image image;

    [SerializeField] Sprite growningSprite;
    [SerializeField] Sprite[] stateSprites;
    [SerializeField] Sprite[] typeSprites;

    public void Awake()
    {
        number = Convert.ToInt32(name);
        image = GetComponent<Image>();
        GetInformation();
        SpriteUpdate();
        Timer();
    }

    public void ChangeState()
    {
        if (!isGrowning)
        {
            if (state < 3)
            {
                BedsChooser.instance.gameObject.SetActive(true);
                BedsChooser.instance.Activate(this);
            }
            else 
            {
                Clear();
            }
        }
    }

    public void ChangeStateValue()
    {
        state += 1;
        UpdateInformation();
        SpriteUpdate();
    }

    public void Clear()
    {
        //GameManager.instance.ChangeValue(type);
        if (type==1)
        {
            //GameManager.instance.p1+=1;
            //SQL.UpdateInformation("storage","p1 = {GameManager.instance.p1}", "login = 'GameManager.instance.login'");
        }
        if (type == 2)
        {
            //GameManager.instance.p2+=1;
            //SQL.UpdateInformation("storage","p2 = {GameManager.instance.p2}", "login = 'GameManager.instance.login'");
        }
        if (type == 3)
        {
            //GameManager.instance.p3+=1;
            //SQL.UpdateInformation("storage","p3 = {GameManager.instance.p3}", "login = 'GameManager.instance.login'");
        }
        state = 0;
        type = 0;
        endTime = DateTime.UtcNow;
        UpdateInformation();
        SpriteUpdate();
    }

    public void End()
    {
        isGrowning = false;
        state = 3;
        UpdateInformation();
        SpriteUpdate();
    }

    public void GetInformation()
    {
        List<string> temp = SQL.GetBedsInformation("login",number);
        idbeds = Convert.ToInt32(temp[0]);
        if (temp[1]=="")
        {
            state = 0;
            type = 0;
            endTime = DateTime.UtcNow;
            SQL.InsertInformation("landing", "idbeds,endTime", $"{idbeds},'{endTime}'");
        }
        else 
        {
            state = Convert.ToInt32(temp[1]);
            type = Convert.ToInt32(temp[2]);
            endTime = DateTime.Parse(temp[3]);
        }
    }

    public void SetType(int type)
    {
        this.type = type;
        int delta = 10 * type;
        endTime = DateTime.UtcNow + new TimeSpan(0, 0, delta);
        UpdateInformation();
        Timer();
    }

    public void SpriteUpdate()
    {
        if (isGrowning)
        {
            image.sprite = growningSprite;
        } else if (state <3)
        {
            image.sprite = stateSprites[state];
        }
        else if (type!=0)
        {
            image.sprite = typeSprites[type];
        }
    }

    public void Timer()
    {
        if ((endTime-DateTime.UtcNow).Seconds>0)
        {
            isGrowning = true;
            SpriteUpdate();
            Invoke("End", (endTime - DateTime.UtcNow).Seconds);
        }
    }

    public void UpdateInformation()
    {
        SQL.UpdateInformation("landing", $"state = {state}", $"idbeds = {idbeds}");
        SQL.UpdateInformation("landing", $"type = {type}", $"idbeds = {idbeds}");
        SQL.UpdateInformation("landing", $"endTime = {endTime}", $"idbeds = {idbeds}");
    }
}
