﻿using MySql.Data.MySqlClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using UnityEngine;


public static class SQL
{
    //public static MySqlConnection connection;
    /*    public static void Connect()
        {
            connection = new MySqlConnection(DBSettings.GetConnectionString());
        }
        public static void ExecuteNonQuery(string command)
        {
            connection.Open();
            try
            {
                MySqlCommand mySqlCommand = connection.CreateCommand();
                mySqlCommand.CommandText = command;
                mySqlCommand.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Debug.Log($"SQL - {e.Message}");
            }
            connection.Close();
        }
        static DbDataReader ReaderCommand(string selectData, string table, string filter) 
        {
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = $"SELECT {selectData} FROM {table} WHERE {filter}";
            DbDataReader dbDataReader = mySqlCommand.ExecuteReader();
            return dbDataReader;
        }
        static DbDataReader JoinReaderCommand(string selectData, string table,string joinTable, string filter, string where)
        {
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = $"SELECT {selectData} FROM {table} LEFT OUTER JOIN {joinTable} ON {filter} WHERE {where}";
            DbDataReader dbDataReader = mySqlCommand.ExecuteReader();
            return dbDataReader;
        }
        public static void InsertInformation(string table, string colums, string values) 
        {
            ExecuteNonQuery($"INSERT INTO {table} ({colums}) VALUES({values})");
        }
        public static void UpdateInformation(string table, string set, string where)
        {
            ExecuteNonQuery($"UPDATE {table} SET {set} WHERE {where}");
        }*/
    /*    public static MySqlConnection connection;
        public static void Connect() 
        {
            connection = new MySqlConnection(DBSettings.GetConnectionString());
        }
        public static void ExecuteNonQuery(string command) 
        {
            connection.Open();
            try
            {
                MySqlCommand mySqlCommand = connection.CreateCommand();
                mySqlCommand.CommandText = command;
                mySqlCommand.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Debug.Log($"SQL - {e.Message}");
            }
            connection.Close();
        }
        public static void InsertInformation(string table, string colums, string values) 
        {
            ExecuteNonQuery($"INSERT INTO {table} ({colums}) VALUES({values})");
        }
        public static void UpdateInformation(string table, string set, string where)
        {
            ExecuteNonQuery($"UPDATE {table} SET {set} WHERE = {where}");
        }
        public static DbDataReader ReaderCommand(string select, string table, string where) 
        {
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = $"SELECT {select} FROM {table} WHERE {where}";
            DbDataReader dbDataReader = mySqlCommand.ExecuteReader();
            return dbDataReader;
        }
        public static DbDataReader JoinReaderCommand(string select, string table, string joinTable, string fiter, string where)
        {
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = $"SELECT {select} FROM {table} LEFT OUTER JOIN {joinTable} ON {fiter} WHERE {where}";
            DbDataReader dbDataReader = mySqlCommand.ExecuteReader();
            return dbDataReader;
        }*/
    /*    public static MySqlConnection connection;
        public static void Connect() 
        {
            connection = new MySqlConnection(DBSettings.GetConnectionString());
        }
        static void ExecuteNonQuery(string command) 
        {
            connection.Open();
            try 
            {
                MySqlCommand mySqlCommand = connection.CreateCommand();
                mySqlCommand.CommandText = command;
                mySqlCommand.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Debug.Log($"SQL - {e.Message}");
            }
            connection.Close();
        }
        public static void InsertIntoInformation(string table, string colums, string values) 
        {
            ExecuteNonQuery($"INSERT INTO {table} ({colums}) VALUES({values})");
        }
        public static void UpdateInformation(string table, string set, string where)
        {
            ExecuteNonQuery($"UPDATE {table} SET {set}  WHERE {where}");
        }
        static DbDataReader ReaderCommand(string select, string table, string where) 
        {
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = $"SELECT {select} FROM {table} WHERE {where}";
            DbDataReader reader = mySqlCommand.ExecuteReader();
            return reader;
        }
        static DbDataReader JoinReaderCommand(string select, string table, string joinTable, string filter, string where)
        {
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = $"SELECT {select} FROM {table} LEFT OUTER JOIN {joinTable} ON {filter} WHERE {where}";
            DbDataReader reader = mySqlCommand.ExecuteReader();
            return reader;
        }*/
    public static MySqlConnection connection;
    public static void ExecuteNonQuery(string command) 
    {
        connection.Open();
        try 
        {
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = command;
            mySqlCommand.ExecuteNonQuery();
        }
        catch (Exception e)
        {
            Debug.Log($"SQL - {e.Message}");
        }
        connection.Close();
    }
    public static void InsertInformation(string table, string colums, string values) 
    {
        ExecuteNonQuery($"INSERT INTO {table} ({colums}) VALUES({values})");
    }
    public static void UpdateInformation(string table, string set, string where)
    {
        ExecuteNonQuery($"UPDATE {table} SET {set} WHERE {where}");
    }
    public static DbDataReader ReaderCommand(string select, string table, string where) 
    {
        MySqlCommand mySqlCommand = connection.CreateCommand();
        mySqlCommand.CommandText = $"SELECT {select} FROM {table} WHERE {where}";
        DbDataReader reader = mySqlCommand.ExecuteReader();
        return reader;
    }
    public static DbDataReader JoinReaderCommand(string select, string table, string joinTable, string on, string where)
    {
        MySqlCommand mySqlCommand = connection.CreateCommand();
        mySqlCommand.CommandText = $"SELECT {select} FROM {table} LEFT OUTER JOIN {joinTable} ON {on} WHERE {where}";
        DbDataReader reader = mySqlCommand.ExecuteReader();
        return reader;
    }
    public static List<string> GetUserInformation(string login, string password) 
    {
        List<string> result = new List<string>();
        connection.Open();
        DbDataReader reader = ReaderCommand("*", "users", $"login = '{login}' AND password = '{password}'");
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                result.Add(reader.GetValue(reader.GetOrdinal("username")).ToString());
            }
        }
        connection.Close();
        return result;
    }
    public static List<string> GetBedsInformation(string login, int number)
    {
        List<string> result = new List<string>();
        connection.Open();
        DbDataReader reader = JoinReaderCommand("*", "beds","landing", "beds.idbeds = landing.idbeds", $"login = '{login}' AND number = {number}");
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                result.Add(reader.GetValue(reader.GetOrdinal("idbeds")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("state")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("type")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("endTime")).ToString());
            }
        }
        connection.Close();
        return result;
    }
}
