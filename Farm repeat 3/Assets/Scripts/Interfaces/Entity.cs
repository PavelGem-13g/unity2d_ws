﻿namespace Assets.Scripts.Interfaces
{
    interface Entity
    {
        void Awake();
        /// <summary>
        /// получение информации
        /// </summary>
        void GetInformation();
        /// <summary>
        /// выгрузка текущей информации
        /// </summary>
        void UpdateInformation();
        /// <summary>
        /// обновление спрайта
        /// </summary>
        void SpriteUpdate();
        /// <summary>
        /// изменение состояния спрайта
        /// </summary>
        void ChangeState();
        /// <summary>
        /// изменение значения состояния
        /// </summary>
        void ChangeStateValue();
        /// <summary>
        /// установка типа 
        /// </summary>
        /// <param name="type"> тип</param>
        void SetType(int type);
        /// <summary>
        /// ожидание, "рост"
        /// </summary>
        void Timer();
        /// <summary>
        /// обработчик конца роста
        /// </summary>
        void End();
        /// <summary>
        /// очистка значений
        /// </summary>
        void Clear();
    }
}
