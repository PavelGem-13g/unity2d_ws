﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Interfaces
{
    interface Chooser
    {
        /// <summary>
        /// Установка типа
        /// </summary>
        /// <param name="type"></param>
        void SetType(int type);
        /// <summary>
        /// Активация окна
        /// </summary>
        void Activate(object obj);
        /// <summary>
        /// Анимация увеличения окна
        /// </summary>
        IEnumerator Zoom();
    }
}
