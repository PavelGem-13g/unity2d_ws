﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSCR : MonoBehaviour
{
    public void LoadMyScene(string scene) 
    {
        SceneManager.LoadScene(scene);
    }
    public void Quit() 
    {
        Application.Quit();
    }
    public void Restart() 
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
