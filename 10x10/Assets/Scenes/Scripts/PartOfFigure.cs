﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartOfFigure : MonoBehaviour
{
    public bool CheckOnGrid() 
    {

        if (transform.position.x < 0.3 && transform.position.x > -9.3 &&
    transform.position.y < 5 && transform.position.y >= -5)
        {
            Vector3 delta = new Vector3(-9, -5);
            return true;

        }
        else return false;
    }
    public void KillParents() 
    {
        transform.parent = transform.parent.parent;
    }
}
