﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditPlayerPrefs : MonoBehaviour
{
    public void Edit(/*string name, */int value) 
    {
        PlayerPrefs.SetInt("timeSet", value);
        UpdateColors();
    }
    public void UpdateColors() 
    {
        CellSCR[] cellSCRs = FindObjectsOfType<CellSCR>();
        DragAndDrop[] dragAndDrops = FindObjectsOfType<DragAndDrop>();
        BackgroundSCR backgroundSCR = FindObjectOfType<BackgroundSCR>();
        foreach (var item in cellSCRs)
        {
            item.UpdateColor();
        }
        foreach (var item in dragAndDrops)
        {
            item.UpdateColor();
        }
        backgroundSCR.UpdateColor();
    }
}
