﻿using System;
using System.Collections;
using UnityEngine;

public class DragAndDrop : MonoBehaviour
{
    public Fragments_SCR fragments;
    SpriteRenderer sp;

    private void Start()
    {
        sp = GetComponent<SpriteRenderer>();
        fragments = FindObjectOfType<Fragments_SCR>();
        UpdateColor();
    }
    /*    private void OnMouseDrag()
        {
            if (isDragable)
            {
                Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                pos.z = -1;
                transform.position = pos;

            }
        }*/
    /*    private void OnMouseUp()
        {
            if (isDragable)
            {
                transform.position = new Vector3((float)Math.Round(transform.position.x), (float)Math.Round(transform.position.y), 0);

                if (transform.position.x < 0.3 && transform.position.x > -9.3 &&
                    transform.position.y < 5 && transform.position.y >= -5)
                {
                    fragments.num--;
                    isDragable = false;
                    //PlayerPrefs.SetInt("current", PlayerPrefs.GetInt("current") + 2);
                    //Check();
                    //FindObjectOfType<Score>().UpdateResults();
                }
            }
        }*/
    public void UpdateGrid()
    {
        if (transform.position.x < 0.3 && transform.position.x > -9.3 &&
        transform.position.y < 5 && transform.position.y >= -5)
        {
            Vector3 delta = new Vector3(-9, -5);
            fragments.gameObjects[Convert.ToInt32(transform.position.x - delta.x), Convert.ToInt32(transform.position.y - delta.y)] += 1;

        }
        /*        Vector3 delta = new Vector3(-9, -5);

                //устновка флагов
                foreach (var item in fragments.fragments)
                {
                    for (int i = 0; i < fragments.gameObjects.GetLength(0); i++)
                    {
                        for (int j = 0; j < fragments.gameObjects.GetLength(1); j++)
                        {
                            if (item.transform.position.x - delta.x == i &&
                                item.transform.position.y - delta.y == j)
                            {
                                fragments.gameObjects[i, j] += 1;
                                isChecked = true;
                                Debug.Log(i+" "+j);
                            }
                        }
                    }
                }*/
    }
    public void Destroy()
    {
        StartCoroutine(DestroyCoroutine());
/*        SpriteRenderer sp = GetComponent<SpriteRenderer>();
        while (sp.color.a > 1)
        {
            sp.color = new Color(sp.color.r, sp.color.g, sp.color.b, sp.color.a - 1 * Time.deltaTime);
        }
        //Destroy(gameObject);
        Destroy(gameObject);*/
    }
    IEnumerator DestroyCoroutine()
    {
        SpriteRenderer sp = GetComponent<SpriteRenderer>();
        while (sp.color.a > 1)
        {
            sp.color = new Color(sp.color.r, sp.color.g, sp.color.b, sp.color.a - 1 * Time.deltaTime);
            Debug.Log("да, я здесь");
            yield return null;
        }
        Destroy(gameObject);
    }
    public void UpdateColor()
    {
        if (PlayerPrefs.GetInt("timeSet") == 0)
        {
            sp.color = Color.white;
        }
        else if (PlayerPrefs.GetInt("timeSet") == 1)
        {
            sp.color = Color.blue;
        }
        else if (PlayerPrefs.GetInt("timeSet") == 2)
        {
            sp.color = Color.black;
        }
        /*        if (dedoutside)
                {
                    SpriteRenderer sp = GetComponent<SpriteRenderer>();
                    if (sp.color.a > 1)
                    {
                        sp.color = new Color(sp.color.r, sp.color.g, sp.color.b, sp.color.a -Time.deltaTime*0.01f);
                    }
                    else
                    {
                        Destroy(gameObject);
                    }
                }*/
        //if()
    }

}
