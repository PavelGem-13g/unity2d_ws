﻿using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] GameObject cell;
    public void Start()
    {
        PlayerPrefs.SetInt("current", 0);
        SpawnMap();
    }
    public void SpawnMap()
    {
        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                Vector3 pos = new Vector3(i, j, 0);
                GameObject s = Instantiate(cell, pos + transform.position, Quaternion.identity, gameObject.transform);
                s.GetComponent<SpriteRenderer>().sortingOrder = -1;
                s.name = i.ToString() + " " + j.ToString();
            }
        }
    }
}
