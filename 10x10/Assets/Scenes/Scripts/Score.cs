﻿using TMPro;
using UnityEngine;

public class Score : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI best;
    [SerializeField] TextMeshProUGUI current;
    [SerializeField] int maxScore;
    [SerializeField] GameObject end;
    [SerializeField] GameObject[] gameMenu;
    public void Start()
    {
        UpdateResults();
    }
    public void UpdateResults()
    {
        if (PlayerPrefs.GetInt("current") > maxScore)
        {
            end.SetActive(true);
            foreach (var item in gameMenu)
            {
                item.SetActive(false);
            }
        }
        else
        {
            if (PlayerPrefs.GetInt("current") > PlayerPrefs.GetInt("best"))
            {
                PlayerPrefs.SetInt("best", PlayerPrefs.GetInt("current"));
            }
        }

        best.text = "Best result:"+PlayerPrefs.GetInt("best").ToString();
        current.text = "Current result:"+PlayerPrefs.GetInt("current").ToString();
    }
}
