﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Fragments_SCR : MonoBehaviour
{
    [SerializeField] GameObject[] fragment;
    public int num = 0;
    public List<GameObject> fragments = new List<GameObject>();
    public int[,] gameObjects = new int[10, 10];
    EditPlayerPrefs editPlayerPrefs;
    private void Start()
    {
        editPlayerPrefs = FindObjectOfType<EditPlayerPrefs>();
    }
    public void Spawn()
    {
        fragments.Add(Instantiate(fragment[UnityEngine.Random.Range(0, fragment.Length-1)], transform.position + new Vector3(UnityEngine.Random.Range(-3f, 1f), UnityEngine.Random.Range(-3f, 3f)), Quaternion.identity, gameObject.transform));
        //editPlayerPrefs.UpdateColors();
    }
    public void Update()
    {
        while (num < 3)
        {
            Spawn();
            num++;
        }

    }
}
