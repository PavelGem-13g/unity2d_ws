﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundSCR : MonoBehaviour
{
    Camera camera;
    private void Start()
    {
        camera = GetComponent<Camera>();
        UpdateColor();
    }
    public void UpdateColor()
    {
        if (PlayerPrefs.GetInt("timeSet") == 0)
        {
            camera.backgroundColor = new Color(220, 224, 203);
        }
        else if (PlayerPrefs.GetInt("timeSet") == 1)
        {
            camera.backgroundColor = Color.black;
        }
        else if (PlayerPrefs.GetInt("timeSet") == 2)
        {
            camera.backgroundColor = Color.cyan;
        }
    }
}
