﻿using UnityEngine;

public class CellSCR : MonoBehaviour
{
    SpriteRenderer sp;

    private void Start()
    {
        sp = GetComponent<SpriteRenderer>();
        UpdateColor();
    }
    public void UpdateColor()
    {
        if (PlayerPrefs.GetInt("timeSet") == 0)
        {
            sp.color = Color.red;
        }
        else if (PlayerPrefs.GetInt("timeSet") == 1)
        {
            sp.color = Color.yellow;
        }
        else if (PlayerPrefs.GetInt("timeSet") == 2)
        {
            sp.color = Color.gray;
        }
    }
}
