﻿using System.Collections;
using UnityEngine;

public class FragmentSCR : MonoBehaviour
{
    public bool isFirst;
    ArrowSCR arrow;
    MoveArrowSCR moveArrow;

    private void Awake()
    {
        arrow = GetComponentInParent<ArrowSCR>();
        moveArrow = GetComponentInParent<MoveArrowSCR>();
    }
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(GetDown());
        transform.parent = transform.parent.parent;
        Vector2Int myPosition = MyPositinInArray();
        if (isFirst)
        {
            arrow.positions[myPosition.x, myPosition.y] = 1;
        }
        else
        {
            arrow.positions[myPosition.x, myPosition.y] = 2;
        }
    }
    public Vector2Int MyPositinInArray()
    {
        Vector3 delta = new Vector2(-9, -5);
        //TODO
        if (transform.position.y >= -5)
        {//transform.position < new Vector3(0, 0, 0)
            return new Vector2Int(Mathf.RoundToInt(transform.position.x - delta.x), Mathf.RoundToInt(transform.position.y - delta.y));
        }
        else
        {
            return new Vector2Int(10, 10);
        }
    }
    public void UpdatePosition()
    {
        StartCoroutine(GetDown());
    }
    IEnumerator GetDown()
    {
        //isStay = false;
        moveArrow.isActive = false;
        //Vector2Int myPosition = MyPositinInArray();
        while (MyPositinInArray().y > 0 && arrow.positions[MyPositinInArray().x, MyPositinInArray().y - 1] == 0)
        {
            yield return new WaitForSeconds(0.25f);
            transform.position = new Vector3(transform.position.x, transform.position.y - 1);
            arrow.positions[MyPositinInArray().x, MyPositinInArray().y + 1] = 0;
            //myPosition = MyPositinInArray();
            if (isFirst)
            {
                arrow.positions[MyPositinInArray().x, MyPositinInArray().y] = 1;
            }
            else
            {
                arrow.positions[MyPositinInArray().x, MyPositinInArray().y] = 2;
            }
        }
        moveArrow.isActive = true;
        
        // = true;
        //arrow.ClearGrid();
    }
}
