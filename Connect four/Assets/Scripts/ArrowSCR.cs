﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowSCR : MonoBehaviour
{
    bool isFirst;
    BallSCR ball;
    public GameObject firstPlayer;
    public GameObject secondPlayer;
    List<FragmentSCR> fragments;
    public int[,] positions;
    public ScoreSCR firstScore;
    public ScoreSCR secondScore;
    MoveArrowSCR moveArrow;

    private void Awake()
    {
        positions = new int[10, 10];
        fragments = new List<FragmentSCR>();
        ball = GetComponentInChildren<BallSCR>();
        moveArrow = GetComponent<MoveArrowSCR>();
    }
    private void Start()
    {
        isFirst = true;
        ball.ChangeColor(isFirst);
        StartCoroutine(UpdateGrid());
    }
    IEnumerator UpdateGrid()
    {
        while (true)
        {
            if (moveArrow.isActive)
            {
                ClearGrid();
            }
            yield return new WaitForSeconds(0.1f);
        }
    }
    public void NewBall()
    {
        if (positions[Mathf.RoundToInt(transform.position.x) + 9, positions.GetLength(1) - 1] == 0)
        {
            GameObject temp;
            isFirst = !isFirst;
            ball.ChangeColor(isFirst);
            if (isFirst)
            {
                temp = Instantiate(firstPlayer, transform.position + new Vector3(0, -1), transform.rotation, transform);
            }
            else
            {
                temp = Instantiate(secondPlayer, transform.position + new Vector3(0, -1), transform.rotation, transform);
            }
            temp.GetComponent<FragmentSCR>().isFirst = !isFirst;
            fragments.Add(temp.GetComponent<FragmentSCR>());
        }
    }
    public void ClearGrid()
    {
        for (int i = 0; i < positions.GetLength(0); i++)
        {
            for (int j = 0; j < positions.GetLength(1); j++)
            {
                if (positions[i, j] != 0)
                {
                    int player = positions[i, j];
                    //int toRight = ToRight(i, j);
                    if (ToRight(i, j) > 3)
                    {
                        //Debug.Log("ToRight " + toRight);
                        ToRightClear(i, j, ToRight(i, j));
                        ScoreUp(ToRightDown(i, j), player);
                    }    
                    
                    //int toDown = ToDown(i, j);
                    if (ToDown(i, j) > 3)
                    {
                        ToDownClear(i, j, ToDown(i, j));
                        ScoreUp(ToDown(i, j), player);
                    }

                    //int toRightDown = ToRightDown(i, j);
                    if (ToRightDown(i, j) > 3)
                    {
                        ToRightDownClear(i, j, ToRightDown(i, j));
                        ScoreUp(ToRightDown(i, j), player);
                    }

                    //int toLeftDown = ToLeftDown(i, j);
                    if (ToLeftDown(i, j) > 3)
                    {
                        ToLeftDownClear(i, j, ToLeftDown(i, j));
                        ScoreUp(ToLeftDown(i, j), player);
                    }
                    DeleteFragments();
                    UpdateFragments();

                }
            }
        }
    }

    void ScoreUp(int delta, int player)
    {
        if (player == 1)
        {
            firstScore.ScoreUp(delta);
        }
        if (player == 2)
        {
            secondScore.ScoreUp(delta);
        }
    }
    void DeleteFragments()
    {
        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                if (positions[i, j] == 0)
                {
                    for (int x = fragments.Count - 1; x >=0 ; x--)
                    {
                        if (fragments[x].MyPositinInArray().x == i && fragments[x].MyPositinInArray().y == j)
                        {
                            fragments.Remove(fragments[x]);
                            break;
                        }
                    }
                }
            }
        }
    }
    void UpdateFragments()
    {
        foreach (var item in fragments)
        {
            item.UpdatePosition();
        }
    }
    int ToRight(int i, int j)
    {
        int result = 0;
        while (i + 1 < positions.GetLength(0) && positions[i, j] == positions[i + 1, j])
        {
            result++;
            i++;
        }
        return result+1;
    }
    void ToRightClear(int i, int j, int k)
    {
        for (int f = 0; f < k; f++)
        {
            positions[i, j] = 0;
            i++;
        }
    }
    int ToDown(int i, int j)
    {
        int result = 0;
        while (j - 1 > 0 && positions[i, j] == positions[i, j - 1])
        {
            result++;
            j--;
        }
        return result+1;
    }
    void ToDownClear(int i, int j, int k)
    {
        for (int f = 0; f < k; f++)
        {
            positions[i, j] = 0;
            j--;
        }
    }
    int ToRightDown(int i, int j)
    {
        int result = 0;
        while (i + 1 < positions.GetLength(0) && j - 1 > 0 && positions[i, j] == positions[i + 1, j - 1])
        {
            result++;

        }
        return result+1;
    }
    void ToRightDownClear(int i, int j, int k)
    {
        for (int f = 0; f < k; f++)
        {
            positions[i, j] = 0;
            i++;
            j--;
        }
    }
    int ToLeftDown(int i, int j)
    {
        int result = 0;

        while (-1 > i && j - 1 > 0 && positions[i, j] == positions[i - 1, j - 1])
        {
            result++;
            i--;
            j--;
        }
        return result+1;
    }
    void ToLeftDownClear(int i, int j, int k)
    {
        for (int f = 0; f < k; f++)
        {
            positions[i, j] = 0;
            i--;
            j--;
        }
    }
}
