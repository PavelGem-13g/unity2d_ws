﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreSCR : MonoBehaviour
{
    TextMeshProUGUI text;
    public int score;
    void Awake() 
    {
        text = GetComponent<TextMeshProUGUI>();
        score = 0;
    }
    public void Start() 
    {
        ScoreUp(0); 
    }
    public void ScoreUp(int delta) 
    {
        score += delta;
        text.text = score.ToString();
    }

}
