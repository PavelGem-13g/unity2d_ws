﻿using UnityEngine;

public class MoveArrowSCR : MonoBehaviour
{
    Vector3[] positions = {
        new Vector3(0,5), new Vector3(-1,5),
        new Vector3(-2, 5), new Vector3(-3, 5),
        new Vector3(-4, 5), new Vector3(-5, 5),
        new Vector3(-6, 5), new Vector3(-7, 5),
        new Vector3(-8, 5), new Vector3(-9, 5) };
    int localPosition = 0;
    ArrowSCR arrowSCR;
    public bool isActive;
    private void Awake()
    {
        arrowSCR = GetComponent<ArrowSCR>();
        isActive = true;
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            localPosition++;
            if (localPosition > 9)
            {
                localPosition = 9;
            }
            UpdatePosition();
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            localPosition--;
            if (localPosition < 0)
            {
                localPosition = 0;
            }
            UpdatePosition();
        }
        if (isActive && Input.GetKeyDown(KeyCode.Space))
        {
            arrowSCR.NewBall();
        }

    }
    void UpdatePosition()
    {
        transform.position = positions[localPosition];
    }
}

