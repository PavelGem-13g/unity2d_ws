﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSCR : MonoBehaviour
{
    public Color colorOne;
    public Color colorTwo;
    SpriteRenderer sp;
    private void Awake()
    {
        sp = GetComponent<SpriteRenderer>();
    }
    void SetColor1() 
    {
        sp.color = colorOne;
    }
    void SetColor2() 
    {
        sp.color = colorTwo;
    }
    public void ChangeColor(bool isFirst) 
    {
        if (isFirst)
        {
            SetColor1();
        }
        else
        {
            SetColor2();
        }
    }

}
