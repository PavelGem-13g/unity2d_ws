﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridSCR : MonoBehaviour
{
    public GameObject sample;
    void Start()
    {
        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                Instantiate(sample, transform.position + new Vector3(i, j), transform.rotation, transform);
            }
        }
    }
}
