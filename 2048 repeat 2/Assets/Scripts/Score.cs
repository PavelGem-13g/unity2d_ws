﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public static Score instance;

    public Text score;
    public Text bestScore;
    private void Awake()
    {
        instance = this; 
        PlayerPrefs.SetInt("score",0);
        ScoreUp(0);
    }
    public void ScoreUp(int delta) 
    {
        PlayerPrefs.SetInt("score", PlayerPrefs.GetInt("score")+delta);
        if (PlayerPrefs.GetInt("score")> PlayerPrefs.GetInt("bestscore")) 
        {
            PlayerPrefs.SetInt("bestscore", PlayerPrefs.GetInt("score"));
        }
        bestScore.text = "Best score:" + PlayerPrefs.GetInt("bestscore");
        score.text = "Score:" + PlayerPrefs.GetInt("score");
    }
}
