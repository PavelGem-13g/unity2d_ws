﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cell : MonoBehaviour
{
    public int value;
    public Text text; 
    private void Awake()
    {
        value = UnityEngine.Random.Range(0,2);
    }
    private void Start()
    {
        StartCoroutine(AddingMe());
        ChangeValue();
    }
    public void ChangeValue() 
    {
        Score.instance.ScoreUp(Convert.ToInt32(Mathf.Pow(2, value)));
        value += 1;
        text.text = Convert.ToInt32(Mathf.Pow(2, value)).ToString();
    }
    IEnumerator AddingMe() 
    {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.color = new Color(spriteRenderer.color.a, spriteRenderer.color.g, spriteRenderer.color.b,0);
        while (spriteRenderer.color.a<1f)
        {
            spriteRenderer.color = new Color(spriteRenderer.color.a, spriteRenderer.color.g, spriteRenderer.color.b, spriteRenderer.color.a + 0.01f);
            yield return new WaitForSeconds(0.01f);
        }
    }
}
