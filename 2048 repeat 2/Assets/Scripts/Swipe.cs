﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swipe : MonoBehaviour
{
    Vector3 firstPosition;
    Vector3 secondPosition;
    void Update()
    {
        if (Input.GetMouseButtonDown(0)) 
        {
            firstPosition = Input.mousePosition;
        }
        if (Input.GetMouseButtonUp(0)) 
        {
            secondPosition = Input.mousePosition;
            Move();
        }
    }
    void Move() 
    {
        Vector3 swipe = secondPosition - firstPosition;

        if (Mathf.Abs(swipe.x)>Mathf.Abs(swipe.y))
        {
            if (swipe.x > 0) 
            {
                StartCoroutine(Controller.instance.ToRight());
            }
            if (swipe.x < 0) 
            {
                StartCoroutine(Controller.instance.ToLeft());
            }
        }
        else 
        {
            if (swipe.y > 0) 
            {
                StartCoroutine(Controller.instance.ToTop());
            }
            if (swipe.y < 0) 
            {
                StartCoroutine(Controller.instance.ToDown());
            }
        }
        if (swipe.magnitude > 0) 
        {
            Controller.instance.CreateNewCell();
        }
    }
}
