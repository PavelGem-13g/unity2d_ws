﻿using System.Collections;
using UnityEngine;

public class Controller : MonoBehaviour
{
    public static Controller instance;
    public GridCell[,] gridCells;
    public GameObject cellPrefab;
    public GameObject gridCellPrefab;
    private void Awake()
    {
        instance = this;
        gridCells = new GridCell[7, 7];
        for (int i = 0; i < 7; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                gridCells[i, j] = Instantiate(gridCellPrefab, new Vector3(i, j), Quaternion.identity, transform).GetComponent<GridCell>();
                if (i == 0 || j == 0 || i == 6 || j == 6)
                {
                    gridCells[i, j].gameObject.SetActive(false);
                }
            }
        }
        CreateNewCell();
    }
    public void CreateNewCell()
    {
        if (CheckGameOver())
        {
            int i = Random.Range(1, 6), j = Random.Range(1, 6);
            while (gridCells[i, j].cell != null)
            {
                i = Random.Range(1, 6);
                j = Random.Range(1, 6);
            }
            gridCells[i, j].cell = Instantiate(cellPrefab, Vector3.zero, Quaternion.identity, gridCells[i, j].transform).GetComponent<Cell>();
            gridCells[i, j].cell.transform.localPosition = Vector3.zero;
        }
        else 
        {
            StartCoroutine(DestroyAll());
        }
    }
    public IEnumerator DestroyAll() 
    {
        for (int i = 1; i < 6; i++)
        {
            for (int j = 1; j < 6; j++)
            {
                Destroy(gridCells[i, j].cell.gameObject);
                yield return new WaitForSeconds(0.01f);
            }
        }
        CreateNewCell();
    }
    public bool CheckGameOver() 
    {
        int k = 0;
        for (int i = 1; i < 6; i++)
        {
            for (int j = 1; j < 6; j++)
            {
                if (gridCells[i, j].cell == null)
                {
                    k++;
                }
            }
        }
        return k > 0;
    }
    public IEnumerator ToRight()
    {
        for (int k = 0; k < 5; k++)
        {
            for (int i = 4; i >= 0; i--)
            {
                for (int j = 1; j < 6; j++)
                {
                    if (gridCells[i, j].cell != null && gridCells[i + 1, j].cell == null)
                    {
                        gridCells[i, j].cell.gameObject.transform.parent = gridCells[i + 1, j].transform;
                        gridCells[i, j].cell.transform.localPosition = Vector3.zero;
                        gridCells[i + 1, j].cell = gridCells[i, j].cell;
                        gridCells[i, j].cell = null;
                    }
                    if (gridCells[i, j].cell != null && gridCells[i + 1, j].cell != null &&
                        gridCells[i, j].cell.value == gridCells[i + 1, j].cell.value)
                    {
                        Destroy(gridCells[i, j].cell.gameObject);
                        gridCells[i, j].cell = null;
                        gridCells[i + 1, j].cell.ChangeValue();
                    }
                }
            }
            yield return new WaitForSeconds(0.05f);
        }
    }
    public IEnumerator ToLeft()
    {
        for (int k = 0; k < 5; k++)
        {
            for (int i = 2; i < 6; i++)
            {
                for (int j = 1; j < 6; j++)
                {
                    if (gridCells[i, j].cell != null && gridCells[i - 1, j].cell == null)
                    {
                        gridCells[i, j].cell.gameObject.transform.parent = gridCells[i - 1, j].transform;
                        gridCells[i, j].cell.transform.localPosition = Vector3.zero;
                        gridCells[i - 1, j].cell = gridCells[i, j].cell;
                        gridCells[i, j].cell = null;
                    }
                    if (gridCells[i, j].cell != null && gridCells[i - 1, j].cell != null &&
                        gridCells[i, j].cell.value == gridCells[i - 1, j].cell.value)
                    {
                        Destroy(gridCells[i, j].cell.gameObject);
                        gridCells[i, j].cell = null;
                        gridCells[i - 1, j].cell.ChangeValue();
                    }
                }
            }
            yield return new WaitForSeconds(0.05f);
        }
    }
    public IEnumerator ToTop()
    {
        for (int k = 0; k < 5; k++)
        {
            for (int i = 1; i < 6; i++)
            {
                for (int j = 4; j >= 0; j--)
                {
                    if (gridCells[i, j].cell != null && gridCells[i, j + 1].cell == null)
                    {
                        gridCells[i, j].cell.gameObject.transform.parent = gridCells[i, j + 1].transform;
                        gridCells[i, j].cell.transform.localPosition = Vector3.zero;
                        gridCells[i, j + 1].cell = gridCells[i, j].cell;
                        gridCells[i, j].cell = null;
                    }
                    if (gridCells[i, j].cell != null && gridCells[i, j + 1].cell != null &&
                        gridCells[i, j].cell.value == gridCells[i, j + 1].cell.value)
                    {
                        Destroy(gridCells[i, j].cell.gameObject);
                        gridCells[i, j].cell = null;
                        gridCells[i, j + 1].cell.ChangeValue();
                    }
                }
            }
            yield return new WaitForSeconds(0.05f);
        }
    }
    public IEnumerator ToDown()
    {
        for (int k = 0; k < 5; k++)
        {
            for (int i = 1; i < 6; i++)
            {
                for (int j = 2; j < 6; j++)
                {
                    if (gridCells[i, j].cell != null && gridCells[i, j - 1].cell == null)
                    {
                        gridCells[i, j].cell.gameObject.transform.parent = gridCells[i, j - 1].transform;
                        gridCells[i, j].cell.transform.localPosition = Vector3.zero;
                        gridCells[i, j - 1].cell = gridCells[i, j].cell;
                        gridCells[i, j].cell = null;
                    }
                    if (gridCells[i, j].cell != null && gridCells[i, j - 1].cell != null &&
                        gridCells[i, j].cell.value == gridCells[i, j - 1].cell.value)
                    {
                        Destroy(gridCells[i, j].cell.gameObject);
                        gridCells[i, j].cell = null;
                        gridCells[i, j - 1].cell.ChangeValue();
                    }
                }
            }
            yield return new WaitForSeconds(0.05f);
        }
    }
}
