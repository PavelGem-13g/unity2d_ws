﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SubmitButton : MonoBehaviour
{
    public Text login;    
    public Text password;
    public Text LogField;
    SQL SQL;
    private void Awake()
    {
        SQL = FindObjectOfType<SQL>();
    }
    public void Login() 
    {
        SQL.AddPlayer(login.text, password.text);
        login.text = "";
        password.text = "";
    }
    public void RemoveLogin() 
    {
        SQL.RemovePlayer(login.text);
        login.text = "";
    }
    public void EditPassword() 
    {
        SQL.EditPassword(login.text, password.text);
        login.text = "";
        password.text = "";
    }
    public void Read()
    {
        LogField.text = SQL.Read("*");
    }
}
