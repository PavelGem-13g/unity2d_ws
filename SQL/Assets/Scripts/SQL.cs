﻿using MySql.Data.MySqlClient;
using System;
using System.Data.Common;
using UnityEngine;

public class SQL : MonoBehaviour
{
    string host = "127.0.0.1";
    private string database = "users";
    private string port = "3307";
    private string username = "root";
    private string password = "iPad1266";
    MySqlConnection conn;

    void Awake()
    {
        Connect();
    }
    void Connect()
    {
        string connString = 
            "Server=" + host + ";" +
            "Database=" + database + ";" +
            "port=" + port + ";" +
            "User Id=" + username + ";" +
            "password=" + password;
        conn = new MySqlConnection(connString);
    }
    public void AddPlayer(string _login, string _password)
    {
        Command($"INSERT INTO users (login,password)" +
            $" VALUES ('{_login}', '{_password}')");
    }
    public void RemovePlayer(string login)
    {
        Command($"DELETE FROM users WHERE login = '{login}'");
    }
    public void EditPassword(string login, string newPassword)
    {
        Command($"UPDATE users SET password = '{newPassword}' WHERE login = '{login}'");
    }
    void Command(string command)
    {
        conn.Open();
        try
        {
            MySqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = command;
            cmd.ExecuteNonQuery();
        }
        catch (Exception e)
        {
            Debug.LogWarning(e.Message);
        }
            conn.Close();
    }
    public string Read(string selectedFields)
    {
        string result = "";
        conn.Open();
        try
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = "SELECT " + selectedFields + " FROM users";
            DbDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    result += reader.GetValue(reader.GetOrdinal("id")) + " " +
                        reader.GetValue(reader.GetOrdinal("login")) + " " +
                        reader.GetValue(reader.GetOrdinal("password")) + "\n";
                    //result += temp + "\n";
                }
            }
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }
        conn.Close();
        return result;
    }
}
