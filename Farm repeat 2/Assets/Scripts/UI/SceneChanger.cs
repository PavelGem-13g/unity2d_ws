﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneChanger : MonoBehaviour
{
    [SerializeField] GameObject[] scenes;
    private void Awake()
    {
        SetScene(0);
    }
    void CloseAllScenes() 
    {
        foreach (var item in scenes)
        {
            item.SetActive(false);
        }
    }
    public void SetScene(int scene) 
    {
        CloseAllScenes();
        scenes[scene].SetActive(true);
    }
}
