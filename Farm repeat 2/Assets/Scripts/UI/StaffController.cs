﻿using UnityEngine;
using UnityEngine.UI;

public class StaffController : MonoBehaviour
{
    public static StaffController instance;
    [SerializeField] Text sfaffNameUI;
    [SerializeField] Text costUI;
    [SerializeField] Button hire;
    int value;
    int number;
    private void Awake()
    {
        instance = this;
        if (GameManager.instance.three != 0)
        {
            hire.interactable = false;
        }
    }
    public void SetStuff(string staffName, int value, int number)
    {
        sfaffNameUI.text = staffName;
        costUI.text = "Cost: " + value.ToString();
        this.value = value;
        this.number = number;
    }
    public void Hire()
    {
        if (GameManager.instance.value >= value)
        {
            GameManager.instance.ChangeValue(-value);
            if (GameManager.instance.one == 0)
            {
                GameManager.instance.one = number;
                SQL.UpdateInformation("staff", $"one = {GameManager.instance.one}",$"login = '{GameManager.instance.login}'");
            }
            else if (GameManager.instance.two == 0)
            {
                GameManager.instance.two = number;
                SQL.UpdateInformation("staff", $"two = {GameManager.instance.two}", $"login = '{GameManager.instance.login}'");
            }
            else if (GameManager.instance.three == 0)
            {
                GameManager.instance.three = number;
                SQL.UpdateInformation("staff", $"three = {GameManager.instance.three}", $"login = '{GameManager.instance.login}'");
                hire.interactable = false;
            }
        }
    }
}
