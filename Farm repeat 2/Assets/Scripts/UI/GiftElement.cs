﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GiftElement : MonoBehaviour
{
    [SerializeField] Text lable;
    [SerializeField] Image image;
    [SerializeField] Button button;
    [SerializeField] GameObject complete;
    [SerializeField] GameObject flash;

    [SerializeField] Sprite[] sprites;

    int day;
    int type;
    public void SetDay(int day) 
    {
        this.day = day;
        lable.text = day.ToString();
        flash.SetActive(false);
        if (GameManager.instance.sday==day && GameManager.instance.catched==0)
        {
            flash.SetActive(true);
            button.interactable = true;
            complete.SetActive(false);
        }
        if (GameManager.instance.sday == day && GameManager.instance.catched == 1)
        {
            button.interactable = false;
            complete.SetActive(true);
        }
        if (GameManager.instance.sday > day)
        {
            button.gameObject.SetActive(false);
            complete.SetActive(true);
        }
        if (GameManager.instance.sday < day)
        {
            button.interactable = false;
            complete.SetActive(false);
        }
        GetGiftElementInformation();
    }
    public void Collect() 
    {
        if (type==0)
        {
            GameManager.instance.ChangeCrystal(200);
        }
        if (type == 1)
        {
            GameManager.instance.ChangeCrystal(GameManager.instance.crystal);
        }
        if (type == 0)
        {
            GameManager.instance.ChangeValue(1000);
        }
        GameManager.instance.catched = 1;
        SQL.UpdateInformation("users",$"ltake = '{GameManager.DateTimeToShortString(DateTime.UtcNow)}'",$"login = '{GameManager.instance.login}'");
        SQL.UpdateInformation("users",$"catched = {GameManager.instance.catched}",$"login = '{GameManager.instance.login}'");
        button.interactable = false;
        complete.SetActive(true);
    }
    void GetGiftElementInformation() 
    {
        List<string> temp = SQL.GetGiftInformation(day);
        foreach (var item in temp)
        {
            Debug.Log(item);
        }
        type = Convert.ToInt32(temp[0]);
        image.sprite = sprites[type];
    }
}
