﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowUserInformation : MonoBehaviour
{
    [SerializeField] int number;
    Text text;
    private void Awake()
    {
        text = GetComponent<Text>();
    }
    void Start()
    {
        UpdateInformation();
    }
    public void UpdateInformation() 
    {
        List<string> temp = new List<string>();
        temp.Add(GameManager.instance.username);
        temp.Add(GameManager.instance.value.ToString());
        temp.Add(GameManager.instance.crystal.ToString());
        text.text = temp[number];
    }
}
