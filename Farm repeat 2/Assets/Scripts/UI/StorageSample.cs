﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StorageSample : MonoBehaviour
{
    [SerializeField] int number;
    [SerializeField] Sprite[] sprites;
    Image image;
    Text text;
    private void Awake()
    {
        image = GetComponent<Image>();
        number = Convert.ToInt32(name);
        text = GetComponentInChildren<Text>();
    }
    public void UpdateInformation() 
    {
        List<int> temp = new List<int>();
        temp.Add(GameManager.instance.p1);
        temp.Add(GameManager.instance.p2);
        temp.Add(GameManager.instance.p3);
        temp.Add(GameManager.instance.a1);
        temp.Add(GameManager.instance.a2);
        temp.Add(GameManager.instance.a3);
        temp.Add(GameManager.instance.f);
        image.sprite = sprites[number];
        text.text = temp[number].ToString();
    }
}
