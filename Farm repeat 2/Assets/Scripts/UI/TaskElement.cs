﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TaskElement : MonoBehaviour
{
    int idtasks;
    public int number;
    [SerializeField] Text textUI;
    [SerializeField] Button buttonCollect;
    string lable;
    string text;
    int state;
    private void Awake()
    {
        GetTaskInformation();
        textUI.text = lable;
    }
    void GetTaskInformation() 
    {
        List<string> temp = SQL.GetTaskInformation(GameManager.instance.login, number);
        idtasks = Convert.ToInt32(temp[0]);
        lable = temp[1];
        text = temp[2];
        state = Convert.ToInt32(temp[3]);
    }
    public void UpdateInformation() 
    {
        if (state==0)
        {
            if (number==0 && GameManager.instance.p1>4 && GameManager.instance.p2 > 4 && GameManager.instance.p3 > 4)
            {
                buttonCollect.interactable = true;
            }
            else if (number==1 && GameManager.instance.a1 > 0 && GameManager.instance.a2 > 0 && GameManager.instance.a3 > 0)
            {
                buttonCollect.interactable = true;
            }
            else if (number == 2 && GameManager.instance.value > 4)
            {
                buttonCollect.interactable = true;
            }
            else 
            {
                buttonCollect.interactable = false;
            }
        }
        else 
        {
            buttonCollect.gameObject.SetActive(false);
        }
    }
    public void Exit() 
    {
        textUI.text = lable;
    }
    public void Enter() 
    {
        textUI.text = text;
    }
    public void Collect() 
    {
        if (number == 0)
        {
            GameManager.instance.ChangeValue(100);
        }
        else if (number == 1)
        {
            GameManager.instance.ChangeValue(200);
        }
        else if (number == 2)
        {
            GameManager.instance.ChangeCrystal(200);
        }
        state = 1;
        SQL.UpdateInformation("tasks", $"state = {state}", $"idtasks = {idtasks}");
        UpdateInformation();
    }
}
