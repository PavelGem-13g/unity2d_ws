﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tools : MonoBehaviour
{
    [SerializeField] GameObject giftParent;
    [SerializeField] GameObject giftPanel;
    [SerializeField] GameObject giftPrefab;
    private void Awake()
    {
        GiftAwakeController();
    }
    void GiftAwakeController() 
    {
        if (GameManager.instance.ltake < new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day,0,0,0))
        {
            GameManager.instance.catched = 0;
            GameManager.instance.sday += 1;
            SQL.UpdateInformation("users", $"catched = {GameManager.instance.catched}", $"login = '{GameManager.instance.login}'");
            SQL.UpdateInformation("users", $"sday = {GameManager.instance.sday}", $"login = '{GameManager.instance.login}'");
            giftPanel.SetActive(true);
        }
        if (GameManager.instance.ltake == new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 0, 0, 0))
        {
            giftPanel.SetActive(false);
        }
        for (int i = 0; i < 6; i++)
        {
            GiftElement temp = Instantiate(giftPrefab).GetComponent<GiftElement>();
            temp.gameObject.transform.parent = giftParent.transform;
            temp.gameObject.transform.localScale = Vector3.one;
            temp.SetDay(i);
        }
    }
    public void UpdateStorageInformaion() 
    {
        foreach (var item in FindObjectsOfType<StorageSample>())
        {
            item.UpdateInformation();
        }
    }
    public void UpdateTaskeInformaion() 
    {
        foreach (var item in FindObjectsOfType<TaskElement>())
        {
            item.UpdateInformation();
        }
    }
}
