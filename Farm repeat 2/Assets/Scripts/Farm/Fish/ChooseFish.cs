﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseFish : MonoBehaviour
{
    public static ChooseFish instance;
    Fish localFish;
    [SerializeField] GameObject clear;
    [SerializeField] GameObject set;
    private void Awake()
    {
        instance = this;
        gameObject.SetActive(false);
    }
    public void SetType(int type)
    {
        localFish.SetType(type);
        gameObject.SetActive(false);
    }
    public void Clear()
    {
        localFish.ChangeStateValue();
        gameObject.SetActive(false);
    }
    public void Activate(Fish animal)
    {
        localFish = animal;
        GetComponent<RectTransform>().transform.position = animal.GetComponent<RectTransform>().position;
        if (localFish.state == 0)
        {
            clear.SetActive(true);
            this.set.SetActive(false);
        }
        if (localFish.state == 1)
        {
            clear.SetActive(false);
            this.set.SetActive(true);
        }
        StartCoroutine(Zoom());
    }
    IEnumerator Zoom()
    {
        transform.localScale = Vector3.zero;
        while (transform.localScale.x < 1f)
        {
            transform.localScale += new Vector3(0.01f, 0.01f);
            yield return new WaitForSeconds(0.01f);
        }
    }
}
