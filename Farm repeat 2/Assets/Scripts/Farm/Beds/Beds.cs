﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Beds : MonoBehaviour
{
    int number;
    int idbeds;
    public int state;
    int type;
    DateTime endTime;
    bool isGrowning = false;
    Image image;
    [SerializeField] Sprite growningSprite;
    [SerializeField] Sprite[] stateSprites;
    [SerializeField] Sprite[] typeSprites;
    [SerializeField] GameObject changeBeds;

    private void Awake()
    {
        image = GetComponent<Image>();
        number = Convert.ToInt32(name);
        GetBedsInformaion();
        SpriteUpdate();
        Timer();
    }
    void GetBedsInformaion()
    {
        List<string> temp = SQL.GetBedsInformation(GameManager.instance.login, number);
        idbeds = Convert.ToInt32(temp[0]);
        if (temp[1] == "")
        {
            state = 0;
            type = 0;
            endTime = DateTime.UtcNow;
            SQL.InserIntoInformation("landing", "idbeds,state,type,endTime", $"{idbeds},{state},{type},'{GameManager.DateTimeToString(endTime)}'");
        }
        else
        {
            state = Convert.ToInt32(temp[1]);
            type = Convert.ToInt32(temp[2]);
            endTime = GameManager.DateTimeFromString(temp[3]);
        }
    }
    public void SetType(int type)
    {
        this.type = type;
        int delta = 0;
        if (type == 1)
        {
            delta = 10;
        }
        if (type == 2)
        {
            delta = 20;
        }
        if (type == 3)
        {
            delta = 30;
        }
        endTime = DateTime.UtcNow + new TimeSpan(0, 0, delta);
        SQL.UpdateInformation("landing", $"type = {type}", $"idbeds = {idbeds}");
        SQL.UpdateInformation("landing", $"state = {state}", $"idbeds = {idbeds}");
        SQL.UpdateInformation("landing", $"endTime = '{GameManager.DateTimeToString(endTime)}'", $"idbeds = {idbeds}");
        Timer();
    }
    public void ChangeStateValue()
    {
        state += 1;
        SpriteUpdate();
    }
    public void ChangeState()
    {
        if (!isGrowning)
        {
            if (state < 3)
            {
                changeBeds.SetActive(true);
                ChooseBeds.instance.Activate(this);
            }
            else
            {
                Clear();
            }
        }
    }
    void Clear()
    {
        GameManager.instance.ChangeValue(type);
        if (type == 1)
        {
            GameManager.instance.p1 += 1;
            SQL.UpdateInformation("storage", $"p1 = {GameManager.instance.p1}", $"login = '{GameManager.instance.login}'");
        }
        if (type == 2)
        {
            GameManager.instance.p2 += 1;
            SQL.UpdateInformation("storage", $"p2 = {GameManager.instance.p2}", $"login = '{GameManager.instance.login}'");
        }
        if (type == 3)
        {
            GameManager.instance.p3 += 1;
            SQL.UpdateInformation("storage", $"p3 = {GameManager.instance.p3}", $"login = '{GameManager.instance.login}'");
        }
        type = 0;
        state = 0;
        endTime = DateTime.UtcNow;
        SQL.UpdateInformation("landing", $"type = {type}", $"idbeds = {idbeds}");
        SQL.UpdateInformation("landing", $"state = {state}", $"idbeds = {idbeds}");
        SQL.UpdateInformation("landing", $"endTime = '{GameManager.DateTimeToString(endTime)}'", $"idbeds = {idbeds}");
        SpriteUpdate();
    }

    void SpriteUpdate()
    {
        if (isGrowning)
        {
            image.sprite = growningSprite;
        }
        else
        if (state < 3)
        {
            image.sprite = stateSprites[state];
        }
        else
        if (type != 0 && state == 3)
        {
            image.sprite = typeSprites[type];
        }
    }
    void Timer()
    {
        if ((endTime - DateTime.UtcNow).Seconds > 0)
        {
            isGrowning = true;
            SpriteUpdate();
            Invoke("End", (endTime - DateTime.UtcNow).Seconds);
        }
    }
    void End()
    {
        isGrowning = false;
        state = 3;
        SpriteUpdate();
    }
}
