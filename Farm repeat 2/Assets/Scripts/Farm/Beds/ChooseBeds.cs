﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseBeds : MonoBehaviour
{
    public static ChooseBeds instance;
    [SerializeField] Beds localBeds;
    [SerializeField] GameObject dig;
    [SerializeField] GameObject polit;
    [SerializeField] GameObject plants;
    RectTransform rectTransform;
    private void Awake()
    {
        instance = this;
        rectTransform = GetComponent<RectTransform>();
        gameObject.SetActive(false);
    }
    public void Polit() 
    {
        polit.SetActive(false);
        localBeds.ChangeStateValue();
        gameObject.SetActive(false);
    }
    public void Dig()
    {
        dig.SetActive(false);
        localBeds.ChangeStateValue();
        gameObject.SetActive(false);
    }
    public void SetType(int type) 
    {
        localBeds.SetType(type);
        gameObject.SetActive(false);
    }
    public void Activate(Beds beds) 
    {
        localBeds = beds;
        rectTransform.transform.position = localBeds.transform.position + new Vector3(-1f,1f);
        if (localBeds.state==0)
        {
            dig.SetActive(true);
            polit.SetActive(false);
            plants.SetActive(false);
        }
        if (localBeds.state == 1)
        {
            dig.SetActive(false);
            polit.SetActive(true);
            plants.SetActive(false);
        }
        if (localBeds.state == 2)
        {
            dig.SetActive(false);
            polit.SetActive(false);
            plants.SetActive(true);
        }
        StartCoroutine(Zoom());
    }
    IEnumerator Zoom() 
    {
        transform.localScale = Vector3.zero;
        while (transform.localScale.x < 1f)
        {
            transform.localScale += new Vector3(0.01f, 0.01f, 0.01f);
            yield return new WaitForEndOfFrame();
        }
    }
}
