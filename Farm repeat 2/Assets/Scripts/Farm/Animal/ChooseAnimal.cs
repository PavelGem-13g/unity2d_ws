﻿using System.Collections;
using UnityEngine;

public class ChooseAnimal : MonoBehaviour
{
    public static ChooseAnimal instance;
    Animal localAnimal;
    [SerializeField] GameObject clear;
    [SerializeField] GameObject animal;
    private void Awake()
    {
        instance = this;
        gameObject.SetActive(false);
    }
    public void SetType(int type) 
    {
        localAnimal.SetType(type);
        gameObject.SetActive(false);
    }
    public void Clear() 
    {
        localAnimal.ChangeStateValue();
        gameObject.SetActive(false);
    }
    public void Activate(Animal animal) 
    {
        localAnimal = animal;
        GetComponent<RectTransform>().transform.position = animal.GetComponent<RectTransform>().position /*+ new Vector3(-1f, 1f)*/; 
        if (localAnimal.state==0)
        {
            clear.SetActive(true);
            this.animal.SetActive(false);
        }
        if (localAnimal.state==1)
        {
            clear.SetActive(false);
            this.animal.SetActive(true);
        }
        StartCoroutine(Zoom());
    }
    IEnumerator Zoom()
    {
        transform.localScale = Vector3.zero;
        while (transform.localScale.x < 1f)
        {
            transform.localScale += new Vector3(0.01f, 0.01f);
            yield return new WaitForSeconds(0.01f);
        }
    }
}
