﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Animal : MonoBehaviour
{
    int idanimal;
    int number;
    public int state;
    int type;
    DateTime endTime;
    bool isGrowning = false;
    Image image;
    Animator animator;
    [SerializeField] Sprite[] animalsSprites;
    [SerializeField] Sprite backgrouSprite;
    [SerializeField] GameObject chooseAnimal;
    private void Awake()
    {
        image = GetComponent<Image>();
        animator = GetComponent<Animator>();
        GetAnimalInformaion();
        SpriteUpdate();
        Timer();
    }
    void GetAnimalInformaion()
    {
        number = Convert.ToInt32(name);
        List<string> temp = SQL.GetAnimalInformation(GameManager.instance.login, number);
        idanimal = Convert.ToInt32(temp[0]);
        state = Convert.ToInt32(temp[1]);
        type = Convert.ToInt32(temp[2]);
        endTime = GameManager.DateTimeFromString(temp[3]);
    }
    public void SetType(int type)
    {
        this.type = type;
        state += 1;
        state = 2;
        this.type = type;
        int delta = 10;
        endTime = DateTime.UtcNow + new TimeSpan(0,0,delta*type);
        SQL.UpdateInformation("animal", $"state = {state}", $"idanimal = {idanimal}");
        SQL.UpdateInformation("animal", $"type = {type}", $"idanimal = {idanimal}");
        SQL.UpdateInformation("animal", $"endTime = '{GameManager.DateTimeToString(endTime)}'", $"idanimal = {idanimal}");
        Timer();
    }
    public void ChangeState()
    {
        if (!isGrowning)
        {
            if (state < 2)
            {
                ChooseAnimal.instance.gameObject.SetActive(true);
                ChooseAnimal.instance.Activate(this);
            }
            else
            {
                Clear();
            }
        }
    }
    public void ChangeStateValue()
    {
        state += 1;
        SpriteUpdate();
    }
    void Clear()
    {
        GameManager.instance.ChangeCrystal(type);
        if (type == 1)
        {
            GameManager.instance.a1 += 1;
            SQL.UpdateInformation("storage", $"a1 = {GameManager.instance.a1}", $"login = '{GameManager.instance.login}'");

        }
        if (type == 2)
        {
            GameManager.instance.a2 += 1;
            SQL.UpdateInformation("storage", $"a2 = {GameManager.instance.a2}", $"login = '{GameManager.instance.login}'");
        }
        if (type == 3)
        {
            GameManager.instance.a3 += 1;
            SQL.UpdateInformation("storage", $"a3 = {GameManager.instance.a3}", $"login = '{GameManager.instance.login}'");
        }        
        state = 0;
        type = 0;
        endTime = DateTime.UtcNow;
        SQL.UpdateInformation("animal", $"state = {state}", $"idanimal = {idanimal}");
        SQL.UpdateInformation("animal", $"type = {type}", $"idanimal = {idanimal}");
        SQL.UpdateInformation("animal", $"endTime = '{GameManager.DateTimeToString(endTime)}'", $"idanimal = {idanimal}");
        SpriteUpdate();
    }
    void SpriteUpdate()
    {
        if (isGrowning)
        {
            animator.Play("AnimalAnim");
        }
        else
        {
            animator.Play("Clear");
        }
        if (state<2)
        {
            image.color = new Color(image.color.r, image.color.g, image.color.b, 0);
        }
        else
        {
            image.sprite = animalsSprites[type-1];
            image.color = new Color(image.color.r, image.color.g, image.color.b, 255);
        }
        image.SetNativeSize();
    }
    void Timer()
    {
        if ((endTime - DateTime.UtcNow).Seconds > 0)
        {
            isGrowning = true;
            SpriteUpdate();
            Invoke("End", (endTime - DateTime.UtcNow).Seconds);
        }
    }
    void End()
    {
        isGrowning = false;
        state = 3;
        SpriteUpdate();
    }
}
