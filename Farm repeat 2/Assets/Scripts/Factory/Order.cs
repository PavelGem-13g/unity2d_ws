﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Order : MonoBehaviour
{
    [SerializeField] int position;
    [SerializeField] Sprite[] machineOne;
    [SerializeField] Sprite[] machineTwo;
    Image image;

    private void Awake()
    {
        image = GetComponent<Image>();
    }
    public void ShowInformation() 
    {
        if (ChooseProducts.instance.localMachine.type == 1)
        {
            if (position==0)
            {
                image.sprite = machineOne[ChooseProducts.instance.localMachine.typeOne];
            }
            if (position == 1)
            {
                image.sprite = machineOne[ChooseProducts.instance.localMachine.typeTwo];
            }
            if (position == 2)
            {
                image.sprite = machineOne[ChooseProducts.instance.localMachine.typeThree];
            }
            if (position == 3)
            {
                image.sprite = machineOne[ChooseProducts.instance.localMachine.typeFour];
            }
            if (position == 4)
            {
                image.sprite = machineOne[ChooseProducts.instance.localMachine.typeFive];
            }
        }
        if (ChooseProducts.instance.localMachine.type == 2)
        {
            if (position == 0)
            {
                image.sprite = machineTwo[ChooseProducts.instance.localMachine.typeOne];
            }
            if (position == 1)
            {
                image.sprite = machineTwo[ChooseProducts.instance.localMachine.typeTwo];
            }
            if (position == 2)
            {
                image.sprite = machineTwo[ChooseProducts.instance.localMachine.typeThree];
            }
            if (position == 3)
            {
                image.sprite = machineTwo[ChooseProducts.instance.localMachine.typeFour];
            }
            if (position == 4)
            {
                image.sprite = machineTwo[ChooseProducts.instance.localMachine.typeFive];
            }
        }
    }

}
