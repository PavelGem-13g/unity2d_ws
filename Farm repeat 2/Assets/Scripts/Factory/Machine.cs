﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Machine : MonoBehaviour
{
    public int type;
    public int idmachine;
    public int number;
    public int state;

    public int typeOne;
    public int typeTwo;
    public int typeThree;
    public int typeFour;
    public int typeFive;
    DateTime endTimeOne;
    DateTime endTimeTwo;
    DateTime endTimeThree;
    DateTime endTimeFour;
    DateTime endTimeFive;

    bool isGrowning = false;
    Image image;
    Animator animator;
    [SerializeField] Sprite[] machineSprites;
    [SerializeField] Sprite backgrouSprite;
    [SerializeField] GameObject breakObject;
    private void Awake()
    {
        image = GetComponent<Image>();
        animator = GetComponent<Animator>();
        GetMachineInformaion();
        SpriteUpdate();
        Timer();
    }
    void GetMachineInformaion()
    {
        number = Convert.ToInt32(name);
        List<string> temp = SQL.GetMachineInformation(GameManager.instance.login, number);
        idmachine = Convert.ToInt32(temp[0]);
        type = Convert.ToInt32(temp[1]);
        if (temp[2] == "")
        {
            state = 0;
            typeOne = 0;
            endTimeOne = DateTime.UtcNow;

            typeTwo = 0;
            endTimeTwo = DateTime.UtcNow;

            typeThree = 0;
            endTimeThree = DateTime.UtcNow;

            typeFour = 0;
            endTimeFour = DateTime.UtcNow;

            typeFive = 0;
            endTimeFive = DateTime.UtcNow;
            SQL.InserIntoInformation(
                "machineorder",
                "idmachine, typeOne, endTimeOne, typeTwo, endTimeTwo, typeThree, endTimeThree, typeFour, endTimeFour, typeFive, endTimeFive",
                $"{idmachine}, {typeOne}, '{GameManager.DateTimeToString(endTimeOne)}', {typeTwo}, '{GameManager.DateTimeToString(endTimeTwo)}', {typeThree}, '{GameManager.DateTimeToString(endTimeThree)}', {typeFour}, '{GameManager.DateTimeToString(endTimeFour)}', {typeFive}, '{GameManager.DateTimeToString(endTimeFive)}'");
        }
        else
        {
            typeOne = Convert.ToInt32(temp[2]);
            endTimeOne = GameManager.DateTimeFromString(temp[3]);

            typeTwo = Convert.ToInt32(temp[4]);
            endTimeTwo = GameManager.DateTimeFromString(temp[5]);

            typeThree = Convert.ToInt32(temp[6]);
            endTimeThree = GameManager.DateTimeFromString(temp[7]);

            typeFour = Convert.ToInt32(temp[8]);
            endTimeFour = GameManager.DateTimeFromString(temp[9]);

            typeFive = Convert.ToInt32(temp[10]);
            endTimeFive = GameManager.DateTimeFromString(temp[11]);
        }
    }
    public void SetMachineType(int type)
    {
        this.type = type;
        SpriteUpdate();
    }
    public void SetType(int type)
    {

        state += 1;
        state = 2;
        //this.type = type;
        int delta = 10 * type;
        if (typeOne == 0)
        {
            typeOne = type;
            endTimeOne = DateTime.UtcNow + new TimeSpan(0, 0, delta);
            SQL.UpdateInformation("machineorder", $"typeOne = {typeOne}", $"idmachine = {idmachine}");
            SQL.UpdateInformation("machineorder", $"endTimeOne = '{GameManager.DateTimeToString(endTimeOne)}'", $"idmachine = {idmachine}");
        }
        else if (typeTwo == 0)
        {
            typeTwo = type;
            endTimeTwo = DateTime.UtcNow + new TimeSpan(0, 0, delta);
            SQL.UpdateInformation("machineorder", $"typeTwo = {typeTwo}", $"idmachine = {idmachine}");
            SQL.UpdateInformation("machineorder", $"endTimeTwo = '{GameManager.DateTimeToString(endTimeTwo)}'", $"idmachine = {idmachine}");
        }
        else if (typeThree == 0)
        {
            typeThree = type;
            endTimeThree = DateTime.UtcNow + new TimeSpan(0, 0, delta);
            SQL.UpdateInformation("machineorder", $"typeThree = {typeThree}", $"idmachine = {idmachine}");
            SQL.UpdateInformation("machineorder", $"endTimeThree = '{GameManager.DateTimeToString(endTimeThree)}'", $"idmachine = {idmachine}");
        }
        else if (typeFour == 0)
        {
            typeFour = type;
            endTimeFour = DateTime.UtcNow + new TimeSpan(0, 0, delta);
            SQL.UpdateInformation("machineorder", $"typeFour = {typeFour}", $"idmachine = {idmachine}");
            SQL.UpdateInformation("machineorder", $"endTimeFour = '{GameManager.DateTimeToString(endTimeFour)}'", $"idmachine = {idmachine}");
        }
        else if (typeFive == 0)
        {
            typeFive = type;
            endTimeFive = DateTime.UtcNow + new TimeSpan(0, 0, delta);
            SQL.UpdateInformation("machineorder", $"typeFive = {typeFive}", $"idmachine = {idmachine}");
            SQL.UpdateInformation("machineorder", $"endTimeFive = '{GameManager.DateTimeToString(endTimeFive)}'", $"idmachine = {idmachine}");
        }
        Timer();
    }
    public void ChangeState()
    {

        if (type == -1)
        {
            type = 0;
            SpriteUpdate();
        }
        else if (type == 0)
        {
            ChooserMachine.instance.gameObject.SetActive(true);
            ChooserMachine.instance.Activate(this);
        }
        else
        {
            ChooseProducts.instance.gameObject.SetActive(true);
            ChooseProducts.instance.Activate(this);
        }
    }
    public void ChangeStateValue()
    {
        state += 1;
        SpriteUpdate();
    }
    void Clear()
    {
        GameManager.instance.ChangeCrystal(type);
        if (typeOne == 1)
        {
            GameManager.instance.m1 += 1;
            SQL.UpdateInformation("storage", $"m1 = {GameManager.instance.m1}", $"login = '{GameManager.instance.login}'");

        }
        if (typeOne == 2)
        {
            GameManager.instance.m2 += 1;
            SQL.UpdateInformation("storage", $"m2 = {GameManager.instance.m2}", $"login = '{GameManager.instance.login}'");
        }
        state = 0;
        type = 0;
        endTimeOne = endTimeTwo;
        endTimeTwo = endTimeThree;
        endTimeThree = endTimeFour;
        endTimeFour = endTimeFive;
        endTimeFive = DateTime.UtcNow;

        typeOne = typeTwo;
        typeTwo = typeThree;
        typeThree = typeFour;
        typeFour = typeFive;
        typeFive = 0;

        SQL.UpdateInformation("machineorder", $"typeOne = {typeOne}", $"idanimal = {idmachine}");
        SQL.UpdateInformation("machineorder", $"endTimeOne = '{GameManager.DateTimeToString(endTimeOne)}'", $"idanimal = {idmachine}");
        SQL.UpdateInformation("machineorder", $"typeTwo = {typeTwo}", $"idanimal = {idmachine}");
        SQL.UpdateInformation("machineorder", $"endTimeTwo = '{GameManager.DateTimeToString(endTimeTwo)}'", $"idanimal = {idmachine}");
        SQL.UpdateInformation("machineorder", $"typeThree = {typeThree}", $"idanimal = {idmachine}");
        SQL.UpdateInformation("machineorder", $"endTimeThree = '{GameManager.DateTimeToString(endTimeThree)}'", $"idanimal = {idmachine}");
        SQL.UpdateInformation("machineorder", $"typeFour = {typeFour}", $"idanimal = {idmachine}");
        SQL.UpdateInformation("machineorder", $"endTimeFour = '{GameManager.DateTimeToString(endTimeFour)}'", $"idanimal = {idmachine}");
        SQL.UpdateInformation("machineorder", $"typeFive = {typeFive}", $"idanimal = {idmachine}");
        SQL.UpdateInformation("machineorder", $"endTimeFive = '{GameManager.DateTimeToString(endTimeFive)}'", $"idanimal = {idmachine}");

        Timer();
    }
    public void SpriteUpdate()
    {
        if (isGrowning)
        {
            animator.Play("AnimalAnim");
        }
        else
        {
            animator.Play("Clear");
        }
        if (type == -1)
        {
            breakObject.SetActive(true);
        }
        else if (type == 0)
        {
            image.sprite = backgrouSprite;
        }
        else
        {
            image.sprite = machineSprites[type - 1];
            breakObject.SetActive(false);
        }
        //image.SetNativeSize();
    }
    void Timer()
    {
        SpriteUpdate();
        if ((endTimeOne - DateTime.UtcNow).Seconds > 0)
        {
/*            if (Random.Range(0, 100) < 30)
            {
                Invoke("Break", (endTimeOne - DateTime.UtcNow).Seconds / 3);
            }
            else
            {*/
                isGrowning = true;
                SpriteUpdate();
                Invoke("End", (endTimeOne - DateTime.UtcNow).Seconds);
            //}
        }
    }
    void End()
    {
        isGrowning = false;
        state = 3;
        //Clear();
    }
    void Break()
    {
        type = -1;
        isGrowning = false;
    }
}
