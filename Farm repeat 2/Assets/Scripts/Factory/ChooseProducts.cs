﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseProducts : MonoBehaviour
{
    public static ChooseProducts instance;
    public Machine localMachine;
    [SerializeField] GameObject typeOne;
    [SerializeField] GameObject typeTwo;
    [SerializeField] GameObject sell;
    [SerializeField] GameObject order;
    //[SerializeField] GameObject panel;
    private void Awake()
    {
        instance = this;
        gameObject.SetActive(false);
    }
    public void SetType(int type)
    {
        localMachine.SetType(type);
        gameObject.SetActive(false);
    }
    public void Sell() 
    {
        GameManager.instance.ChangeValue(1000);
        localMachine.type = 0;
        localMachine.SpriteUpdate();
        SQL.UpdateInformation("machine",$"type = {localMachine.type}",$"idmachine = {localMachine.idmachine}");
        gameObject.SetActive(false);        
    }
    public void Activate(Machine machine)
    {
        StartCoroutine(Zoom());        
        order.SetActive(true);
        sell.SetActive(true);
        localMachine = machine;
        GetComponent<RectTransform>().transform.position = localMachine.GetComponent<RectTransform>().position ;
        foreach (var item in FindObjectsOfType<Order>())
        {
            item.ShowInformation();
        }
        if (localMachine.type==1)
        {
            typeOne.SetActive(true);
            typeTwo.SetActive(false);
        }
        if (localMachine.type==2)
        {
            typeOne.SetActive(false);
            typeTwo.SetActive(true);
        }

    }
    IEnumerator Zoom()
    {
        transform.localScale = Vector3.zero;
        while (transform.localScale.x < 1f)
        {
            transform.localScale += new Vector3(0.01f, 0.01f);
            yield return new WaitForSeconds(0.01f);
        }
    }
}
