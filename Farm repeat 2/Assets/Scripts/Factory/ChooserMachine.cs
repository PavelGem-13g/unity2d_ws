﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooserMachine : MonoBehaviour
{
    public static ChooserMachine instance;
    Machine localMachine;
    [SerializeField] GameObject set;
    private void Awake()
    {
        instance = this;
        gameObject.SetActive(false);
    }
    public void SetType(int type)
    {
        localMachine.SetMachineType(type);
        gameObject.SetActive(false);
    }
    public void Activate(Machine machine)
    {
        StartCoroutine(Zoom());
        localMachine = machine;
        GetComponent<RectTransform>().transform.position = localMachine.GetComponent<RectTransform>().position /*+ new Vector3(-1f, 1f)*/;
        if (localMachine.type == 0)
        {
            set.SetActive(true);
        }
    }
    IEnumerator Zoom()
    {
        transform.localScale = Vector3.zero;
        while (transform.localScale.x < 1f)
        {
            transform.localScale += new Vector3(0.01f, 0.01f);
            yield return new WaitForSeconds(0.01f);
        }
    }
}
