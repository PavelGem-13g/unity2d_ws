﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class FillLine : MonoBehaviour
{
    public UnityEvent @event;
    void Start()
    {
        StartCoroutine(Loading());
    }
    IEnumerator Loading() 
    {
        Image image = GetComponent<Image>();
        image.fillAmount = 0;
        while (image.fillAmount<1f)
        {
            image.fillAmount += 0.01f;
            yield return new WaitForEndOfFrame();
        }
        @event.Invoke();
    }
}
