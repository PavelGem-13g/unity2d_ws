﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TouchToStart : MonoBehaviour
{
    public UnityEvent @event;
    private void Update()
    {
        if (Input.anyKey)
        {
            @event.Invoke();
        }
    }
}
