﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Authorization : MonoBehaviour
{
    [SerializeField] InputField log_in_login;
    [SerializeField] InputField log_in_password;
    [SerializeField] Text log_in_error;

    [SerializeField] InputField sign_up_login;
    [SerializeField] InputField sign_up_password;
    [SerializeField] InputField sign_up_username;
    [SerializeField] Text sign_up_error;
    private void Awake()
    {
        SQL.Connect();
    }
    public void SignUp()
    {
        List<string> temp = SQL.GetUserInformation(sign_up_login.text, sign_up_password.text);
        Debug.Log(temp.Count > 0);
        if (temp.Count > 0)
        {
            sign_up_error.text = "Пользователь существует";
        }
        else
        {
            SQL.AddUser(sign_up_login.text, sign_up_password.text, sign_up_username.text);
            sign_up_error.gameObject.transform.parent.gameObject.SetActive(false);
        }
    }
    public void LogIn()
    {
        List<string> temp = SQL.GetUserInformation(log_in_login.text, log_in_password.text);
        if (temp.Count > 0)
        {
            GameManager.instance.Authorize(log_in_login.text, log_in_password.text, temp[0], Convert.ToInt32(temp[1]), Convert.ToInt32(temp[2]), Convert.ToInt32(temp[3]),GameManager.DateTimeFromShortString(temp[4]),Convert.ToInt32(temp[5]));
            log_in_error.text = "Hello" + temp[0];
            SceneManager.LoadScene(1);
        }
        else
        {
            log_in_error.text = "Ошибка";
        }
    }
}
