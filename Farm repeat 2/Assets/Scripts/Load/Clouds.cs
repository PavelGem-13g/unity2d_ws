﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Clouds : MonoBehaviour
{
    [SerializeField] Image[] clouds;
    [SerializeField] RectTransform[] targets;
    [SerializeField] UnityEvent @event;
    void Start()
    {
        StartCoroutine(DeletingClouds());
    }
    IEnumerator DeletingClouds() 
    { 
        @event.Invoke();
        while (clouds[0].color.a>0)
        {
            for (int i = 0; i < clouds.Length; i++)
            {
                clouds[i].color = new Color(clouds[i].color.r, clouds[i].color.g, clouds[i].color.b, clouds[i].color.a - 0.01f);
                clouds[i].GetComponent<RectTransform>().transform.position = Vector3.MoveTowards(clouds[i].GetComponent<RectTransform>().transform.position,targets[i].position,0.01f);
            }
            yield return new WaitForSeconds(0.01f);
        }       
        for (int i = 0; i < clouds.Length; i++)
        {
            Destroy(clouds[i].gameObject);
            Destroy(targets[i].gameObject);
        }
        //Destroy(gameObject);
    }
}
