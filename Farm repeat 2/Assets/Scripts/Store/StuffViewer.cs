﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StuffViewer : MonoBehaviour
{
    [SerializeField] Sprite[] sprites;
    [SerializeField] int number;
    [SerializeField] GameObject error;
    private void Awake()
    {
        UpdateInformation();
    }
    public void UpdateInformation() 
    {
        List<int> temp = new List<int>();
        temp.Add(GameManager.instance.one);
        temp.Add(GameManager.instance.two);
        temp.Add(GameManager.instance.three);
        Image image = GetComponent<Image>();
        if (temp[number]!=0)
        {
            image.sprite = sprites[temp[number]];
        }
        else 
        {
            error.SetActive(false);
        }
    }
}
