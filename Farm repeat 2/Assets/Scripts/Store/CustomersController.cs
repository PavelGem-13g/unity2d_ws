﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomersController : MonoBehaviour
{
    public static CustomersController instance;
    public Customers[] customers;
    public Transform[] positions;
    public Button button;
    private void Awake()
    {
        instance = this;
        UpdateInformation();
    }
    public void UpdateInformation() 
    {
        List<int> temp = new List<int>();
        temp.Add(GameManager.instance.one);
        temp.Add(GameManager.instance.two);
        temp.Add(GameManager.instance.three);
        bool flag = false;
        foreach (var item in temp)
        {
            if (item!=0)
            {
                flag = true;
                break;
            }
        }
        if (!flag)
        {
            button.GetComponentInChildren<Text>().text = "Error";
            button.interactable = false;
        }
        else 
        {
            button.interactable = true;
            if (customers[0].willingType==0)
            {
                button.GetComponentInChildren<Text>().text = "vp";
            }
            else if (customers[0].willingType == 1)
            {
                button.GetComponentInChildren<Text>().text = "rp";
            }else if (customers[0].willingType == 2)
            {
                button.GetComponentInChildren<Text>().text = "yp";
            }
        }
    }
    public void NewCustomer() 
    {
        for (int i = 0; i < customers.Length-1; i++)
        {
            customers[i].type = customers[i + 1].type;
            customers[i].willingType = customers[i + 1].willingType;
            customers[i].SpriteUpdate();
        }
        customers[customers.Length - 1].type = Random.Range(0, customers[customers.Length - 1].mask.Length);
        customers[customers.Length - 1].willingType = Random.Range(0, 3);
        customers[customers.Length - 1].SpriteUpdate();
        if (customers[0].willingType == 0)
        {
            button.GetComponentInChildren<Text>().text = "p1";
        } 
        else if (customers[0].willingType == 1)
        {
            button.GetComponentInChildren<Text>().text = "p2";
        }
        else if (customers[0].willingType == 2)
        {
            button.GetComponentInChildren<Text>().text = "p3";
        }
        foreach (var item in customers)
        {
            item.UpdateInformation();
        }
    }
    public void ButtonHandler() 
    {
        if (customers[0].willingType == 0 && GameManager.instance.p1>0)
        {
            GameManager.instance.p1 -= 1;
            GameManager.instance.ChangeValue(1);
            NewCustomer();
        }
        if (customers[0].willingType == 1 && GameManager.instance.p2 > 0)
        {
            GameManager.instance.p2 -= 1;
            GameManager.instance.ChangeValue(1);
            NewCustomer();
        }
        if (customers[0].willingType == 2 && GameManager.instance.p3 > 0)
        {
            GameManager.instance.p3 -= 1;
            GameManager.instance.ChangeValue(1);
            NewCustomer();
        }
        foreach (var item in FindObjectsOfType<StorageSample>())
        {
            item.UpdateInformation();
        }
    }
}
