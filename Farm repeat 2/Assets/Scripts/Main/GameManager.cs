﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public string login;
    public string password;
    public string username;
    public int value;
    public int crystal;

    public int p1;
    public int p2;
    public int p3;

    public int a1;
    public int a2;
    public int a3;

    public int f;

    public int m1;
    public int m2;

    public int one;
    public int two;
    public int three;

    public int sday;
    public DateTime ltake;
    public int catched;

    private void Awake()
    {
        instance = this;
        DontDestroyOnLoad(gameObject);
    }
    public void Authorize(string login, string password, string username, int value, int crystal, int sday, DateTime ltake, int catched)
    {
        this.login = login;
        this.password = password;
        this.username = username;
        this.value = value;
        this.crystal = crystal;
        this.sday= sday;
        this.ltake = ltake;
        this.catched = catched;

        GetStorageInformation();
        GetStaffInformation();
    }
    public void GetStorageInformation()
    {
        List<string> temp = SQL.GetStorageInformation(login);
        p1 = Convert.ToInt32(temp[0]);
        p2 = Convert.ToInt32(temp[1]);
        p3 = Convert.ToInt32(temp[2]);

        a1 = Convert.ToInt32(temp[3]);
        a2 = Convert.ToInt32(temp[4]);
        a3 = Convert.ToInt32(temp[5]);

        f = Convert.ToInt32(temp[6]);

        m1 = Convert.ToInt32(temp[7]);
        m2 = Convert.ToInt32(temp[8]);
    }
    public void GetStaffInformation() 
    {
        List<string> temp = SQL.GetStaffInformation(login);
        one = Convert.ToInt32(temp[0]);
        two = Convert.ToInt32(temp[1]);
        three = Convert.ToInt32(temp[2]);
    }
    public void ChangeValue(int value)
    {
        instance.value += value;
        SQL.UpdateInformation("users", $"value = {instance.value}", $"login = '{instance.login}'");
        UpdateTopPanel();
    }
    public void ChangeCrystal(int value)
    {
        instance.crystal += value;
        SQL.UpdateInformation("users", $"crystal = {instance.crystal}", $"login = '{instance.login}'");
        UpdateTopPanel();
    }
    public void UpdateTopPanel()
    {
        foreach (var item in FindObjectsOfType<ShowUserInformation>())
        {
            item.UpdateInformation();
        }
    }
    public static DateTime DateTimeFromString(string data)
    {
        string[] pasrsed = data.Split(new char[] { '.' });
        int[] result = new int[pasrsed.Length];
        if (result.Length > 0)
        {
            for (int i = 0; i < pasrsed.Length; i++)
            {
                result[i] = Convert.ToInt32(pasrsed[i]);
            }
            return new DateTime(result[0], result[1], result[2], result[3], result[4], result[5]);
        }
        return DateTime.UtcNow;
    }
    public static string DateTimeToString(DateTime dateTime)
    {
        return $"{dateTime.Year}.{dateTime.Month}.{dateTime.Day}.{dateTime.Hour}.{dateTime.Minute}.{dateTime.Second}";
    }
    public static DateTime DateTimeFromShortString(string data)
    {
        string[] pasrsed = data.Split(new char[] { '.' });
        int[] result = new int[pasrsed.Length];
        if (result.Length > 0)
        {
            for (int i = 0; i < pasrsed.Length; i++)
            {
                result[i] = Convert.ToInt32(pasrsed[i]);
            }
            return new DateTime(result[0], result[1], result[2], 0, 0, 0);
        }
        return DateTime.UtcNow;
    }
    public static string DateTimeToShortString(DateTime dateTime)
    {
        return $"{dateTime.Year}.{dateTime.Month}.{dateTime.Day}";
    }
}
