﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MySql.Data.MySqlClient;
using System;
using System.Data.Common;

public static class SQL
{
    static MySqlConnection connection;
    /// подключение к бд
    public static void Connect() 
    {
        connection = new MySqlConnection(DBSettings.GetConnectionString());
    }
    /// выполениние команды
    public static void NonQueryCommand(string command) 
    {
        connection.Open();
        try
        {
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = command;
            mySqlCommand.ExecuteNonQuery();
        }
        catch (Exception e)
        {
            Debug.Log($"SQL - {e.Message}");
        }
        connection.Close();
    }
    /// чтение из бд
    static DbDataReader ReaderCommand(string selectData, string table, string filter) 
    {
        MySqlCommand mySqlCommand = connection.CreateCommand();
        mySqlCommand.CommandText = $"SELECT {selectData} FROM {table} WHERE {filter}";
        DbDataReader reader = mySqlCommand.ExecuteReader();
        return reader;
    }
    /// объединенное чтиние из бд
    static DbDataReader JoinReaderCommand(string selectData, string table, string joinTable, string filter, string where) 
    {
        MySqlCommand mySqlCommand = connection.CreateCommand();
        mySqlCommand.CommandText = $"SELECT {selectData} FROM {table} LEFT OUTER JOIN {joinTable} ON {filter} WHERE {where}";
        DbDataReader reader = mySqlCommand.ExecuteReader();
        return reader;
    }
    ///ввод информации в бд
    public static void InserIntoInformation(string table, string colums, string values) 
    {
        NonQueryCommand($"INSERT INTO {table} ({colums}) VALUES ({values})");
    }
    /// обновление информации
    public static void UpdateInformation(string table, string set, string where) 
    {
        NonQueryCommand($"UPDATE {table} SET {set} WHERE {where}");
    }
    public static void AddUser(string login, string password, string username) 
    {
        InserIntoInformation("users", $"login, password, username, value, crystal, sday, ltake, catched", $"'{login}','{password}','{username}',{0},{0},{0},'{GameManager.DateTimeToShortString(DateTime.UtcNow)}',{0}");
        InserIntoInformation("storage", $"login, p1, p2, p3, a1, a2, a3, f, m1, m2", $"'{login}',{0},{0},{0},{0},{0},{0},{0},{0},{0}");
        for (int i = 0; i < 10; i++)
        {
            InserIntoInformation("beds", $"login, number", $"'{login}',{i}");
        }
        for (int i = 0; i < 3; i++)
        {
            InserIntoInformation("animal", $"login, number, state, type, endTime", $"'{login}',{i},{0},{0},'{GameManager.DateTimeToString(DateTime.UtcNow)}'");
        }
        InserIntoInformation("fish", $"login, number, state, type, endTime", $"'{login}',{0},{0},{0},'{GameManager.DateTimeToString(DateTime.UtcNow)}'");
        InserIntoInformation("tasks", $"login, number, lable, text, state", $"'{login}',{0}, 'Farm is farm', 'collect 5 p1, 5 p2, 5 p3',{0}");
        InserIntoInformation("tasks", $"login, number, lable, text, state", $"'{login}',{1}, 'Oh my animals', 'collect 1 cow, 1 chicken, 1 pig',{0}");
        InserIntoInformation("tasks", $"login, number, lable, text, state", $"'{login}',{2}, 'MONEY', 'collect 5 coins',{0}");
        for (int i = 0; i < 6; i++)
        {
            InserIntoInformation("machine", $"login, number,type", $"'{login}',{i},{0}");
        }
        InserIntoInformation("staff", $"login, one, two, three", $"'{login}',{0},{0},{0}");
        for (int i = 0; i < 10; i++)
        {
            InserIntoInformation("customers", $"login, number,type,willingtype", $"'{login}',{i},{UnityEngine.Random.Range(0, 3)},{UnityEngine.Random.Range(0, 3)}");
        }
    }
    public static List<string> GetUserInformation(string login, string password) 
    {
        List<string> result = new List<string>();
        connection.Open();
        DbDataReader reader = ReaderCommand("*", "users",$"login = '{login}' AND password = '{password}'");
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                result.Add(reader.GetValue(3).ToString());
                result.Add(reader.GetValue(4).ToString());
                result.Add(reader.GetValue(5).ToString());
                result.Add(reader.GetValue(6).ToString());
                result.Add(reader.GetValue(7).ToString());
                result.Add(reader.GetValue(8).ToString());
            }
        }
        connection.Close();
        return result;
    }
    public static List<string> GetBedsInformation(string login, int number) 
    {
        List<string> result = new List<string>();
        connection.Open();
        DbDataReader reader = JoinReaderCommand("*", "beds","landing", "beds.idbeds = landing.idbeds", $"login = '{login}' AND number = {number}");
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                result.Add(reader.GetValue(reader.GetOrdinal("idbeds")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("state")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("type")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("endTime")).ToString());
            }
        }
        connection.Close();
        return result;
    }
    public static List<string> GetStorageInformation(string login)
    {
        List<string> result = new List<string>();
        connection.Open();
        DbDataReader reader = ReaderCommand("*", "storage", $"login = '{login}'");
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                result.Add(reader.GetValue(reader.GetOrdinal("p1")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("p2")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("p3")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("a1")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("a2")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("a3")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("f")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("m1")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("m2")).ToString());
            }
        }
        connection.Close();
        return result;
    }
    public static List<string> GetAnimalInformation(string login, int number)
    {
        List<string> result = new List<string>();
        connection.Open();
        DbDataReader reader = ReaderCommand("*", "animal", $"login = '{login}' AND number = {number}");
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                result.Add(reader.GetValue(reader.GetOrdinal("idanimal")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("state")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("type")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("endTime")).ToString());
            }
        }
        connection.Close();
        return result;
    }
    public static List<string> GetGiftInformation(int day)
    {
        List<string> result = new List<string>();
        connection.Open();
        DbDataReader reader = ReaderCommand("*", "gift", $"day = {day}");
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                result.Add(reader.GetValue(reader.GetOrdinal("type")).ToString());
            }
        }
        connection.Close();
        return result;
    }
    public static List<string> GetTaskInformation(string login, int number)
    {
        List<string> result = new List<string>();
        connection.Open();
        DbDataReader reader = ReaderCommand("*", "tasks", $"login = '{login}' AND number = {number}");
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                result.Add(reader.GetValue(reader.GetOrdinal("idtasks")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("lable")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("text")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("state")).ToString());
            }
        }
        connection.Close();
        return result;
    }
    public static List<string> GetMachineInformation(string login, int number)
    {
        List<string> result = new List<string>();
        connection.Open();
        DbDataReader reader = JoinReaderCommand("*", "machine", "machineorder", "machine.idmachine = machineorder.idmachine", $"login = '{login}' AND number = {number}");
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                result.Add(reader.GetValue(reader.GetOrdinal("idmachine")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("type")).ToString());

                result.Add(reader.GetValue(reader.GetOrdinal("typeOne")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("endTimeOne")).ToString());

                result.Add(reader.GetValue(reader.GetOrdinal("typeTwo")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("endTimeTwo")).ToString());

                result.Add(reader.GetValue(reader.GetOrdinal("typeThree")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("endTimeThree")).ToString());

                result.Add(reader.GetValue(reader.GetOrdinal("typeFour")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("endTimeFour")).ToString());

                result.Add(reader.GetValue(reader.GetOrdinal("typeFive")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("endTimeFive")).ToString());
            }
        }
        connection.Close();
        return result;
    }
    public static List<string> GetStaffInformation(string login)
    {
        List<string> result = new List<string>();
        connection.Open();
        DbDataReader reader = ReaderCommand("*", "staff", $"login = '{login}'");
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                result.Add(reader.GetValue(reader.GetOrdinal("one")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("two")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("three")).ToString());
            }
        }
        connection.Close();
        return result;
    }
    public static List<string> GetCustomerInformation(string login, int number)
    {
        List<string> result = new List<string>();
        connection.Open();
        DbDataReader reader = ReaderCommand("*", "customers", $"login = '{login}' AND number = {number}");
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                result.Add(reader.GetValue(reader.GetOrdinal("type")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("willingType")).ToString());
            }
        }
        connection.Close();
        return result;
    }
}
