﻿using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public static Score instance;
    public Text score;
    public Text maxScore;
    private void Awake()
    {
        instance = this;
        PlayerPrefs.SetInt("score", 0);
    }
    private void Start()
    {
        ScoreUpdate(0);
    }
    public void ScoreUpdate(int delta)
    {
        PlayerPrefs.SetInt("score", PlayerPrefs.GetInt("score") + delta);
        score.text = "Score: " + PlayerPrefs.GetInt("score").ToString();
        maxScore.text = "Best score: " + PlayerPrefs.GetInt("maxScore").ToString();
        if (PlayerPrefs.GetInt("score") > PlayerPrefs.GetInt("maxScore"))
        {
            PlayerPrefs.SetInt("maxScore", PlayerPrefs.GetInt("score"));
        }
    }
}
