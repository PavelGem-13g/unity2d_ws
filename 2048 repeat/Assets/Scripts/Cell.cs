﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour
{
    public int value;
    public Sprite[] sprites;
    SpriteRenderer spriteRenderer;
    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        value = UnityEngine.Random.Range(0, 3);
    }
    private void Start()
    {
        ChangeValue(0);
    }
    public void ChangeValue(int delta)
    {
        value += delta;
        spriteRenderer.sprite = sprites[value];
        Score.instance.ScoreUpdate(Convert.ToInt32(Mathf.Pow(2, delta)));
    }
    public IEnumerator Move(Vector3 start, Vector3 end) 
    {
        transform.position = start;
        while (Vector3.Distance(transform.position, end)>0) 
        {
            transform.position = Vector3.MoveTowards(transform.position, end, 0.1f);
            yield return new WaitForSeconds(0.01f);
        }
    }
}
