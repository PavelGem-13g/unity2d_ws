﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Controller : MonoBehaviour
{
    GridCell[,] gridCells;
    public GameObject cellPrefab;
    public GameObject gridCellPrefab;
    public static Controller instance;
    public GameObject gameOver;
    private void Awake()
    {
        instance = this;
        gridCells = new GridCell[7, 7];
        for (int i = 1; i < 6; i++)
        {
            for (int j = 1; j < 6; j++)
            {
                GameObject temp = Instantiate(gridCellPrefab, new Vector3(i, j), Quaternion.identity, transform);
                gridCells[i, j] = temp.GetComponent<GridCell>();
                if (i == 0 || i == 6 || j == 0 || j == 6)
                {
                    temp.SetActive(false);
                }
            }
        }
        CreateCell();
    }
    public void CreateCell()
    {
        if (ChaeckGameOver())
        {
            int i = Random.Range(1, 6), j = Random.Range(1, 6);
            while (gridCells[i, j].cell != null)
            {
                i = Random.Range(1, 6);
                j = Random.Range(1, 6);
            }
            gridCells[i, j].cell = Instantiate(cellPrefab, Vector3.zero, Quaternion.identity, gridCells[i, j].transform);
            gridCells[i, j].cell.transform.localPosition = Vector3.zero;
        }
        else 
        {
            StartCoroutine(GameOver());
            //gameOver.SetActive(true);
        }
    }
    bool ChaeckGameOver() 
    {
        int k = 0;
        for (int i = 1; i < 6; i++)
        {
            for (int j = 1; j < 6; j++)
            {
                if (gridCells[i, j].cell == null)
                {
                    k++;
                }
            }
        }
        return k > 0;
    }
    IEnumerator GameOver() 
    {
        for (int i = 1; i < 6; i++)
        {
            for (int j = 1; j < 6; j++)
            {
                Destroy(gridCells[i, j].cell);
                yield return new WaitForSeconds(0.01f);
            }
        }
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public IEnumerator ToUp()
    {
        for (int f = 0; f < 4; f++)
        {
            for (int i = 5; i >= 1; i--)
            {
                for (int j = 4; j >= 1; j--)
                {
                    if (gridCells[i, j].cell != null && gridCells[i, j + 1].cell != null)
                    {
                        if (gridCells[i, j].cell.GetComponent<Cell>().value == gridCells[i, j + 1].cell.GetComponent<Cell>().value)
                        {
                            Destroy(gridCells[i, j].cell);
                            gridCells[i, j + 1].cell.GetComponent<Cell>().ChangeValue(1);
                            gridCells[i, j].cell = null;
                        }
                    }
                    if (gridCells[i, j].cell != null && gridCells[i, j + 1].cell == null)
                    {
                        gridCells[i, j].cell.transform.parent = gridCells[i, j + 1].transform;
                        gridCells[i, j + 1].cell = gridCells[i, j].cell;
                        gridCells[i, j + 1].cell.transform.localPosition = Vector3.zero;
                        gridCells[i, j].cell = null;
                    }
                }
            }
            yield return new WaitForSeconds(0.05f);
        }
        CreateCell();

    }
    public IEnumerator ToDown()
    {
        for (int f = 0; f < 4; f++)
        {
            for (int i = 1; i < 6; i++)
            {
                for (int j = 2; j < 6; j++)
                {
                    if (gridCells[i, j].cell != null && gridCells[i, j - 1].cell != null)
                    {
                        if (gridCells[i, j].cell.GetComponent<Cell>().value == gridCells[i, j - 1].cell.GetComponent<Cell>().value)
                        {
                            gridCells[i, j - 1].cell.GetComponent<Cell>().ChangeValue(1);
                            Destroy(gridCells[i, j].cell);
                            gridCells[i, j].cell = null;
                        }
                    }
                    if (gridCells[i, j].cell != null && gridCells[i, j - 1].cell == null)
                    {
                        gridCells[i, j].cell.transform.parent = gridCells[i, j - 1].transform;
                        gridCells[i, j - 1].cell = gridCells[i, j].cell;
                        gridCells[i, j - 1].cell.transform.localPosition = Vector3.zero;
                        //StartCoroutine(gridCells[i, j].cell.GetComponent<Cell>().Move(gridCells[i, j].transform.position, gridCells[i, j + 1].transform.position));
                        gridCells[i, j].cell = null;
                    }
                }
            }
            yield return new WaitForSeconds(0.05f);
        }
        CreateCell();
    }
    public IEnumerator ToRight()
    {
        for (int f = 0; f < 4; f++)
        {
            for (int i = 4; i >= 1; i--)
            {
                for (int j = 1; j < 6; j++)
                {
                    if (gridCells[i, j].cell != null && gridCells[i + 1, j].cell != null)
                    {
                        if (gridCells[i, j].cell.GetComponent<Cell>().value == gridCells[i + 1, j].cell.GetComponent<Cell>().value)
                        {
                            gridCells[i + 1, j].cell.GetComponent<Cell>().ChangeValue(1);
                            Destroy(gridCells[i, j].cell);
                            gridCells[i, j].cell = null;
                        }
                    }
                    if (gridCells[i, j].cell != null && gridCells[i + 1, j].cell == null)
                    {
                        gridCells[i, j].cell.transform.parent = gridCells[i + 1, j].transform;
                        gridCells[i + 1, j].cell = gridCells[i, j].cell;
                        gridCells[i + 1, j].cell.transform.localPosition = Vector3.zero;
                        //StartCoroutine(gridCells[i, j].cell.GetComponent<Cell>().Move(gridCells[i, j].transform.position, gridCells[i, j + 1].transform.position));
                        gridCells[i, j].cell = null;
                    }
                }
            }
            yield return new WaitForSeconds(0.05f);
        }
        CreateCell();
    }
    public IEnumerator ToLeft()
    {
        for (int f = 0; f < 4; f++)
        {
            for (int i = 2; i < 6; i++)
            {
                for (int j = 1; j < 6; j++)
                {
                    if (gridCells[i, j].cell != null && gridCells[i - 1, j].cell != null)
                    {
                        if (gridCells[i, j].cell.GetComponent<Cell>().value == gridCells[i - 1, j].cell.GetComponent<Cell>().value)
                        {
                            gridCells[i - 1, j].cell.GetComponent<Cell>().ChangeValue(1);
                            Destroy(gridCells[i, j].cell);
                            gridCells[i, j].cell = null;
                        }
                    }
                    if (gridCells[i, j].cell != null && gridCells[i - 1, j].cell == null)
                    {
                        gridCells[i, j].cell.transform.parent = gridCells[i - 1, j].transform;
                        gridCells[i - 1, j].cell = gridCells[i, j].cell;
                        gridCells[i - 1, j].cell.transform.localPosition = Vector3.zero;
                        //StartCoroutine(gridCells[i, j].cell.GetComponent<Cell>().Move(gridCells[i, j].transform.position, gridCells[i, j + 1].transform.position));
                        gridCells[i, j].cell = null;
                    }
                }
            }
            yield return new WaitForSeconds(0.05f);
        }
        CreateCell();
    }
}
