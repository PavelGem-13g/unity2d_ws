﻿using UnityEngine;

public class Swipe : MonoBehaviour
{
    Vector3 firstPosition;
    Vector3 secondPosition;
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            firstPosition = Input.mousePosition;
        }
        if (Input.GetMouseButtonUp(0))
        {
            secondPosition = Input.mousePosition;
            Calculating();
        }
    }
    void Calculating()
    {
        Vector3 swipe = secondPosition - firstPosition;
        if (Mathf.Abs(swipe.x) > Mathf.Abs(swipe.y))
        {
            if (swipe.x > 0)
            {
                StartCoroutine(Controller.instance.ToRight());
            }
            if (swipe.x < 0)
            {
                StartCoroutine(Controller.instance.ToLeft());
            }
        }
        else
        {
            if (swipe.y > 0)
            {
                StartCoroutine(Controller.instance.ToUp());
            }
            if (swipe.y < 0)
            {
                StartCoroutine(Controller.instance.ToDown());
            }
        }
    }
}
