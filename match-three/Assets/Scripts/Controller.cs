﻿using System.Collections;
using UnityEngine;

public class Controller : MonoBehaviour
{
    GameObject[,] cells;

    public GameObject crystalPrefab;
    public GameObject cellPrefab;
    public Vector3 firstPosition;
    public Vector3 secondPosition;

    private void Awake()
    {
        cells = new GameObject[12, 12];
        for (int i = 0; i < 12; i++)
        {
            for (int j = 0; j < 12; j++)
            {
                GameObject temp = Instantiate(cellPrefab, new Vector3(i, j), Quaternion.identity, transform);
                if (i == 0 || i == 11 || j == 0 || j == 11)
                {
                    temp.SetActive(false);
                }
                else
                {
                    temp.GetComponent<Cell>().crystal = Instantiate(crystalPrefab, temp.transform);
                    cells[i, j] = temp;
                }
            }
        }
        firstPosition = new Vector3(-1, -1);
    }

    public void SetPosition(Vector3 vector)
    {
        if (firstPosition == new Vector3(-1, -1))
        {
            firstPosition = vector;
        }
        else
        {
            secondPosition = vector;
            Stop();
            if (!IsOneType() && IsNear() && IsCombination())
            {
                Swap();
                Clear();
                firstPosition = new Vector3(-1, -1);
            }
            else
            {
                firstPosition = new Vector3(-1, -1);
            }
        }
    }
    bool IsOneType()
    {
        return cells[Mathf.RoundToInt(firstPosition.x), Mathf.RoundToInt(firstPosition.y)].GetComponent<Cell>().crystal.GetComponent<Ctystal>().type == cells[Mathf.RoundToInt(secondPosition.x), Mathf.RoundToInt(secondPosition.y)].GetComponent<Cell>().crystal.GetComponent<Ctystal>().type;
    }
    void Stop()
    {
        cells[Mathf.RoundToInt(firstPosition.x), Mathf.RoundToInt(firstPosition.y)].GetComponent<Cell>().crystal.GetComponent<Ctystal>().Stop();
        cells[Mathf.RoundToInt(secondPosition.x), Mathf.RoundToInt(secondPosition.y)].GetComponent<Cell>().crystal.GetComponent<Ctystal>().Stop();
    }
    bool IsNear()
    {
        return Vector3.Distance(firstPosition, secondPosition) < 1.1f;
    }

    void Swap()
    {
        GameObject temp = cells[Mathf.RoundToInt(firstPosition.x), Mathf.RoundToInt(firstPosition.y)].GetComponent<Cell>().crystal;
        cells[Mathf.RoundToInt(firstPosition.x), Mathf.RoundToInt(firstPosition.y)].GetComponent<Cell>().crystal =
            cells[Mathf.RoundToInt(secondPosition.x), Mathf.RoundToInt(secondPosition.y)].GetComponent<Cell>().crystal;

        cells[Mathf.RoundToInt(secondPosition.x), Mathf.RoundToInt(secondPosition.y)].GetComponent<Cell>().crystal = temp;

        cells[Mathf.RoundToInt(firstPosition.x), Mathf.RoundToInt(firstPosition.y)].GetComponent<Cell>().crystal.transform.parent = cells[Mathf.RoundToInt(firstPosition.x), Mathf.RoundToInt(firstPosition.y)].transform;
        cells[Mathf.RoundToInt(secondPosition.x), Mathf.RoundToInt(secondPosition.y)].GetComponent<Cell>().crystal.transform.parent = cells[Mathf.RoundToInt(secondPosition.x), Mathf.RoundToInt(secondPosition.y)].transform;

        cells[Mathf.RoundToInt(firstPosition.x), Mathf.RoundToInt(firstPosition.y)].GetComponent<Cell>().crystal.transform.localPosition = Vector3.zero;
        cells[Mathf.RoundToInt(secondPosition.x), Mathf.RoundToInt(secondPosition.y)].GetComponent<Cell>().crystal.transform.localPosition = Vector3.zero;
    }
    void Clear()
    {
        for (int i = 1; i < 10; i++)
        {
            for (int j = 1; j < 11; j++)
            {
                int _i = i, _j = j;
                int k = 0;
                if (cells[_i, _j].GetComponent<Cell>().crystal && cells[_i + 1, _j].GetComponent<Cell>().crystal)
                {
                    Ctystal one = cells[_i, _j].GetComponent<Cell>().crystal.GetComponent<Ctystal>();
                    Ctystal two = cells[_i + 1, _j].GetComponent<Cell>().crystal.GetComponent<Ctystal>();
                    while (one && two && _i != 10 && cells[_i, _j].GetComponent<Cell>().crystal && cells[_i + 1, _j].GetComponent<Cell>().crystal)
                    {
                        if (one.type == two.type)
                        {
                            k += 1;
                        }
                        one = cells[_i, _j].GetComponent<Cell>().crystal.GetComponent<Ctystal>();
                        two = cells[_i + 1, _j].GetComponent<Cell>().crystal.GetComponent<Ctystal>();
                        _i += 1;
                    }
                    if (k > 3)
                    {
                        for (int f = 0; f < k; f++)
                        {
                            Destroy(cells[i + f, j].GetComponent<Cell>().crystal.GetComponent<Ctystal>().gameObject);
                            cells[i + f, j].GetComponent<Cell>().crystal = null;
                            FindObjectOfType<Score>().ScoreUp(1);
                        }
                    }
                }
            }
        }
        for (int i = 1; i < 11; i++)
        {
            for (int j = 1; j < 10; j++)
            {
                int _i = i, _j = j;
                int k = 0;
                if (cells[_i, _j].GetComponent<Cell>().crystal && cells[_i, _j + 1].GetComponent<Cell>().crystal)
                {
                    Ctystal one = cells[_i, _j].GetComponent<Cell>().crystal.GetComponent<Ctystal>();
                    Ctystal two = cells[_i, _j + 1].GetComponent<Cell>().crystal.GetComponent<Ctystal>();
                    while (one && two && _j != 10 && cells[_i, _j].GetComponent<Cell>().crystal && cells[_i, _j + 1].GetComponent<Cell>().crystal)
                    {
                        if (one.type == two.type)
                        {
                            k += 1;
                        }
                        one = cells[_i, _j].GetComponent<Cell>().crystal.GetComponent<Ctystal>();
                        two = cells[_i, _j + 1].GetComponent<Cell>().crystal.GetComponent<Ctystal>();
                        _j += 1;
                    }
                    if (k > 3)
                    {
                        for (int f = 0; f < k; f++)
                        {
                            Destroy(cells[i, j + f].GetComponent<Cell>().crystal.GetComponent<Ctystal>().gameObject);
                            cells[i, j + f].GetComponent<Cell>().crystal = null;
                            FindObjectOfType<Score>().ScoreUp(1);
                        }
                    }
                }
            }
        }
        StartCoroutine(ToDown());
    }
    bool IsCombination()
    {
        Swap();
        for (int i = 1; i < 10; i++)
        {
            for (int j = 1; j < 11; j++)
            {
                int _i = i, _j = j;
                int k = 0;
                if (cells[_i, _j].GetComponent<Cell>().crystal && cells[_i + 1, _j].GetComponent<Cell>().crystal)
                {
                    Ctystal one = cells[_i, _j].GetComponent<Cell>().crystal.GetComponent<Ctystal>();
                    Ctystal two = cells[_i + 1, _j].GetComponent<Cell>().crystal.GetComponent<Ctystal>();
                    while (one && two && _i != 10 && cells[_i, _j].GetComponent<Cell>().crystal && cells[_i + 1, _j].GetComponent<Cell>().crystal)
                    {
                        if (one.type == two.type)
                        {
                            k += 1;
                        }
                        if (k >= 3 && (firstPosition == one.gameObject.transform.position || secondPosition == one.gameObject.transform.position ||
                            firstPosition == two.gameObject.transform.position || secondPosition == two.gameObject.transform.position))
                        {
                            return true;
                        }
                        one = cells[_i, _j].GetComponent<Cell>().crystal.GetComponent<Ctystal>();
                        two = cells[_i + 1, _j].GetComponent<Cell>().crystal.GetComponent<Ctystal>();
                        _i += 1;
                    }
                }
            }
        }
        for (int i = 1; i < 11; i++)
        {
            for (int j = 1; j < 10; j++)
            {
                int _i = i, _j = j;
                int k = 0;
                if (cells[_i, _j].GetComponent<Cell>().crystal && cells[_i, _j + 1].GetComponent<Cell>().crystal)
                {
                    Ctystal one = cells[_i, _j].GetComponent<Cell>().crystal.GetComponent<Ctystal>();
                    Ctystal two = cells[_i, _j + 1].GetComponent<Cell>().crystal.GetComponent<Ctystal>();
                    while (one && two && _j != 10 && cells[_i, _j].GetComponent<Cell>().crystal && cells[_i, _j + 1].GetComponent<Cell>().crystal)
                    {
                        if (one.type == two.type)
                        {
                            k += 1;
                        }
                        if (k >= 3 && (firstPosition == one.gameObject.transform.position || secondPosition == one.gameObject.transform.position ||
                            firstPosition == two.gameObject.transform.position || secondPosition == two.gameObject.transform.position))
                        {
                            return true;
                        }
                        one = cells[_i, _j].GetComponent<Cell>().crystal.GetComponent<Ctystal>();
                        two = cells[_i, _j + 1].GetComponent<Cell>().crystal.GetComponent<Ctystal>();
                        _j += 1;
                    }
                }
            }
        }
        Swap();
        return false;
    }
    IEnumerator ToDown()
    {
        for (int k = 0; k < 10; k++)
        {
            for (int i = 1; i < 11; i++)
            {
                for (int j = 2; j < 11; j++)
                {
                    if (cells[i, j].GetComponent<Cell>().crystal != null && cells[i, j - 1].GetComponent<Cell>().crystal == null)
                    {
                        cells[i, j].GetComponent<Cell>().crystal.transform.parent = cells[i, j - 1].transform;
                        cells[i, j - 1].GetComponent<Cell>().crystal = cells[i, j].GetComponent<Cell>().crystal;
                        cells[i, j - 1].GetComponent<Cell>().crystal.transform.localPosition = Vector3.zero;
                        cells[i, j].GetComponent<Cell>().crystal = null;
                    }
                }
            }
            yield return new WaitForSeconds(0.05f);
        }
        CreateNewCrystals();
    }

    void CreateNewCrystals()
    {
        for (int i = 1; i < 11; i++)
        {
            if (cells[i, 10].GetComponent<Cell>().crystal == null)
            {
                cells[i, 10].GetComponent<Cell>().crystal = Instantiate(crystalPrefab, cells[i, 10].transform);
                cells[i, 10].GetComponent<Cell>().crystal.transform.localPosition = Vector3.zero;
            }
        }
        StartCoroutine(ToDown());
    }
}

