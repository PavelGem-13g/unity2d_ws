﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    Text text;
    private void Awake()
    {
        text = GetComponent<Text>();
    }
    private void Start()
    {
        PlayerPrefs.SetInt("score", 0);
        ScoreUp(0);
    }
    public void ScoreUp(int delta) 
    {
        PlayerPrefs.SetInt("score", PlayerPrefs.GetInt("score")+ delta);
        text.text = "Score: +\n" + PlayerPrefs.GetInt("score");
    }
}
