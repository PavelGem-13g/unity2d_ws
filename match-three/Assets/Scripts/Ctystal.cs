﻿using System.Collections;
using UnityEngine;

public class Ctystal : MonoBehaviour
{
    public int type;
    SpriteRenderer image;
    public Sprite[] sprites;
    private bool isRotate;

    private void Start()
    {
        type = Random.Range(0, sprites.Length);
        image = GetComponent<SpriteRenderer>();
        image.sprite = sprites[type];
        StartCoroutine(Rotate());
        isRotate = false;
    }
    private void OnMouseUp()
    {
        isRotate = true;
        FindObjectOfType<Controller>().SetPosition(transform.position);
    }
    IEnumerator Rotate()
    {
        while (true)
        {
            if (isRotate)
            {
                transform.Rotate(new Vector3(0, 0, 2));
            }
            yield return new WaitForSeconds(0.01f);
        }
    }
    public void Stop()
    {
        isRotate = false;
    }
}
