﻿using UnityEngine;

public class Grid : MonoBehaviour
{
    public GameObject gridPrefab;
    public GridCell[,] gridCells;

    void Start()
    {
        gridCells = new GridCell[10, 10];
        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                GameObject temp = Instantiate(gridPrefab, new Vector3(i, j), Quaternion.identity, transform);
                gridCells[i, j] = temp.GetComponent<GridCell>();
                temp.GetComponent<SpriteRenderer>().sortingOrder = -30;
            }
        }
    }
    public void Clear()
    {
        for (int i = 0; i < 10; i++)
        {
            int k = 0;
            for (int j = 0; j < 10; j++)
            {
                if (gridCells[i,j].cell!=null) 
                {
                    k++;
                }
            }
            if (k==10)
            {
                FindObjectOfType<Score>().ScoreUp(30);
                for (int j = 0; j < 10; j++)
                {
                    gridCells[i, j].cell.GetComponent<Cell>().Destroy();
                }
            }
        }
        for (int i = 0; i < 10; i++)
        {
            int k = 0;
            for (int j = 0; j < 10; j++)
            {
                if (gridCells[j, i].cell!=null)
                {
                    k++;
                }
            }
            if (k == 10)
            {
                for (int j = 0; j < 10; j++)
                {
                    gridCells[i, j].cell.GetComponent<Cell>().Destroy();
                }
            }
        }
    }
}
