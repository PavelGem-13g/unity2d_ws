﻿using System.Collections;
using UnityEngine;

public class Cell : MonoBehaviour
{
    Grid grid;

    private void Awake()
    {
        grid = FindObjectOfType<Grid>();
    }
    public bool IsOnGround()
    {
        if (!(transform.position.x < 10 && transform.position.x >= 0 &&
            transform.position.y < 10 && transform.position.y >= 0))
        {
            return false;
        }
        return !grid.gridCells[Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y)].GetComponent<GridCell>().cell;
    }
    public void ChangeParent() 
    {
        GridCell temp = grid.gridCells[Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y)].GetComponent<GridCell>();
        temp.cell = gameObject;
        transform.parent = temp.transform;
        GetComponent<SpriteRenderer>().sortingOrder = -10;
    }
    public void Destroy() 
    {
        StartCoroutine(DestroyCoroutine());
    }
    IEnumerator DestroyCoroutine() 
    {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        while (spriteRenderer.color.a>0) 
        {
            spriteRenderer.color = new Color(spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, spriteRenderer.color.a-0.01f);
            yield return new WaitForSeconds(0.01f);
        }
        Destroy(gameObject);
    }
}
