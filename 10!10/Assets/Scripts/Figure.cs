﻿using System.Collections;
using UnityEngine;

public class Figure : MonoBehaviour
{
    public Cell[] cells;
    public Vector3 startPosition;
    public Main main;
    private void Awake()
    {
        main = FindObjectOfType<Main>();
        startPosition = transform.position;
    }
    private void OnMouseDrag()
    {
        if (!main.isEnd)
        {
            transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition) + new Vector3(0, 0, 10);
            transform.localScale = new Vector3(1.2f, 1.2f);
        }
    }
    private void OnMouseUp()
    {
        transform.position = new Vector3(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y));
        transform.localScale = Vector3.one;
        if (isAnyPlace())
        {
            FindObjectOfType<Score>().ScoreUp(10);
            Suicide();
            if (!main.isEnd)
            {
                FindObjectOfType<Grid>().Clear();
            }
        }
        else
        {
            StartCoroutine(ToStartPosition());
        }
    }
    public bool isAnyPlace()
    {
        for (int i = 0; i < cells.Length; i++)
        {
            if (!cells[i].IsOnGround())
            {
                return false;
            }
        }
        return true;
    }
    public void Suicide()
    {
        Main main = FindObjectOfType<Main>();
        main.countOfFigures--;
        foreach (var item in cells)
        {
            item.ChangeParent();
        }
        if (main.countOfFigures == 0)
        {
            main.CreateFigures();
        }
        Destroy(gameObject);
    }
    IEnumerator ToStartPosition()
    {
        while (Vector3.Distance(transform.position, startPosition) > 0)
        {
            transform.position = Vector3.MoveTowards(transform.position, startPosition, 1f);
            yield return new WaitForSeconds(0.01f);
        }
        main.IsGameOver();
    }
}
