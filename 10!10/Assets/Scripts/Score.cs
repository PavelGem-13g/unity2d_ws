﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public Text score;
    public Text bestScore;
    private void Start()
    {
        PlayerPrefs.SetInt("score", 0);
        ScoreUp(0);
    }
    public void ScoreUp(int delta) 
    {
        PlayerPrefs.SetInt("score", PlayerPrefs.GetInt("score") + delta);
        if (PlayerPrefs.GetInt("score")> PlayerPrefs.GetInt("bestScore"))
        {
            PlayerPrefs.SetInt("bestScore", PlayerPrefs.GetInt("score"));
        }
        score.text = "Score:\n" + PlayerPrefs.GetInt("score").ToString();
        bestScore.text = "Best score:\n" + PlayerPrefs.GetInt("bestScore").ToString();
    }
}
