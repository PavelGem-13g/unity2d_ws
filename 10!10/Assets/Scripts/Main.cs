﻿using UnityEngine;
using UnityEngine.Events;

public class Main : MonoBehaviour
{
    public GameObject[] prefabs;
    public GameObject[] positions;

    public int countOfFigures;

    public bool isEnd;

    public UnityEvent gameOver;

    private void Awake()
    {
        isEnd = false;
        countOfFigures = 3;
    }
    private void Start()
    {
        CreateFigures();
    }
    public void CreateFigures()
    {
        if (!isEnd)
        {
            for (int i = 0; i < positions.Length; i++)
            {
                Instantiate(prefabs[Random.Range(0, prefabs.Length)], positions[i].transform.position, Quaternion.identity, transform);
            }
            countOfFigures = 3;
        }
    }
    public bool IsGameOver()
    {
        int k = 0;
        foreach (var item in FindObjectsOfType<Figure>())
        {
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    item.gameObject.transform.position = new Vector3(i, j);
                    if (item.isAnyPlace())
                    {
                        k++;
                    }
                }
            }
            item.gameObject.transform.position = item.startPosition;
        }
        if (k > 0)
        {
            return false;
        }
        isEnd = true;
        gameOver.Invoke();
        return true;
    }
}
