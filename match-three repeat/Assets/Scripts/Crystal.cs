﻿using System.Collections;
using UnityEngine;

public class Crystal : MonoBehaviour
{
    public int value;

    public Sprite[] sprites;

    private void Awake()
    {
        value = Random.Range(0, sprites.Length);
        GetComponent<SpriteRenderer>().sprite = sprites[value];
    }

    public IEnumerator Rotate()
    {
        while (true)
        {
            gameObject.transform.Rotate(new Vector3(0, 0, 2));
            yield return new WaitForSeconds(0.01f);
        }
    }
    private void OnMouseDown()
    {
        StartCoroutine(Rotate());
        Controller.instance.SetPosition(transform.position);
    }
}
