﻿using System.Collections;
using UnityEngine;

public class Controller : MonoBehaviour
{
    public static Controller instance;
    Cell[,] cells;
    public GameObject cellPrefab;
    public GameObject crystalPrefab;
    Vector3 firstPosition;
    Vector3 secondPosition;
    int nonChanged;
    public GameObject gameOver;

    private void Awake()
    {
        firstPosition = new Vector3(-1, -1);
        instance = this;
        cells = new Cell[12, 12];
        nonChanged = 0;
    }
    private void Start()
    {
        for (int i = 0; i < 12; i++)
        {
            for (int j = 0; j < 12; j++)
            {
                GameObject temp = Instantiate(cellPrefab, new Vector3(i, j), Quaternion.identity, transform);
                cells[i, j] = temp.GetComponent<Cell>();
                cells[i, j].crystal = Instantiate(crystalPrefab, new Vector3(i, j), Quaternion.identity, cells[i, j].transform).GetComponent<Crystal>();
                if (i == 0 || j == 0 || i == 11 || j == 11)
                {
                    temp.gameObject.SetActive(false);
                }
            }
        }
        Clear();
    }
    public void SetPosition(Vector3 value)
    {
        if (firstPosition == new Vector3(-1, -1))
        {
            firstPosition = value;
        }
        else
        {
            secondPosition = value;
            Stop();
            if (Check() && IsCombination())
            {
                Swap();
                Clear();
            }
            else
            {
                nonChanged += 1;
            }
            if (nonChanged>3)
            {
                gameOver.SetActive(true);
            }
            firstPosition = new Vector3(-1, -1);
        }
    }
    bool Check()
    {
        return cells[Mathf.RoundToInt(firstPosition.x), Mathf.RoundToInt(firstPosition.y)].crystal.value != cells[Mathf.RoundToInt(secondPosition.x), Mathf.RoundToInt(secondPosition.y)].crystal.value &&
            Vector3.Distance(firstPosition, secondPosition) < 1.1f;
    }
    bool IsCombination()
    {
        Swap();
        for (int i = 1; i < 10; i++)
        {
            for (int j = 1; j < 11; j++)
            {
                if (cells[i, j].crystal)
                {
                    int value = cells[i, j].crystal.value;
                    int k = 0;
                    int _i = i, _j = j;
                    while (_i < 11 &&
                        (cells[_i, _j].crystal != null && cells[_i + 1, _j].crystal != null) &&
                        (cells[_i, _j].crystal.value == cells[_i + 1, _j].crystal.value) &&
                        cells[_i + 1, _j].crystal.value == value)
                    {
                        k += 1;
                        if (k > 1 && (_i == Mathf.RoundToInt(firstPosition.x) && _j == Mathf.RoundToInt(firstPosition.y) ||
                            _i == Mathf.RoundToInt(secondPosition.x) && _j == Mathf.RoundToInt(secondPosition.y)) ||
                            (_i + 1 == Mathf.RoundToInt(firstPosition.x) && _j == Mathf.RoundToInt(firstPosition.y) ||
                            _i + 1 == Mathf.RoundToInt(secondPosition.x) && _j == Mathf.RoundToInt(secondPosition.y)))
                        {
                            Swap();
                            Debug.Log("1");
                            return true;
                        }
                        _i += 1;

                    }
                }
            }
        }

        for (int i = 1; i < 11; i++)
        {
            for (int j = 1; j < 11; j++)
            {
                if (cells[i, j].crystal)
                {
                    int value = cells[i, j].crystal.value;
                    int k = 0;
                    int _i = i, _j = j;
                    while (_j < 11 &&
                        (cells[_i, _j].crystal != null && cells[_i, _j + 1].crystal != null) &&
                        (cells[_i, _j].crystal.value == cells[_i, _j + 1].crystal.value) &&
                        cells[_i, _j + 1].crystal.value == value)
                    {
                        k += 1;
                        if (k > 1 && (_i == Mathf.RoundToInt(firstPosition.x) && _j == Mathf.RoundToInt(firstPosition.y) ||
                            _i == Mathf.RoundToInt(secondPosition.x) && _j == Mathf.RoundToInt(secondPosition.y)) ||
                            (_i == Mathf.RoundToInt(firstPosition.x) && _j + 1 == Mathf.RoundToInt(firstPosition.y) ||
                            _i == Mathf.RoundToInt(secondPosition.x) && _j + 1 == Mathf.RoundToInt(secondPosition.y)))
                        {
                            Debug.Log("2");
                            Swap();
                            return true;
                        }
                        _j += 1;
                    }
                }
            }
        }
        Swap();
        return false;
    }
    void Stop()
    {
        cells[Mathf.RoundToInt(firstPosition.x), Mathf.RoundToInt(firstPosition.y)].crystal.StopAllCoroutines();
        cells[Mathf.RoundToInt(secondPosition.x), Mathf.RoundToInt(secondPosition.y)].crystal.StopAllCoroutines();
    }

    void Swap()
    {
        Crystal temp = cells[Mathf.RoundToInt(firstPosition.x), Mathf.RoundToInt(firstPosition.y)].crystal;
        cells[Mathf.RoundToInt(firstPosition.x), Mathf.RoundToInt(firstPosition.y)].crystal =
            cells[Mathf.RoundToInt(secondPosition.x), Mathf.RoundToInt(secondPosition.y)].crystal;

        cells[Mathf.RoundToInt(secondPosition.x), Mathf.RoundToInt(secondPosition.y)].crystal = temp;

        cells[Mathf.RoundToInt(firstPosition.x), Mathf.RoundToInt(firstPosition.y)].crystal.transform.parent = cells[Mathf.RoundToInt(firstPosition.x), Mathf.RoundToInt(firstPosition.y)].transform;
        cells[Mathf.RoundToInt(secondPosition.x), Mathf.RoundToInt(secondPosition.y)].crystal.transform.parent = cells[Mathf.RoundToInt(secondPosition.x), Mathf.RoundToInt(secondPosition.y)].transform;

        cells[Mathf.RoundToInt(firstPosition.x), Mathf.RoundToInt(firstPosition.y)].crystal.transform.localPosition = Vector3.zero;
        cells[Mathf.RoundToInt(secondPosition.x), Mathf.RoundToInt(secondPosition.y)].crystal.transform.localPosition = Vector3.zero;

    }

    void Clear()
    {
        StopAllCoroutines();
        for (int i = 1; i < 10; i++)
        {
            for (int j = 1; j < 11; j++)
            {
                if (cells[i, j].crystal)
                {
                    int value = cells[i, j].crystal.value;
                    int k = 0;
                    int _i = i, _j = j;
                    while (_i < 10 &&
                        (cells[_i, _j].crystal != null && cells[_i + 1, _j].crystal != null) &&
                        (cells[_i, _j].crystal.value == cells[_i + 1, _j].crystal.value) &&
                        cells[_i + 1, _j].crystal.value == value)
                    {
                        k += 1;
                        _i += 1;

                    }
                    if (k > 1)
                    {
                        for (int f = 0; f < k + 1; f++)
                        {
                            Score.instance.ScoreUp(1);
                            Destroy(cells[i + f, j].crystal.gameObject);
                            cells[i + f, j].crystal = null;
                        }
                    }
                }
            }
        }

        for (int i = 1; i < 11; i++)
        {
            for (int j = 1; j < 10; j++)
            {
                if (cells[i, j].crystal)
                {
                    int value = cells[i, j].crystal.value;
                    int k = 0;
                    int _i = i, _j = j;
                    while (_j < 10 &&
                        (cells[_i, _j].crystal != null && cells[_i, _j + 1].crystal != null) &&
                        (cells[_i, _j].crystal.value == cells[_i, _j + 1].crystal.value) &&
                        cells[_i, _j + 1].crystal.value == value)
                    {
                        k += 1;
                        _j += 1;
                    }
                    if (k > 1)
                    {
                        for (int f = 0; f < k + 1; f++)
                        {
                            Score.instance.ScoreUp(1);
                            Destroy(cells[i, j + f].crystal.gameObject);
                            cells[i, j + f].crystal = null;
                        }
                    }
                }
            }
        }
        StartCoroutine(ToDown());
    }
    IEnumerator ToDown()
    {
        for (int f = 0; f < 10; f++)
        {
            for (int i = 1; i < 11; i++)
            {
                for (int j = 2; j < 11; j++)
                {
                    if (cells[i, j].crystal != null && cells[i, j - 1].crystal == null)
                    {
                        cells[i, j - 1].crystal = cells[i, j].crystal;
                        cells[i, j - 1].crystal.transform.parent = cells[i, j - 1].transform;
                        cells[i, j - 1].crystal.transform.localPosition = Vector3.zero;
                        cells[i, j].crystal = null;
                    }
                }
                yield return new WaitForSeconds(0.001f);
            }
        }
        CreateCells();
    }
    void CreateCells()
    {
        for (int i = 1; i < 11; i++)
        {
            if (cells[i, 10].crystal == null)
            {
                cells[i, 10].crystal = Instantiate(crystalPrefab, new Vector3(i, 10), Quaternion.identity, cells[i, 10].transform).GetComponent<Crystal>();
            }
        }
        Clear();
        StartCoroutine(ToDown());
    }
}
