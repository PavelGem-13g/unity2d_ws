﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour
{
    public Sprite[] sprites;
    public int value;
    private void Awake()
    {
        value = Random.Range(0,sprites.Length);
        GetComponent<SpriteRenderer>().sprite = sprites[value];
    }
    private void OnMouseUp()
    {
        StartCoroutine(Rotate());
        Controller.instance.SetPosition(transform.position);
    }
    IEnumerator Rotate() 
    {
        while (true)
        {
            transform.Rotate(new Vector3(0,0,2));
            yield return new WaitForEndOfFrame();
        }
    }
}
