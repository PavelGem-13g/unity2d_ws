﻿using System.Collections;
using UnityEngine;

public class Controller : MonoBehaviour
{
    public static Controller instance;

    public GridCell[,] gridCells;

    public GameObject cellPrefab;
    public GameObject gridCellPrefab;

    Vector3 firstPosition;
    Vector3 secondPosition;
    private void Awake()
    {
        instance = this;
        firstPosition = new Vector3(-1, -1);
        gridCells = new GridCell[12, 12];
    }
    private void Start()
    {
        for (int i = 0; i < 12; i++)
        {
            for (int j = 0; j < 12; j++)
            {
                gridCells[i, j] = Instantiate(gridCellPrefab, new Vector3(i, j), Quaternion.identity, transform).GetComponent<GridCell>();
                gridCells[i, j].cell = Instantiate(cellPrefab, new Vector3(i, j), Quaternion.identity, gridCells[i, j].transform).GetComponent<Cell>();
                if (i == 0 || j == 0 || i == 11 || j == 11)
                {
                    gridCells[i, j].gameObject.SetActive(false);
                }
            }
        }
        Clear();
    }
    public void SetPosition(Vector3 vector)
    {
        if (firstPosition == new Vector3(-1, -1))
        {
            firstPosition = vector;
        }
        else
        {
            secondPosition = vector;
            Stop();
            if (Check() && IsCombination())
            {
                Swap();
                Clear();
            }
            firstPosition = new Vector3(-1, -1);
        }
    }
    void Swap()
    {
        Vector2Int fp = new Vector2Int(Mathf.RoundToInt(firstPosition.x), Mathf.RoundToInt(firstPosition.y));
        Vector2Int sp = new Vector2Int(Mathf.RoundToInt(secondPosition.x), Mathf.RoundToInt(secondPosition.y));

        Cell temp = gridCells[fp.x, fp.y].cell;
        gridCells[fp.x, fp.y].cell = gridCells[sp.x, sp.y].cell;
        gridCells[sp.x, sp.y].cell = temp;

        gridCells[fp.x, fp.y].cell.gameObject.transform.parent = gridCells[fp.x, fp.y].transform;
        gridCells[sp.x, sp.y].cell.gameObject.transform.parent = gridCells[sp.x, sp.y].transform;

        gridCells[fp.x, fp.y].cell.gameObject.transform.localPosition = Vector3.zero;
        gridCells[sp.x, sp.y].cell.gameObject.transform.localPosition = Vector3.zero;
    }
    bool Check()
    {
        Vector2Int fp = new Vector2Int(Mathf.RoundToInt(firstPosition.x), Mathf.RoundToInt(firstPosition.y));
        Vector2Int sp = new Vector2Int(Mathf.RoundToInt(secondPosition.x), Mathf.RoundToInt(secondPosition.y));
        return Vector3.Distance(firstPosition, secondPosition) < 1.1f &&
            gridCells[fp.x, fp.y].cell.value != gridCells[sp.x, sp.y].cell.value;
    }
    void Clear()
    {
        for (int i = 1; i < 11; i++)
        {
            for (int j = 1; j < 11; j++)
            {
                if (gridCells[i, j].cell)
                {
                    int _i = i, _j = j;
                    int k = 0;
                    int value = gridCells[i, j].cell.value;
                    while (_i < 11 &&
                        (gridCells[_i, _j].cell && gridCells[_i + 1, _j].cell) &&
                        (gridCells[_i, _j].cell.value == gridCells[_i + 1, _j].cell.value) &&
                        (gridCells[_i + 1, _j].cell.value == value))
                    {
                        k += 1;
                        _i += 1;
                    }
                    if (k > 1)
                    {
                        for (int f = 0; f < k + 1; f++)
                        {
                            Destroy(gridCells[i + f, j].cell.gameObject);
                            gridCells[i + f, j].cell = null;
                        }
                    }
                }
                if (gridCells[i, j].cell)
                {
                    int _i = i, _j = j;
                    int k = 0;
                    int value = gridCells[i, j].cell.value;
                    while (_j < 11 &&
                        (gridCells[_i, _j].cell && gridCells[_i, _j + 1].cell) &&
                        (gridCells[_i, j].cell.value == gridCells[_i, _j + 1].cell.value) &&
                        (gridCells[_i, _j + 1].cell.value == value))
                    {
                        k += 1;
                        _j += 1;
                    }
                    if (k > 1)
                    {
                        for (int f = 0; f < k + 1; f++)
                        {
                            Destroy(gridCells[i, j + f].cell.gameObject);
                            gridCells[i, j + f].cell = null;
                        }
                    }
                }
            }
        }
        StartCoroutine(ToDown());
    }
    bool IsCombination()
    {
        Swap();
        Vector2Int fp = new Vector2Int(Mathf.RoundToInt(firstPosition.x), Mathf.RoundToInt(firstPosition.y));
        Vector2Int sp = new Vector2Int(Mathf.RoundToInt(secondPosition.x), Mathf.RoundToInt(secondPosition.y));
        for (int i = 1; i < 11; i++)
        {
            for (int j = 1; j < 11; j++)
            {
                if (gridCells[i, j].cell)
                {
                    int _i = i, _j = j;
                    int k = 0;
                    int value = gridCells[i, j].cell.value;
                    bool isChanged = false;
                    while (_i < 11 &&
                        (gridCells[_i, _j].cell && gridCells[_i + 1, _j].cell) &&
                        (gridCells[_i, _j].cell.value == gridCells[_i + 1, _j].cell.value) &&
                        (gridCells[_i + 1, _j].cell.value == value))
                    {
                        if ((_i == fp.x && _j == fp.y) || (_i == sp.x && _j == sp.y) || (_i + 1 == fp.x && _j == fp.y) || (_i + 1 == sp.x && _j == sp.y))
                        {
                            isChanged = true;
                        }
                        k += 1;
                        _i += 1;
                    }
                    if (k > 1 && isChanged)
                    {
                        Swap();
                        return true;
                    }
                }
                if (gridCells[i, j].cell)
                {
                    int _i = i, _j = j;
                    int k = 0;
                    int value = gridCells[i, j].cell.value;
                    while (_j < 11 &&
                        (gridCells[_i, _j].cell && gridCells[_i, _j + 1].cell) &&
                        (gridCells[_i, j].cell.value == gridCells[_i, _j + 1].cell.value) &&
                        (gridCells[_i, _j + 1].cell.value == value))
                    {
                        k += 1;
                        _j += 1;
                    }
                    if (k > 1)
                    {
                        Swap();
                        return true;
                    }
                }
            }
        }
        Swap();
        return false;
    }
    IEnumerator ToDown()
    {
        for (int f = 0; f < 10; f++)
        {
            for (int i = 1; i < 11; i++)
            {
                for (int j = 2; j < 11; j++)
                {
                    if (gridCells[i, j].cell && !gridCells[i, j - 1].cell)
                    {
                        gridCells[i, j - 1].cell = gridCells[i, j].cell;
                        gridCells[i, j].cell = null;

                        gridCells[i, j - 1].cell.gameObject.transform.parent = gridCells[i, j - 1].transform;

                        gridCells[i, j - 1].cell.gameObject.transform.localPosition = Vector3.zero;
                    }
                }
                yield return new WaitForEndOfFrame();
            }
        }
        CreateNewCellS();
    }
    void CreateNewCellS() 
    {
        for (int i = 1; i < 11; i++)
        {
            if (!gridCells[i,10].cell) 
            {
                gridCells[i,10].cell = Instantiate(cellPrefab, new Vector3(i, 10), Quaternion.identity, transform).GetComponent<Cell>();
            }
        }
        Clear();
    }
    void Stop() 
    {
        foreach (var item in FindObjectsOfType<Cell>())
        {
            item.StopAllCoroutines();
        }
    }
}
