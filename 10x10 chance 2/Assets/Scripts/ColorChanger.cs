﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChanger : MonoBehaviour
{
    private void Start()
    {
        ColorUpdate();
    }
    public void ColorUpdate() 
    {
        Element[] elements = FindObjectsOfType<Element>();
        Dot[] dots = FindObjectsOfType<Dot>();
        foreach (var item in elements)
        {
            item.ColorUpdate();
        }
        foreach (var item in dots)
        {
            item.ColorUpdate();
        }

        if (PlayerPrefs.GetInt("color") == 0)
            {
                GetComponent<Camera>().backgroundColor = Color.white;
            }
        if (PlayerPrefs.GetInt("color") == 1)
        {
            GetComponent<Camera>().backgroundColor = Color.black;
        }
    }
}
