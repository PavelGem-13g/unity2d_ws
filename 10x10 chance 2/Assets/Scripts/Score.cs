﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    Text text;
    public string namePlrefs;
    private void Awake()
    {
        text = GetComponent<Text>();
        TextUpdate();
    }
    public void TextUpdate()
    {
        text.text = PlayerPrefs.GetInt(namePlrefs).ToString();
    }
}
