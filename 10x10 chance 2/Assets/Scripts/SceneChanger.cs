﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour
{
    public void Reload() 
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void LoadScene(string name) 
    {
        SceneManager.LoadScene(name);
    }
    public void LoadScene(int number)
    {
        SceneManager.LoadScene(number);
    }
    public void Quit() 
    {
        Application.Quit();
    }
}
