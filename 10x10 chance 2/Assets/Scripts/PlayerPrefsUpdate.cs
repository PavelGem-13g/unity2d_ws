﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPrefsUpdate : MonoBehaviour
{
    public string namePlrefs;
    public int value;
    public void PrefsUpdate() 
    {
        PlayerPrefs.SetInt(namePlrefs,value);
    }
}
