﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
public class MainSCR : MonoBehaviour
{
    public GameObject dot;
    public GameObject[,] dots;
    public GameObject[] figures;
    public GameObject[] posFigures;
    public GameObject parentDot;
    public bool isPause = false;
    ColorChanger colorChanger;
    public UnityEvent gameOver; 
    private void Awake()
    {
        colorChanger = FindObjectOfType<ColorChanger>();
    }
    private void Start()
    {
        PlayerPrefs.SetInt("score", 0);
        ScoreUp(0);
        dots = new GameObject[10, 10];
        CreateGrid();
        CreateFigures();
    }
    public void Pause() 
    {
        isPause = true;
    }
    public void UnPause()
    {
        isPause = false;
    }
    void CreateGrid()
    {
        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                dots[i, j] = Instantiate(dot, transform);
                dots[i, j].transform.localPosition = new Vector3(i, j);
            }
        }
    }
    void CreateFigures()
    {
        for (int i = 0; i < posFigures.Length; i++)
        {
            Instantiate(figures[Random.Range(0, figures.Length)], posFigures[i].transform.position, Quaternion.identity);
        }
        colorChanger.ColorUpdate();
    }
    public void CheckFigures()
    {
        Figure[] figures = FindObjectsOfType<Figure>();
        if (figures.Length <= 1)
        {
            CreateFigures();
        }
        StartCoroutine(CheckGameOver());
        /*if (IsGameOver())
        {
            Debug.Log("GG");
        }*/
    }
    void DestroyElements()
    {
        foreach (var item in dots)
        {
            if (item.GetComponent<Dot>().isDestroy)
            {
                item.GetComponent<Dot>().Destroy();
            }
        }
    }
    public void CheckDestroy()
    {
        int num = 0;
        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                if (dots[i, j].GetComponent<Dot>().element)
                {
                    num++;
                }
            }
            if (num == 10)
            {
                ScoreUp(10);
                for (int j = 0; j < 10; j++)
                {
                    dots[i, j].GetComponent<Dot>().isDestroy = true;
                }
            }
            num = 0;
        }
        for (int j = 0; j < 10; j++)
        {
            for (int i = 0; i < 10; i++)
            {
                if (dots[i, j].GetComponent<Dot>().element)
                {
                    num++;
                }
            }
            if (num == 10)
            {
                ScoreUp(10);
                for (int i = 0; i < 10; i++)
                {
                    dots[i, j].GetComponent<Dot>().isDestroy = true;
                }
            }
            num = 0;
        }
        DestroyElements();
    }
    IEnumerator CheckGameOver()
    {
        yield return new WaitForSeconds(0.1f);
        Figure[] figurs = GameObject.FindObjectsOfType<Figure>();

        int num = 0;
        if (figurs.Length != 0)
        {
            foreach (var item in figurs)
            {
                for (int x = 0; x < 10; x++)
                {
                    for (int y = 0; y < 10; y++)
                    {
                        Vector3 pos = new Vector3(x, y, 0);
                        item.transform.parent = parentDot.transform;
                        item.transform.localPosition = pos;

                        if (item.IsAnyPlace())
                        {
                            num++;
                        }                  
                    }
                }

                item.transform.position = item.startPosition;
            }
            if (num == 0)
            {
                gameOver.Invoke();
            }
        }
    }
    public void ScoreUp(int delta) 
    {
        PlayerPrefs.SetInt("score", PlayerPrefs.GetInt("score") + delta);
        if (PlayerPrefs.GetInt("score") > PlayerPrefs.GetInt("bestScore")) 
        {
            PlayerPrefs.SetInt("bestScore", PlayerPrefs.GetInt("score"));
        }
        Score[] score = FindObjectsOfType<Score>();
        foreach (var item in score)
        {
            item.TextUpdate();
        }
    }
}
