﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Element : MonoBehaviour
{
    MainSCR mainSCR;
    private void Awake()
    {
        mainSCR = FindObjectOfType<MainSCR>();
    }
    public bool IsOnVoidGrid() 
    {
        bool result = false;
        Vector3 position = mainSCR.gameObject.transform.InverseTransformPoint(transform.position);
        //Debug.Log(position);
        Vector2Int posiitionInArray = new Vector2Int(Mathf.RoundToInt(position.x), Mathf.RoundToInt(position.y));
        if (posiitionInArray.x >= 0 && posiitionInArray.x < 10 && posiitionInArray.y >= 0 && posiitionInArray.y < 10)
        {
            if (mainSCR.dots[posiitionInArray.x, posiitionInArray.y].GetComponent<Dot>().element == null)
            {
                result = true;
            }
        }
        return result;
    }
    public void NewParent() 
    {
        Vector3 position = mainSCR.gameObject.transform.InverseTransformPoint(transform.position);
        Vector2Int posiitionInArray = new Vector2Int(Mathf.RoundToInt(position.x), Mathf.RoundToInt(position.y));
        mainSCR.dots[posiitionInArray.x, posiitionInArray.y].GetComponent<Dot>().element = gameObject;
        transform.parent = mainSCR.dots[posiitionInArray.x, posiitionInArray.y].transform;
        transform.localPosition = Vector3.zero;
    }
    public void ColorUpdate() 
    {
        if (PlayerPrefs.GetInt("color") == 0) 
        {
            GetComponent<SpriteRenderer>().color = Color.red;
        }
        if (PlayerPrefs.GetInt("color") == 1) 
        {
            GetComponent<SpriteRenderer>().color = Color.grey;
        }
    }
}
