﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dot : MonoBehaviour
{
    public GameObject element;
    public bool isDestroy = false;

    public void Destroy()
    {
        StartCoroutine(Destroying());
        isDestroy = false;
    }
    IEnumerator Destroying() 
    {
        while (element.GetComponent<SpriteRenderer>().color.a>0) 
        {
            element.GetComponent<SpriteRenderer>().color = new Color(element.GetComponent<SpriteRenderer>().color.r, 
                element.GetComponent<SpriteRenderer>().color.g, 
                element.GetComponent<SpriteRenderer>().color.b, 
                element.GetComponent<SpriteRenderer>().color.a-0.01f);
            yield return new WaitForSeconds(0.01f);
        }
        Destroy(element);
    }
    public void ColorUpdate() 
    {
        if (PlayerPrefs.GetInt("color") == 0)
        {
            GetComponent<SpriteRenderer>().color = Color.black;
        }
        if (PlayerPrefs.GetInt("color") == 1)
        {
            GetComponent<SpriteRenderer>().color = Color.white;
        }
    }
}
