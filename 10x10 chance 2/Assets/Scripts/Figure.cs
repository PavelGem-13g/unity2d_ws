﻿using System.Collections;
using UnityEngine;

public class Figure : MonoBehaviour
{
    public GameObject[] elements;
    public Vector3 startPosition;
    MainSCR mainSCR;
    private void Start()
    {
        startPosition = transform.position;
        mainSCR = FindObjectOfType<MainSCR>();
    }
    private void OnMouseDrag()
    {
        if (!mainSCR.isPause)
            transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition) + new Vector3(0, 0, 10);
    }
    private void OnMouseDown()
    {
        if (!mainSCR.isPause)
            transform.localScale = new Vector3(1.3f, 1.3f, 0);
    }
    private void OnMouseUp()
    {
        if (IsAnyPlace())
        {
            transform.localScale = Vector3.one;
            foreach (var item in elements)
            {
                item.GetComponent<Element>().NewParent();
            }
            mainSCR.CheckFigures();
            mainSCR.CheckDestroy();
            mainSCR.ScoreUp(elements.Length);
            Destroy(gameObject);
        }
        else
        {
            transform.localScale = Vector3.one;
            StartCoroutine(ToStartPosition());
        }
    }
    IEnumerator ToStartPosition()
    {
        while (Vector3.Distance(transform.position, startPosition) > 0)
        {
            transform.position = Vector3.MoveTowards(transform.position, startPosition, 0.1f);
            yield return new WaitForSeconds(0.001f);
        }
    }
    public bool IsAnyPlace()
    {
        bool result = false;
        int num = 0;
        foreach (var item in elements)
        {
            if (item.GetComponent<Element>().IsOnVoidGrid())
            {
                num++;
            }
        }
        if (num == elements.Length)
        {
            result = true;
        }
        return result;
    }
}
