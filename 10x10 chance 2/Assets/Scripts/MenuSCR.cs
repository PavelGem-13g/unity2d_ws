﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MenuSCR : MonoBehaviour
{
    public Image[] images;
    UnityEvent turnOff;
    public void UnPause()
    {
        StartCoroutine(UnPauseCoroutine());
    }
    IEnumerator UnPauseCoroutine()
    {
        foreach (var item in images)
        {
                item.color = new Color(
                    item.color.r,
                    item.color.g,
                    item.color.b,
                    1);
        }
        while (IsAllMoreThen(0))
        {
            foreach (var item in images)
            {
                if (item.color.a > 0)
                {
                    item.color = new Color(
                        item.color.r,
                        item.color.g,
                        item.color.b,
                        item.color.a - 0.1f);
                }
            }
            yield return new WaitForSeconds(0.01f);
        }
        gameObject.SetActive(false);    
    }
    bool IsAllMoreThen(int value)
    {
        bool result = false;
        foreach (var item in images)
        {
            if (item.color.a > value)
            {
                result = true;
            }
        }
        return result;
    }
    public void Pause()
    {
        StartCoroutine(PauseCoroutine());
    }
    IEnumerator PauseCoroutine()
    {
        foreach (var item in images)
        {
            item.color = new Color(
                item.color.r,
                item.color.g,
                item.color.b,
                0);
        }
        while (IsAllLessThen(255))
        {
            foreach (var item in images)
            {
                if (item.color.a < 255)
                {
                    item.color = new Color(
                        item.color.r,
                        item.color.g,
                        item.color.b,
                        item.color.a + 0.1f);
                }
            }
            yield return new WaitForSeconds(0.01f);
        }        
    }
    bool IsAllLessThen(int value)
    {
        bool result = false;
        foreach (var item in images)
        {
            if (item.color.a < value)
            {
                result = true;
            }
        }
        return result;
    }
}
