﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public Text score;
    public Text maxScore;
    public static Score instance;
    private void Awake()
    {
        instance = this;
        PlayerPrefs.SetInt("score", 0);
    }
    private void Start()
    {
        ScoreUp(0);
    }

    public void ScoreUp(int delta) 
    {
        PlayerPrefs.SetInt("score", PlayerPrefs.GetInt("score") + delta);
        if (PlayerPrefs.GetInt("score")> PlayerPrefs.GetInt("bestScore")) 
        {
            PlayerPrefs.SetInt("bestScore", PlayerPrefs.GetInt("score"));
        }
        score.text = "Score: " + PlayerPrefs.GetInt("score").ToString();
        maxScore.text = "Best score: " + PlayerPrefs.GetInt("bestScore").ToString();
    }
}
