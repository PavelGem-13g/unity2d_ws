﻿using System.Collections;
using UnityEngine;

public class Cell : MonoBehaviour
{
    public bool IsAnyPlace()
    {
        if (!(transform.position.x < 10 && transform.position.y < 10 &&
            transform.position.x > -1 && transform.position.y > -1))
            return false;
        return !Controller.instance.gridCells[Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y)].cell;
    }
    public void ChangeParent()
    {
        transform.parent = Controller.instance.gridCells[Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y)].transform;
        Controller.instance.gridCells[Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y)].cell = this;
        transform.localPosition = Vector3.zero;
    }

    public IEnumerator Destroy()
    {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        while (spriteRenderer.color.a > 0)
        {
            spriteRenderer.color = new Color(spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, spriteRenderer.color.a - 0.01f);
            yield return new WaitForSeconds(0.01f);
        }
        Destroy(gameObject);
    }
}
