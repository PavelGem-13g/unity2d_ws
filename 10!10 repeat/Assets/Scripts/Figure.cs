﻿using System.Collections;
using UnityEngine;

public class Figure : MonoBehaviour
{
    public Cell[] array;

    public Vector3 startPosition;

    private void Awake()
    {
        startPosition = transform.position;
    }
    private void OnMouseDown()
    {
        if (!Controller.instance.isEnd)
        {
            transform.localScale = new Vector3(1.1f, 1.1f);
        }
    }
    private void OnMouseDrag()
    {
        if (!Controller.instance.isEnd)
        {
            transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition) + new Vector3(0, 0, 10);
        }
    }

    private void OnMouseUp()
    {
        transform.position = new Vector3(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y));
        if (IsAnyPlace())
        {
            Suicide();
        }
        else
        {
            StartCoroutine(ToStartPosition());
        }
        if (!Controller.instance.isEnd)
        {
            transform.localScale = Vector3.one;
        }
    }
    void Suicide()
    {
        Score.instance.ScoreUp(array.Length);
        Controller.instance.countOfFigures--;
        if (Controller.instance.countOfFigures == 0)
        {
            Controller.instance.CreateFigures();
        }
        foreach (var item in array)
        {
            item.ChangeParent();
        }
        Controller.instance.Clear();
        Controller.instance.IsGameOver();
        Destroy(gameObject);
    }
    public bool IsAnyPlace()
    {
        foreach (var item in array)
        {
            if (!item.IsAnyPlace())
            {
                return false;
            }
        }
        return true;
    }

    IEnumerator ToStartPosition()
    {
        while (Vector3.Distance(transform.position, startPosition) > 0)
        {
            transform.position = Vector3.MoveTowards(transform.position, startPosition, 1);
            yield return new WaitForSeconds(0.01f);
        }
    }
}
