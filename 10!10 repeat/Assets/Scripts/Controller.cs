﻿using UnityEngine;

public class Controller : MonoBehaviour
{
    public static Controller instance;
    public GridCell[,] gridCells;

    public GameObject gridCellPrefab;

    public GameObject[] positions;

    public GameObject[] figurePrefab;
    public int countOfFigures = 3;

    public bool isEnd;

    public GameObject gameOver;

    private void Awake()
    {
        instance = this;
        isEnd = false;
    }
    private void Start()
    {
        gridCells = new GridCell[10, 10];
        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                GameObject temp = Instantiate(gridCellPrefab, new Vector3(i, j), Quaternion.identity, transform);
                gridCells[i, j] = temp.GetComponent<GridCell>();
            }
        }
        CreateFigures();
    }
    public void CreateFigures()
    {
        for (int i = 0; i < positions.Length; i++)
        {
            Instantiate(figurePrefab[Random.Range(0, figurePrefab.Length)], positions[i].transform.position, Quaternion.identity, transform.parent);
        }
        countOfFigures = 3;
    }
    public void Clear()
    {
        for (int i = 0; i < 10; i++)
        {
            int k = 0;
            for (int j = 0; j < 10; j++)
            {
                if (gridCells[i, j].cell)
                {
                    k++;
                }
            }
            if (k == 10)
            {
                Score.instance.ScoreUp(30);
                for (int j = 0; j < 10; j++)
                {
                    StartCoroutine(gridCells[i, j].cell.Destroy());
                }
            }
        }
        for (int i = 0; i < 10; i++)
        {
            int k = 0;
            for (int j = 0; j < 10; j++)
            {
                if (gridCells[j, i].cell)
                {
                    k++;
                }
            }
            if (k == 10)
            {
                Score.instance.ScoreUp(30);
                for (int j = 0; j < 10; j++)
                {
                    StartCoroutine(gridCells[j, i].cell.Destroy());
                }
            }
        }
    }
    public bool IsGameOver()
    {
        foreach (var item in FindObjectsOfType<Figure>())
        {
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    item.transform.position = new Vector3(i, j);
                    if (item.IsAnyPlace())
                    {
                        item.transform.position = item.startPosition;
                        return false;
                    }
                }
            }
            item.transform.position = item.startPosition;
        }
        gameOver.SetActive(true);
        isEnd = true;
        return true;
    }
}
