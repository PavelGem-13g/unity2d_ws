﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using UnityEngine;

public class SQL : MonoBehaviour
{
    string host = "127.0.0.1";
    string database = "farm";
    string port = "3307";
    string username = "root";
    string password = "iPad1266";
    MySqlConnection connection;

    void Awake()
    {
        Connect();
        DontDestroyOnLoad(gameObject);
    }
    void Connect()
    {
        string connect =
            "Server=" + host + ";" +
            "Database=" + database + ";" +
            "port=" + port + ";" +
            "User Id=" + username + ";" +
            "password=" + password;
        connection = new MySqlConnection(connect);
    }
    public void AddPlayer(string name, string login, string password)
    {
        Command($"INSERT INTO users (name,login,password,level,value,crystal) VALUES ('{name}','{login}','{password}',0,0,0)");
        for (int i = 0; i < 9; i++)
        {
            Command($"INSERT INTO beds (login,state,number) VALUES ('{login}',{0},{i})");
        }
    }
    public List<string> GetBedsInformation(string login, int number)
    {
        List<string> result = new List<string>();
        connection.Open();
        MySqlCommand mySqlCommand = connection.CreateCommand();
        mySqlCommand.CommandText = $"SELECT * FROM beds WHERE login='{login}' AND number={number}";
        DbDataReader reader = mySqlCommand.ExecuteReader();
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                result.Add(reader.GetValue(reader.GetOrdinal("idbeds")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("state")).ToString());
            }
            connection.Close();
        }
        else
        {
            connection.Close();
            Command($"INSERT INTO beds (login,state,number) VALUES ('{login}',{0},{number})");
            connection.Open();
            mySqlCommand.CommandText = $"SELECT * FROM beds WHERE login = '{login}' AND number = {number}";
            reader = mySqlCommand.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    result.Add(reader.GetValue(reader.GetOrdinal("idbeds")).ToString());
                    result.Add(reader.GetValue(reader.GetOrdinal("state")).ToString());
                }
                connection.Close();
            }
        }
        return result;
    }
    public List<string> GetMachineInformation(string login, int number, int type)
    {
        List<string> result = new List<string>();
        connection.Open();
        MySqlCommand mySqlCommand = connection.CreateCommand();
        mySqlCommand.CommandText = $"SELECT * FROM machine WHERE login='{login}' AND number={number}";
        DbDataReader reader = mySqlCommand.ExecuteReader();
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                result.Add(reader.GetValue(reader.GetOrdinal("idmachine")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("endTime")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("state")).ToString());
            }
            connection.Close();
        }
        else
        {
            connection.Close();
            Command($"INSERT INTO machine (login,number,type,endTime,state) VALUES ('{login}',{number},{type},'{DateTime.UtcNow}',{0})");
            connection.Open();
            mySqlCommand.CommandText = $"SELECT * FROM machine WHERE login = '{login}' AND number = {number}";
            reader = mySqlCommand.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    result.Add(reader.GetValue(reader.GetOrdinal("idmachine")).ToString());
                    result.Add(reader.GetValue(reader.GetOrdinal("endTime")).ToString());
                    result.Add(reader.GetValue(reader.GetOrdinal("state")).ToString());
                }
                connection.Close();
            }
        }
        return result;
    }
    public List<string> GetBonusInformation(string login, int day)
    {
        List<string> result = new List<string>();
        connection.Open();
        MySqlCommand mySqlCommand = connection.CreateCommand();
        mySqlCommand.CommandText = $"SELECT * FROM bonus WHERE login='{login}' AND day={day}";
        DbDataReader reader = mySqlCommand.ExecuteReader();
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                result.Add(reader.GetValue(reader.GetOrdinal("idbonus")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("state")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("endDay")).ToString());
            }
            connection.Close();
        }
        else
        {
            connection.Close();
            Command($"INSERT INTO bonus (login,state,endDay,day) VALUES ('{login}',{0},'{DateTime.UtcNow + new TimeSpan(day, 0, 0, 0)}',{day})");
            connection.Open();
            mySqlCommand.CommandText = $"SELECT * FROM bonus WHERE login = '{login}' AND day = {day}";
            reader = mySqlCommand.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    result.Add(reader.GetValue(reader.GetOrdinal("idbonus")).ToString());
                    result.Add(reader.GetValue(reader.GetOrdinal("state")).ToString());
                    result.Add(reader.GetValue(reader.GetOrdinal("endDay")).ToString());
                }
                connection.Close();
            }
        }
        return result;
    }
    public List<string> GetAchiveInformation(string login, int number)
    {
        List<string> result = new List<string>();
        connection.Open();
        MySqlCommand mySqlCommand = connection.CreateCommand();
        mySqlCommand.CommandText = $"SELECT * FROM achivements WHERE login='{login}' AND number={number}";
        DbDataReader reader = mySqlCommand.ExecuteReader();
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                result.Add(reader.GetValue(reader.GetOrdinal("idachivements")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("complete")).ToString());
            }
            connection.Close();
        }
        else
        {
            connection.Close();
            Command($"INSERT INTO achivements (login,number,complete) VALUES ('{login}',{number},{0})");
            connection.Open();
            mySqlCommand.CommandText = $"SELECT * FROM achivements WHERE login = '{login}' AND number = {number}";
            reader = mySqlCommand.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    result.Add(reader.GetValue(reader.GetOrdinal("idachivements")).ToString());
                    result.Add(reader.GetValue(reader.GetOrdinal("complete")).ToString());
                }
                connection.Close();
            }
        }
        return result;
    }

    public void UpdateTime(int idbeads, string time)
    {
        Command($"UPDATE landing SET endTime = {time} WHERE idbeds = {idbeads}");
    }
    public List<string> GetLandingsInformation(int idbeads)
    {
        List<string> result = new List<string>();
        connection.Open();
        MySqlCommand mySqlCommand = connection.CreateCommand();
        mySqlCommand.CommandText = $"SELECT * FROM landing WHERE idbeds={idbeads}";
        DbDataReader reader = mySqlCommand.ExecuteReader();
        if (reader.HasRows)
        {

            while (reader.Read())
            {
                result.Add(reader.GetValue(reader.GetOrdinal("type")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("state")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("endTime")).ToString());
            }
            connection.Close();
        }
        else
        {
            connection.Close();
            Command($"INSERT INTO landing (idbeds,type,state,endTime) VALUES ({idbeads},{0},{0},'')");
            connection.Open();
            mySqlCommand.CommandText = $"SELECT * FROM landing WHERE idbeds={idbeads}";
            reader = mySqlCommand.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    result.Add(reader.GetValue(reader.GetOrdinal("type")).ToString());
                    result.Add(reader.GetValue(reader.GetOrdinal("state")).ToString());
                    result.Add(reader.GetValue(reader.GetOrdinal("endTime")).ToString());
                }
            }
            connection.Close();
        }
        return result;
    }

    public List<string> GetStorageInformation(string login)
    {
        List<string> result = new List<string>();
        connection.Open();
        MySqlCommand mySqlCommand = connection.CreateCommand();
        mySqlCommand.CommandText = $"SELECT * FROM storage WHERE login='{login}'";
        DbDataReader reader = mySqlCommand.ExecuteReader();
        if (reader.HasRows)
        {

            while (reader.Read())
            {
                result.Add(reader.GetValue(reader.GetOrdinal("milk")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("egg")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("yellowPlant")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("whitePlant")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("violetPlant")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("resultOne")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("resultTwo")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("resultThree")).ToString());
            }
            connection.Close();
        }
        else
        {
            connection.Close();
            Command($"INSERT INTO storage (login, milk, egg, yellowPlant, whitePlant, violetPlant, resultOne, resultTwo, resultThree) VALUES ('{login}',{0},{0},{0},{0},{0},{0},{0},{0})");
            connection.Open();
            mySqlCommand.CommandText = $"SELECT * FROM storage WHERE login = '{login}'";
            reader = mySqlCommand.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    result.Add(reader.GetValue(reader.GetOrdinal("milk")).ToString());
                    result.Add(reader.GetValue(reader.GetOrdinal("egg")).ToString());
                    result.Add(reader.GetValue(reader.GetOrdinal("yellowPlant")).ToString());
                    result.Add(reader.GetValue(reader.GetOrdinal("whitePlant")).ToString());
                    result.Add(reader.GetValue(reader.GetOrdinal("violetPlant")).ToString());
                    result.Add(reader.GetValue(reader.GetOrdinal("resultOne")).ToString());
                    result.Add(reader.GetValue(reader.GetOrdinal("resultTwo")).ToString());
                    result.Add(reader.GetValue(reader.GetOrdinal("resultThree")).ToString());
                }
            }
            connection.Close();
        }
        return result;
    }
    public void UpdateStorage(string paramtr, int value, string login)
    {
        Command($"UPDATE storage SET {paramtr} = {value} WHERE login = '{login}'");
    }
    public List<string> GetPaddockInformation(string login, int number)
    {
        List<string> result = new List<string>();
        connection.Open();
        MySqlCommand mySqlCommand = connection.CreateCommand();
        mySqlCommand.CommandText = $"SELECT * FROM paddock WHERE login='{login}' AND number={number}";
        DbDataReader reader = mySqlCommand.ExecuteReader();
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                result.Add(reader.GetValue(reader.GetOrdinal("idpaddock")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("state")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("endTime")).ToString());
            }
            connection.Close();
        }
        else
        {
            connection.Close();
            Command($"INSERT INTO paddock (login,state,number,endTime) VALUES ('{login}',{0},{number},'')");
            connection.Open();
            mySqlCommand.CommandText = $"SELECT * FROM paddock WHERE login = '{login}' AND number = {number}";
            reader = mySqlCommand.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    result.Add(reader.GetValue(reader.GetOrdinal("idpaddock")).ToString());
                    result.Add(reader.GetValue(reader.GetOrdinal("state")).ToString());
                    result.Add(reader.GetValue(reader.GetOrdinal("endTime")).ToString());
                    //result.Add(reader.GetValue(reader.GetOrdinal("type")).ToString());
                }
                connection.Close();
            }
        }
        return result;
    }
    public void EditParametr(string login, string nameOfParametr, int value)
    {
        Command($"UPDATE users SET {nameOfParametr} = {value} WHERE login = '{login}'");
    }
    public void EditParametr(string login, string nameOfParametr, string value)
    {
        Command($"UPDATE users SET {nameOfParametr} = '{value}' WHERE login = '{login}'");
    }
    public List<string> GetInformation(string login, string password)
    {
        List<string> result = new List<string>();
        connection.Open();
        MySqlCommand mySqlCommand = connection.CreateCommand();
        mySqlCommand.CommandText = $"SELECT * FROM users WHERE login = '{login}' AND password = '{password}'";
        DbDataReader reader = mySqlCommand.ExecuteReader();
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                result.Add(reader.GetValue(reader.GetOrdinal("name")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("login")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("password")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("level")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("value")).ToString());
                result.Add(reader.GetValue(reader.GetOrdinal("crystal")).ToString());
            }
        }
        connection.Close();
        return result;
    }
    public bool IsExistPlayer(string login, string password)
    {
        bool result = false;
        if (GetInformation(login, password).Count > 0)
        {
            result = true;
        }
        return result;
    }
    public bool CheckLogin(string login, string password)
    {
        bool result = false;
        connection.Open();
        try
        {
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = $"SELECT * FROM users WHERE login = '{login}' AND password = '{password}'";
            DbDataReader reader = mySqlCommand.ExecuteReader();
            if (reader.HasRows)
            {
                result = true;
            }
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }
        connection.Close();
        return result;
    }
    public bool IsUsedLogin(string login)
    {
        bool result = false;
        connection.Open();
        try
        {
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = $"SELECT * FROM users WHERE login = '{login}'";
            DbDataReader reader = mySqlCommand.ExecuteReader();
            if (reader.HasRows)
            {
                result = true;
            }
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }
        connection.Close();
        return result;
    }
    public void UpdateLanding(int idbeads, int type)
    {
        connection.Open();
        MySqlCommand mySqlCommand = connection.CreateCommand();
        mySqlCommand.CommandText = $"SELECT * FROM landing WHERE idbeds={idbeads}";
        DbDataReader reader = mySqlCommand.ExecuteReader();
        if (reader.HasRows)
        {
            connection.Close();
            Command($"UPDATE landing SET type = {type} WHERE idbeds = {idbeads}");
        }
        else
        {
            connection.Close();
            Command($"INSERT INTO landing (idbeds,type,state,endTime) VALUES({idbeads},{type},{0},'')");
        }
    }

    public void Command(string command)
    {
        connection.Open();
        try
        {
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = command;
            mySqlCommand.ExecuteNonQuery();
        }
        catch (Exception e)
        {
            Debug.LogWarning(e.Message);
        }
        connection.Close();
    }
    void CommandWithoutReconection(string command)
    {
        try
        {
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = command;
            mySqlCommand.ExecuteNonQuery();
        }
        catch (Exception e)
        {
            Debug.LogWarning(e.Message);
        }
    }
}
