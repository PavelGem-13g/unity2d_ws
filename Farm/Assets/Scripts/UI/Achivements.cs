﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Achivements : MonoBehaviour
{
    MainSCR main;
    SQL sql;
    public Text achiveName;
    public Text description;
    public Button button;
    public int idachivements;
    public int complete;
    public int number;

    private void Start()
    {
        Check();
        if (number == 0)
        {
            achiveName.text = "First steps";
            description.text = "Collect 5 yellow plant";
        }
        else if (number == 1)
        {
            achiveName.text = "Oh my animals";
            description.text = "Collect 1 milk and 1 egg";
        }
        else if (number == 2)
        {
            achiveName.text = "Is it Factorio?";
            description.text = "Create 1 sample that created with milk and egg";
        }
    }
    private void Awake()
    {
        main = FindObjectOfType<MainSCR>();
        sql = FindObjectOfType<SQL>();
        button = GetComponentInChildren<Button>();
        button.interactable = false;
        GetAchiveInformation();
    }
    void GetAchiveInformation()
    {
        List<string> temp = sql.GetAchiveInformation(main.login, number);
        idachivements = Convert.ToInt32(temp[0]);
        complete = Convert.ToInt32(temp[1]);
    }
    public void Check()
    {
        if (((main.yellowPlant > 4 && number == 0) ||
            (main.milk > 0 && main.egg > 0 && number == 1) ||
            (main.resultOne > 0 && number == 2)) &&
            complete==0)
        {
            complete = 1;
            sql.Command($"UPDATE achivements SET complete = {complete} WHERE login = '{main.login}' AND number = {number}");
        }
        if (complete == 1) 
        {
            button.interactable = true;
        }
    }
    public void GetPresents() 
    {
        if (complete==1)
        {
            if (number==0)
            {
                main.crystal += 10;
                main.value += 10;
            }
            if (number==1)
            {
                main.crystal += 30;
            }
            if (number==2)
            {
                main.value += 30;
            }
            complete = 2;
            button.interactable = false;
            sql.Command($"UPDATE achivements SET complete = {2} WHERE idachivements = {idachivements}");
            sql.Command($"UPDATE users SET crystal = {main.crystal}, value = {main.value} WHERE login = '{main.login}'");
            main.TopPanelUpdate();
        }
    }
}
