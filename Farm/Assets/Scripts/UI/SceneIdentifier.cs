﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneIdentifier : MonoBehaviour
{
    public GameObject[] scenes;
    public void SceneUpdate(string name)
    {
        foreach (var item in scenes)
        {
            if (name != item.name)
            {
                item.SetActive(false);
            }
            if (name == item.name)
            {
                item.SetActive(true);
            }
        }
        foreach (var item in FindObjectsOfType<Bed>())
        {
            item.UnPause();
        }
        foreach (var item in FindObjectsOfType<Paddock>())
        {
            item.UnPause();
        }
        foreach (var item in FindObjectsOfType<Machine>())
        {
            item.UnPause();
        }
    }
}
