﻿using UnityEngine;

public class UI : MonoBehaviour
{
    void Awake()
    {
        if (FindObjectsOfType<UI>().Length > 1)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }
}
