﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EveryDayBonus : MonoBehaviour
{
    MainSCR main;
    SQL sql;
    public int idbonus;
    public int day;
    public int state;
    public DateTime endDay;

    public GameObject button;
    public GameObject complete;

    private void Awake()
    {
        main = FindObjectOfType<MainSCR>();
        sql = FindObjectOfType<SQL>();
        GetBonusInformation();
    }
    private void Start()
    {
        if (state == 3)
        {
            if (DateTime.UtcNow.Date == endDay.Date)
            {
                transform.parent.parent.gameObject.SetActive(false);
            }
        }
        else
        {
            StartCoroutine(Timer());
        }
    }

    void GetBonusInformation()
    {
        List<string> temp = sql.GetBonusInformation(main.login, day);
        idbonus = Convert.ToInt32(temp[0]);
        state = Convert.ToInt32(temp[1]);
        endDay = DateTime.Parse(temp[2]);
    }
    IEnumerator Timer()
    {
        if (state < 2)
        {
            button.SetActive(true);
            button.GetComponent<Button>().interactable = false;
            complete.SetActive(false);
            state = 1;
            sql.Command($"UPDATE bonus SET state = {state} WHERE login = '{main.login}' AND day = {day}");
            while (DateTime.UtcNow <= endDay)
            {
                state = 1;
                yield return new WaitForSeconds(0.2f);
            }
        }
        End();
    }

    private void End()
    {
        if (state == 1)
        {
            state = 2;
            sql.Command($"UPDATE bonus SET state = {state} WHERE login = '{main.login}' AND day = {day}");
            button.SetActive(true);
            button.GetComponent<Button>().interactable = true;
            complete.SetActive(false);
        }
        else if (state == 3)
        {
            complete.SetActive(true);
            button.SetActive(false);
        }
        else if (state == 2)
        {
            button.SetActive(true);
            complete.SetActive(false);
        }

    }
    public void GetBonus()
    {
        button.SetActive(false);
        complete.SetActive(true);

        if (day == 0)
        {
            main.crystal += 10;
            main.value += 10;
        }
        if (day == 1)
        {
            main.crystal += 30;
        }
        if (day == 2)
        {
            main.value += 30;
        }
        state = 3;
        sql.Command($"UPDATE bonus SET state = {3} WHERE idbonus = {idbonus}");
        sql.Command($"UPDATE users SET crystal = {main.crystal}, value = {main.value} WHERE login = '{main.login}'");
        main.TopPanelUpdate();
    }
}
