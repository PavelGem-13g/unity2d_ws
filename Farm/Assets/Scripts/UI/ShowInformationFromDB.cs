﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowInformationFromDB : MonoBehaviour
{
    public SQL sql;
    public MainSCR main;
    public Text text;
    public int number;
    private void Awake()
    {
        text = GetComponent<Text>();
        sql = FindObjectOfType<SQL>();
        main = FindObjectOfType<MainSCR>();
    }
    private void Start()
    {
        UpdateText();
    }
    public void UpdateText()
    {
        text.text = sql.GetInformation(main.login,main.password)[number].ToString();
    }
}
