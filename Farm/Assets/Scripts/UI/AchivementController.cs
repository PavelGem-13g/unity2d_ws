﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchivementController : MonoBehaviour
{
    public void UpdateAchivements() 
    {
        foreach (var item in FindObjectsOfType<Achivements>())
        {
            item.Check();
        }
    }
}
