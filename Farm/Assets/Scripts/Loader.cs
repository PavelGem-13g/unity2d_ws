﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Loader : MonoBehaviour
{
    SceneIdentifier identifier;
    private void Awake()
    {
        identifier = FindObjectOfType<SceneIdentifier>();
    }
    public void LoadScene(string name) 
    {
        SceneManager.LoadScene(name);
    }
    public void LoadScene(int number)
    {
        SceneManager.LoadScene(number);
    }
    public void ReloadScene() 
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void LoadWithoutReload(string name) 
    {
        if (SceneManager.GetActiveScene().name != name) 
        {
            SceneManager.LoadScene(name);
            identifier.SceneUpdate(name);
        }
    }
}
