﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MainSCR : MonoBehaviour
{

    SQL sql;
    public bool isAuthorised = false;
    public string username;
    public string login;
    public string password;
    public int level;
    public int value;
    public int crystal;
    public UnityEvent authorize;

    public int milk;
    public int egg;
    public int yellowPlant;
    public int whitePlant;
    public int violetPlant;

    public int resultOne;
    public int resultTwo;
    public int resultThree;


    public void Awake()
    {
        sql = FindObjectOfType<SQL>();
        DontDestroyOnLoad(gameObject);
    }
    public void Authorize(string login, string password)
    {
        List<string> list = sql.GetInformation(login, password);
        username = list[0];
        this.login = list[1];
        this.password = list[2];
        level = Convert.ToInt32(list[3]);
        value = Convert.ToInt32(list[4]);
        crystal = Convert.ToInt32(list[5]);
        isAuthorised = true;
        authorize.Invoke();
        GetStorageInformation(login);
    }
    public void TopPanelUpdate()
    {
        ShowInformationFromDB[] informationFromDB = FindObjectsOfType<ShowInformationFromDB>();
        foreach (var item in informationFromDB)
        {
            item.UpdateText();
        }
    }
    void GetStorageInformation(string login) 
    {
        List<string> temp = sql.GetStorageInformation(login);
        milk = Convert.ToInt32(temp[0]);
        egg = Convert.ToInt32(temp[1]);
        yellowPlant = Convert.ToInt32(temp[2]);
        whitePlant = Convert.ToInt32(temp[3]);
        violetPlant = Convert.ToInt32(temp[4]);
        resultOne = Convert.ToInt32(temp[5]);
        resultTwo = Convert.ToInt32(temp[6]);
        resultThree = Convert.ToInt32(temp[7]);
    }
    public static DateTime FromString(string data)
    {
        string[] parsed = data.Split(new char[] { '.' });
        int[] result = new int[parsed.Length];

        if (result.Length == 6)
        {
            for (int i = 0; i < parsed.Length; i++)
            {
                result[i] = Convert.ToInt32(parsed[i]);
            }
            return new DateTime(result[0], result[1], result[2], result[3], result[4], result[5]);
        }
        return new DateTime();
    }
}
