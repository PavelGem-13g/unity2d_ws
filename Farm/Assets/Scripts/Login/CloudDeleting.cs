﻿using System.Collections;
using UnityEngine;

public class CloudDeleting : MonoBehaviour
{
    public SpriteRenderer[] images;
    public GameObject touchToPlay;
    public GameObject[] endPositions;
    // Update is called once per frame
    void Start()
    {
        StartCoroutine(Deleting());
    }
    IEnumerator Deleting()
    {
        while (images[0].color.a > 0f)
        {
            for (int i = 0; i < images.Length; i++)
            {
                images[i].color = new Color(images[i].color.r, images[i].color.g, images[i].color.b, images[i].color.a - 0.01f);
                images[i].transform.position = Vector3.MoveTowards(images[i].transform.position, endPositions[i].transform.position, 0.1f);
            }
            yield return new WaitForSeconds(0.01f);
        }
        for (int i = 0; i < images.Length; i++)
        {
            Destroy(images[i].gameObject);
        }
        Destroy(gameObject);
        touchToPlay.SetActive(true);
    }
}
