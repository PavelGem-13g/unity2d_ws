﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapAnySCR : MonoBehaviour
{
    public GameObject farmPreview;
    public GameObject loginBackground;
    public GameObject loginInput;
    void Update()
    {
        if (Input.anyKey) 
        {
            farmPreview.SetActive(false);
            loginBackground.SetActive(true);
            loginInput.SetActive(true);
            gameObject.SetActive(false);
        }       
    }
}
