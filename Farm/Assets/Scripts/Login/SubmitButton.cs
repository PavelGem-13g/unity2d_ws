﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SubmitButton : MonoBehaviour
{
    SQL sql;
    public Text username;
    public Text login;
    public InputField password;
    public Text error;
    public GameObject panel;
    public Animator animator;
    private void Awake()
    {
        sql = FindObjectOfType<SQL>();
    }

    public void SignUp()
    {
        try
        {
            if (!(username.text.Length == 0 ||
                username.text.Length == 0 ||
                username.text.Length == 0))
            {
                if (!sql.IsUsedLogin(login.text))
                {
                    sql.AddPlayer(username.text, login.text, password.text);
                    //Close();
                    panel.SetActive(false);
                    //StartCoroutine(CloseCoroutine());
                }
                else
                {
                    error.text = "Придумайте новый логин";
                }

            }
            else
            {
                error.text = "Заполните одно из значений ";
            }
        }
        catch (Exception e)
        {
            error.text = e.Message;
            Debug.Log(e.Message);
        }
    }

    private void Close()
    {
        animator.Play("ClosedWindow");

    }

    public void LogIn()
    {
        if (sql.IsExistPlayer(login.text, password.text))
        {
            FindObjectOfType<MainSCR>().Authorize(login.text, password.text);
            error.text = "Здравствуйте," + FindObjectOfType<MainSCR>().username + "!";
            panel.SetActive(false);
            error.color = Color.green;
        }
        else
        {
            error.text = "Неверный логин или пароль";
            error.color = Color.red;
        }
    }
/*    IEnumerator CloseCoroutine()
    {
        animator.Play("ClosedWindow");
        yield return new WaitForSecondsRealtime(1f);
        panel.SetActive(false);
    }*/
}
