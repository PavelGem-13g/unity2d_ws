﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class LoadBarSCR : MonoBehaviour
{
    Image image;
    float timer = 2f;
    float realTimer = 0f;
    public UnityEvent start;
    public GameObject bar;
    private void Start()
    {
        image = GetComponent<Image>();
    }
    private void Update()
    {
        if (realTimer<timer) 
        {
            realTimer += Time.deltaTime;
            image.fillAmount = realTimer / timer;
        }
        else 
        {
            start.Invoke();
            Destroy(bar);
        }
    }
}
