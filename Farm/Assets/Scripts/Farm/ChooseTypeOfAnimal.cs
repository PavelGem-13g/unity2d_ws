﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseTypeOfAnimal : MonoBehaviour
{
    public int type;
    public Paddock paddock;
    SQL sql;
    private void Awake()
    {
        sql = FindObjectOfType<SQL>();
    }

    public void SetType(int type)
    {
        paddock.SetTime(type);
        sql.UpdateLanding(paddock.idpaddock, type);
        gameObject.SetActive(false);
    }
}
