﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bed : MonoBehaviour
{
    MainSCR main;
    SQL sql;
    public int idbeads;
    public string login;
    public int state;
    public int number;
    public int type;
    public int landingState;
    public ChooseType chooseType;
    public Sprite[] states;
    public Image image;
    public Sprite[] langing;
    public DateTime endTime;
    public Sprite growningSprite;
    public bool isGrowning;

    private void Awake()
    {
        main = FindObjectOfType<MainSCR>();
        sql = FindObjectOfType<SQL>();
        number = Convert.ToInt32(gameObject.name);
        image = GetComponent<Image>();
        GetBeadsInformation();
        GetLandingsInformation();
    }

    private void Start()
    {
        StartCoroutine(Timer());
    }

    public void UnPause() 
    {
        StartCoroutine(Timer());
    }
    private void GetBeadsInformation()
    {
        login = main.login;
        List<string> temp = sql.GetBedsInformation(login, number);
        idbeads = Convert.ToInt32(temp[0]);
        state = Convert.ToInt32(temp[1]);
    }
    private void GetLandingsInformation()
    {
        List<string> temp = sql.GetLandingsInformation(idbeads);
        type = Convert.ToInt32(temp[0]);
        landingState = Convert.ToInt32(temp[1]);
        endTime = MainSCR.FromString(temp[2]);
    }

    public void ChangeState()
    {
        if (!isGrowning)
        {
            if (state < 3)
            {
                state++;
            }
            else if (state == 3 && type != 0 && !isGrowning)
            {
                Clear();
            }
            else
            {
                chooseType.gameObject.SetActive(true);
                chooseType.bed = this;
            }
            SpriteUpdate();
            sql.Command($"UPDATE beds SET state = {state} WHERE login = '{login}' AND number = {number}");
        }
    }
    public void SpriteUpdate()
    {
        if (state == 3 && type != 0)
        {
            image.sprite = langing[type - 1];
        }
        else
        {
            image.sprite = states[state];
        }
    }
    public void SetType(int type)
    {
        int delta = 0;
        this.type = type;
        if (type == 1)
        {
            delta = 2;
        }
        if (type == 2)
        {
            delta = 10;
        }
        if (type == 3)
        {
            delta = 30;
        }
        endTime = DateTime.UtcNow + new TimeSpan(0,0,delta);
        sql.Command($"UPDATE landing SET endTime = '{endTime}', type = {type} WHERE idbeds = {idbeads}");
        StartCoroutine(Timer());
    }
    public void Clear()
    {
        main.value += type;
        sql.Command($"UPDATE users SET value = {main.value} WHERE login = '{login}'");
        if (type == 1)
        {
            main.yellowPlant += 1;
            sql.Command($"UPDATE storage SET yellowPlant = {main.yellowPlant} WHERE login = '{login}'");
        }
        if (type == 2)
        {
            main.whitePlant += 1;
            sql.Command($"UPDATE storage SET whitePlant = {main.whitePlant} WHERE login = '{login}'");
        }
        if (type == 3)
        {
            main.violetPlant += 1;
            sql.Command($"UPDATE storage SET violetPlant = {main.violetPlant} WHERE login = '{login}'");
        }
        main.TopPanelUpdate();
        type = 0;
        state = 0;
        isGrowning = false;
        sql.Command($"UPDATE beds SET state = {state} WHERE idbeds = {idbeads}");
        sql.Command($"UPDATE landing SET endTime = '{DateTime.UtcNow}', type = {type} WHERE idbeds = {idbeads}");
    }
    IEnumerator Timer()
    {
        while (DateTime.UtcNow<=endTime)
        {
            image.sprite = growningSprite;
            isGrowning = true;
            yield return new WaitForSeconds(0.2f);
        }
        isGrowning = false;
        SpriteUpdate();
    }
}
