﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Paddock : MonoBehaviour
{
    MainSCR main;
    SQL sql;
    public int idpaddock;
    public string login;
    public int state;
    public int number;
    public int type;
    public int landingState;
    public DateTime endTime;
    public Text text;

    public Sprite[] spritesOfAnimals;

    private void Awake()
    {
        main = FindObjectOfType<MainSCR>();
        sql = FindObjectOfType<SQL>();
        number = Convert.ToInt32(gameObject.name);
        text = GetComponentInChildren<Text>();
        GetPaddockInformation();
    }

    private void Start()
    {
        SpriteUpdate();
        GetComponent<Image>().sprite = spritesOfAnimals[type];
        StartCoroutine(Timer());
    }
    public void UnPause()
    {
        StartCoroutine(Timer());
    }
    private void GetPaddockInformation()
    {
        login = main.login;
        List<string> temp = sql.GetPaddockInformation(login, number);
        idpaddock = Convert.ToInt32(temp[0]);
        state = Convert.ToInt32(temp[1]);
        endTime = MainSCR.FromString(temp[2]);
    }
    public void ChangeState()
    {
        if (state == 0)
        {
            SetTime(type);
        }
        else
        {
            Clear();
        }
        SpriteUpdate();
        sql.Command($"UPDATE paddock SET state = {state} WHERE login = '{login}' AND number = {number}");
    }
    public void SpriteUpdate()
    {
        if (type == 0)
        {
            text.text = "Chicken is ready";
        }
        else
        {
            text.text = "Cow is ready";
        }
    }
    public void SetTime(int type)
    {
        int delta = 0;
        this.type = type;
        if (type == 0)
        {
            delta = 2;
        }
        if (type == 1)
        {
            delta = 10;
        }
        endTime = DateTime.UtcNow + new TimeSpan(0, 0, delta);
        sql.Command($"UPDATE paddock SET endTime = '{endTime}' WHERE idpaddock = {idpaddock}");
        StartCoroutine(Timer());
    }
    public void Clear()
    {
        main.crystal += type + 1;
        sql.Command($"UPDATE users SET crystal = {main.crystal} WHERE login = '{login}'");
        if (type == 0)
        {
            main.egg += 1;
            sql.Command($"UPDATE storage SET egg = {main.egg} WHERE login = '{login}'");
        }
        else
        {
            main.milk += 1;
            sql.Command($"UPDATE storage SET milk = {main.milk} WHERE login = '{login}'");
        }
        main.TopPanelUpdate();
        state = 0;
        sql.Command($"UPDATE paddock SET state = {state} WHERE idpaddock = {idpaddock}");
        sql.Command($"UPDATE paddock SET endTime = '{DateTime.UtcNow}' WHERE idpaddock = {idpaddock}");
        SetTime(type);
    }
    IEnumerator Timer()
    {
        while (DateTime.UtcNow <= endTime)
        {
            text.text = "End time is "+endTime;
            state = 1;
            yield return new WaitForSeconds(0.2f);
        }
        SpriteUpdate();
    }
}
