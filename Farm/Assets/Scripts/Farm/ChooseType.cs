﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseType : MonoBehaviour
{
    public int type;
    public Bed bed;
    SQL sql;
    private void Awake()
    {
        sql = FindObjectOfType<SQL>();
    }

    public void SetType(int type) 
    {
        bed.SetType(type);
        gameObject.SetActive(false);
    }
}
