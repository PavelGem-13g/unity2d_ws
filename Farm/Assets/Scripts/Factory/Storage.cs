﻿using UnityEngine;
using UnityEngine.UI;

public class Storage : MonoBehaviour
{
    Text text;
    SQL sql;
    MainSCR main;
    private void Awake()
    {
        sql = FindObjectOfType<SQL>();
        main = FindObjectOfType<MainSCR>();
        text = GetComponent<Text>();
    }
    public void UpateText()
    {
        text.text = "";
        text.text += "Milk: " + main.milk.ToString() + "\n";
        text.text += "Egg: " + main.egg.ToString() + "\n";
        text.text += "Yellow plant: " + main.yellowPlant.ToString() + "\n";
        text.text += "White plant: " + main.whitePlant.ToString() + "\n";
        text.text += "Violet plant: " + main.violetPlant.ToString() + "\n";
    }
}
