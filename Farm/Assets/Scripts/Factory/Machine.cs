﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Machine : MonoBehaviour
{
    public MainSCR main;
    public SQL sql;

    public int requiedMilk;
    public int requiedEgg;
    public int requiedYellowPlant;
    public int requiedWhitePlant;
    public int requiedVioletPlant;

    public int idmachine;
    public int type;
    public int number;
    public DateTime endTime;
    public int state;

    public Text text;

    public GameObject submitButton;

    public GameObject fail;

    public Sprite[] spritesOfMachines;

    private void Awake()
    {
        main = FindObjectOfType<MainSCR>();
        sql = FindObjectOfType<SQL>();
        GetMachineInformation();
        if (type == 0)
        {
            requiedMilk = 10;
            requiedEgg = 20;
        }
        if (type == 1)
        {
            requiedEgg = 10;
            requiedYellowPlant = 3;
        }
        if (type == 2)
        {
            requiedYellowPlant = 5;
            requiedWhitePlant = 5;
            requiedVioletPlant = 5;
        }
    }
    private void Start()
    {
        StartCoroutine(Timer());
        GetComponent<Image>().sprite = spritesOfMachines[type];
        RequirementText();
    }

    void RequirementText()
    {
        text.text = "";
        if (requiedMilk > 0)
        {
            text.text += "Milk: " + requiedMilk.ToString() + "\n";
        }
        if (requiedEgg > 0)
        {
            text.text += "Egg: " + requiedEgg.ToString() + "\n";
        }
        if (requiedYellowPlant > 0)
        {
            text.text += "Yellow plant: " + requiedYellowPlant.ToString() + "\n";
        }
        if (requiedWhitePlant > 0)
        {
            text.text += "White plant: " + requiedWhitePlant.ToString() + "\n";
        }
        if (requiedVioletPlant > 0)
        {
            text.text += "Violet plant: " + requiedVioletPlant.ToString() + "\n";
        }
    }

    void GetMachineInformation()
    {
        List<string> temp = sql.GetMachineInformation(main.login, number, type);
        idmachine = Convert.ToInt32(temp[0]);
        endTime = MainSCR.FromString(temp[1]);
        state = Convert.ToInt32(temp[2]);
    }

    public void Check()
    {
        if (state == 0)
        {
            if (main.milk >= requiedMilk &&
                main.egg >= requiedEgg &&
                main.yellowPlant >= requiedYellowPlant &&
                main.whitePlant >= requiedWhitePlant &&
                main.violetPlant >= requiedVioletPlant)
            {
                main.milk -= requiedMilk;
                main.egg -= requiedEgg;
                main.yellowPlant -= requiedYellowPlant;
                main.whitePlant -= requiedWhitePlant;
                main.violetPlant -= requiedVioletPlant;
                sql.UpdateStorage("milk", main.milk, main.login);
                sql.UpdateStorage("egg", main.egg, main.login);
                sql.UpdateStorage("yellowPlant", main.yellowPlant, main.login);
                sql.UpdateStorage("whitePlant", main.whitePlant, main.login);
                sql.UpdateStorage("violetPlant", main.violetPlant, main.login);
                if (type == 0)
                {
                    endTime = DateTime.UtcNow + new TimeSpan(0, 0, 10);
                }
                else if (type == 1)
                {
                    endTime = DateTime.UtcNow + new TimeSpan(0, 0, 30);
                }
                else if (type == 2)
                {
                    endTime = DateTime.UtcNow + new TimeSpan(0, 1, 0);
                }
                else
                {
                    endTime = DateTime.UtcNow + new TimeSpan(0);
                }
                sql.Command($"UPDATE machine SET endTime = '{endTime}' WHERE idmachine = {idmachine}");
                StartCoroutine(Timer());
            }
            else
            {
                RequirementText();
                text.text += "Недостаточно ресурсов";
            }

        }
        if (state == 2)
        {
            fail.SetActive(false);
            text.text = "";
            state = 0;
        }

    }
    IEnumerator Timer()
    {
        while (DateTime.UtcNow <= endTime)
        {
            state = 1;
            text.text = "End time is " + endTime;
            if (UnityEngine.Random.Range(0, 100) == 0)
            {
                state = 2;
                sql.Command($"UPDATE machine SET state = {2} WHERE idmachine = {idmachine}");
                break;
            }
            yield return new WaitForSeconds(0.2f);
        }
        Clear();
    }
    void Clear()
    {
        if (state != 2)
        {
            state = 0;
            if (type == 0)
            {
                main.resultOne += 1;
                sql.UpdateStorage("resultOne", main.resultOne, main.login);
            }
            if (type == 1)
            {
                main.resultTwo += 1;
                sql.UpdateStorage("resultTwo", main.resultTwo, main.login);
            }
            if (type == 2)
            {
                main.resultThree += 1;
                sql.UpdateStorage("resultThree", main.resultThree, main.login);
            }
            sql.Command($"UPDATE machine SET endTime = '{DateTime.UtcNow}',state = {0} WHERE idmachine = {idmachine}");
            text.text = "Ready";
        }
        else
        {
            fail.SetActive(true);
        }
    }
    public void UnPause()
    {
        StartCoroutine(Timer());
    }
}
