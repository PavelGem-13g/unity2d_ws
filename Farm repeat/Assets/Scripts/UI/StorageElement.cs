﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StorageElement : MonoBehaviour
{
    Text text;
    public int number;
    // инициализации
    private void Awake()
    {
        text = GetComponentInChildren<Text>();
        SetInformation();
    }
    // обновление отображаемой информации
    public void SetInformation()
    {
        List<int> temp = new List<int>();
        temp.Add(GameManager.instance.yp);
        temp.Add(GameManager.instance.vp);
        temp.Add(GameManager.instance.rp);
        temp.Add(GameManager.instance.pig);
        temp.Add(GameManager.instance.cow);
        temp.Add(GameManager.instance.chicken);
        text.text = temp[number].ToString();
    }
}
