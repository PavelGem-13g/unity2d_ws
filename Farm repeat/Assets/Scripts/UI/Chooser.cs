﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chooser : MonoBehaviour
{
    public void OpenOnlyOne() 
    {
        foreach (var item in FindObjectsOfType<Chooser>())
        {
            if (item!=this)
            {
                item.gameObject.SetActive(false);
            }
        }
    }
    public void CloseAll() 
    {
    
    }
}
