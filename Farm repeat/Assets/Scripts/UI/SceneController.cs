﻿using UnityEngine;

public class SceneController : MonoBehaviour
{
    public GameObject[] scenes;
    private void Awake()
    {
        CloseAllScenes();
        SetScene(0);
    }
    void CloseAllScenes()
    {
        foreach (var item in scenes)
        {
            item.SetActive(false);
        }
    }
    public void SetScene(int scene)
    {
        if (!scenes[scene].activeSelf)
        {
            CloseAllScenes();
            scenes[scene].SetActive(true);
        }
    }
}
