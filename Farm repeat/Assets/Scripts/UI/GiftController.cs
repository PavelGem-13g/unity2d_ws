﻿    using System;
using UnityEngine;

public class GiftController : MonoBehaviour
{
    public GameObject panel;
    public GameObject content;
    public GameObject giftElementPrefab;    
    private void Awake()
    {
        Check();
    }
    public void Check()
    {
        if (GameManager.instance.ltake < new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day, 0, 0, 0))
        {
            GameManager.instance.catched = 0;
            SQL.UpdateInformation("users", $"catched = {GameManager.instance.catched}", $"login = '{GameManager.instance.login}'");
            SQL.UpdateInformation("users", $"sday = {GameManager.instance.sday++}", $"login = '{GameManager.instance.login}'");
            panel.SetActive(true);
        }
        if (GameManager.instance.ltake == new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day) && GameManager.instance.catched == 1)
        {
            panel.SetActive(false);
        }
    }
    public void Open()
    {
        foreach (var item in FindObjectsOfType<GiftElement>())
        {
            Destroy(item);
        }
        for (int i = 0; i < 3; i++)
        {
            GiftElement temp = Instantiate(giftElementPrefab, Vector3.zero, Quaternion.identity, content.transform).GetComponent<GiftElement>();
            temp.day = i;
            temp.UpdateInformation();
        }
    }
}
