﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscClose : MonoBehaviour
{
    // метод, вызывающийся каждый кадр
    void Update()
    {
        //проверка на нажатие esc, выключение объекта
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            gameObject.SetActive(false);
        }
    }
}
