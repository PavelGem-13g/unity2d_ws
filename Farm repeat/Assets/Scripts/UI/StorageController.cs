﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StorageController : MonoBehaviour
{
    // обновление информации склада
    public void UpdateInformation() 
    {
        foreach (var item in FindObjectsOfType<StorageElement>())
        {
            item.SetInformation();
        }
    }
}
