﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowUserInformation : MonoBehaviour
{
    Text text;
    public int number;
    // инициализация
    private void Awake()
    {
        text = GetComponentInChildren<Text>();
        SetInformation();
    }
    // обновление отображение информации
    public void SetInformation()
    {
        List<string> temp = new List<string>();
        temp.Add(GameManager.instance.username);
        temp.Add(GameManager.instance.crystal.ToString());
        temp.Add(GameManager.instance.value.ToString());
        text.text = temp[number].ToString();
    }
}
