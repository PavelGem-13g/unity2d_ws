﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TaskElement : MonoBehaviour
{
    public Text lable;
    string lableText;
    string textText;
    int state;
    int type;
    public Button buttonCollect;
    //инициализация
    private void Awake()
    {
        GetTaskInformation();
        UpdateInformation();
    }
    //получение информацию из бд
    public void GetTaskInformation()
    {
        List<string> temp = SQL.GetTaskInformation(GameManager.instance.login, Convert.ToInt32(name));
        lableText = temp[0];
        textText = temp[1];
        lable.text = lableText;
        state = Convert.ToInt32(temp[2]);
        type = Convert.ToInt32(temp[3]);
    }
    //активация кнопки сбора
    public void UpdateInformation()
    {
        if (state == 0 && (type == 0 && GameManager.instance.rp > 4 && GameManager.instance.vp > 4 && GameManager.instance.yp > 4 ||
            type == 1 && GameManager.instance.cow > 0 && GameManager.instance.chicken > 0 && GameManager.instance.pig > 0 ||
            type == 2 && GameManager.instance.value > 4))
        {
            buttonCollect.interactable = true;
        }
        else
        {
            buttonCollect.interactable = false;
        }
    }
    //обработка нажатия на кнопку
    public void Collect()
    {
        if (state == 0)
        {
            if (type == 0 && GameManager.instance.rp > 4 && GameManager.instance.vp > 4 && GameManager.instance.yp > 4)
            {
                GameManager.instance.rp -= 5;
                GameManager.instance.vp -= 5;
                GameManager.instance.yp -= 5;
                GameManager.instance.value *= 2;
            }
            if (type == 1 && GameManager.instance.cow > 0 && GameManager.instance.chicken > 0 && GameManager.instance.pig > 0)
            {
                GameManager.instance.cow -= 1;
                GameManager.instance.chicken -= 1;
                GameManager.instance.pig -= 1;
                GameManager.instance.value *= 2;
            }
            if (type == 2 && GameManager.instance.value > 4)
            {
                GameManager.instance.value -= 5;
                GameManager.instance.crystal *= 3;
                GameManager.instance.TopPanelAnimation(-5);
            }
            GameManager.instance.UpdateTopPanel();
            state = 1;
            SQL.UpdateInformation("tasks", $"state = {1}", $"login = '{GameManager.instance.login}' AND type = {type}");
            SQL.UpdateInformation("storage", $"yp = {GameManager.instance.yp}", $"login = '{GameManager.instance.login}'");
            SQL.UpdateInformation("storage", $"vp = {GameManager.instance.vp}", $"login = '{GameManager.instance.login}'");
            SQL.UpdateInformation("storage", $"rp = {GameManager.instance.rp}", $"login = '{GameManager.instance.login}'");
            SQL.UpdateInformation("storage", $"pig = {GameManager.instance.pig}", $"login = '{GameManager.instance.login}'");
            SQL.UpdateInformation("storage", $"cow = {GameManager.instance.cow}", $"login = '{GameManager.instance.login}'");
            SQL.UpdateInformation("storage", $"chicken = {GameManager.instance.chicken}", $"login = '{GameManager.instance.login}'");
            buttonCollect.interactable = false;
        }
    }
    public void ShowDescription() 
    {
        lable.text = textText;
    }
    public void ShowLable() 
    {
        lable.text = lableText;
    }
}
