﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TopPanelAnim : MonoBehaviour
{
    public Text text;
    //инициализация
    private void Awake()
    {
        Animate(0);
    }
    // выбор цвета, запуск куратины
    public void Animate(int value)
    {
        text.text = value.ToString();
        if (value > 0)
        {
            text.color = Color.green;
        }
        else
        {
            text.color = Color.red;
        }
        StartCoroutine(Animation());
    }
    //куратина плавной анимации
    IEnumerator Animation()
    {
        text.color = new Color(text.color.r, text.color.g, text.color.b, 1);
        while (text.color.a > 0f)
        {
            text.color = new Color(text.color.r, text.color.g, text.color.b, text.color.a-0.01f);
            yield return new WaitForSeconds(0.01f);
        }
    }
}
