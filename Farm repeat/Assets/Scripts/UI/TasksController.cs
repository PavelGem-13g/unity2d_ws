﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TasksController : MonoBehaviour
{
    public void UpdateInformation() 
    {
        foreach (var item in FindObjectsOfType<TaskElement>())
        {
            item.UpdateInformation();
        }
    }
}
