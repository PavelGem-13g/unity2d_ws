﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GiftElement : MonoBehaviour
{
    public int day;
    public Text lable;
    public Sprite[] types;
    public Image typeImage;
    public int type;
    public Button getButton;
    public GameObject completeObject;
    [SerializeField] GameObject flash;

    // метод перед первым кадром, инициализация полей
    public void UpdateInformation()
    {
        GetGiftInformation();
        lable.text = day.ToString();
        typeImage.sprite = types[type];
        if (GameManager.instance.sday==day && GameManager.instance.catched==0) 
        {
            flash.SetActive(true);
            getButton.interactable = true;
            completeObject.SetActive(false);
        }
        if (GameManager.instance.sday == day && GameManager.instance.catched == 1)
        {
            flash.SetActive(false);
            getButton.gameObject.SetActive(false);
            completeObject.SetActive(true);
        }
        if (GameManager.instance.sday > day)
        {
            flash.SetActive(false);
            getButton.gameObject.SetActive(false);
            completeObject.SetActive(true);
        }
        if (GameManager.instance.sday < day)
        {
            flash.SetActive(false);
            getButton.interactable = false;
            completeObject.SetActive(false);
        }
    }
    // получение информации из БД
    void GetGiftInformation()
    {
        List<string> temp = new List<string>();
        temp = SQL.GetGiftInformation(day);
        type = Convert.ToInt32(temp[0]);
    }
    //обработчик события сбора подарка 
    public void Collect()
    {
        GameManager.instance.catched = 1;

        SQL.UpdateInformation("users", $"ltake = '{GameManager.DateTimeToShortString(DateTime.UtcNow)}'", $"login = '{GameManager.instance.login}'");
        SQL.UpdateInformation("users", $"sday = {GameManager.instance.sday}", $"login = '{GameManager.instance.login}'");
        SQL.UpdateInformation("users", $"catched = {1}", $"login = '{GameManager.instance.login}'");
        if (type == 0)
        {
            GameManager.instance.value *= 2;
        }
        if (type == 1)
        {
            GameManager.instance.value *= 2;
        }
        if (type == 2)
        {
            GameManager.instance.crystal *= 3;
        }
        GameManager.instance.UpdateTopPanel();
        GameManager.instance.TopPanelAnimation(type);
        SQL.UpdateInformation("users", $"value = {GameManager.instance.value}", $"login = '{GameManager.instance.login}'");
        SQL.UpdateInformation("users", $"crystasl = {GameManager.instance.crystal}", $"login = '{GameManager.instance.login}'");

        getButton.interactable = false;
        completeObject.SetActive(true);
    }

}
