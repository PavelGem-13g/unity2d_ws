﻿using UnityEngine;

public class Tutorial : MonoBehaviour
{
    [SerializeField] string parametr;
    private void Awake()
    {
        if (!((GameManager.instance.city == 0 && parametr == "city") ||
            (GameManager.instance.store == 0 && parametr == "store") ||
            (GameManager.instance.factory == 0 && parametr == "factory") ||
            (GameManager.instance.farm == 0 && parametr == "farm")))
        {
            gameObject.SetActive(false);
        }
    }
    private void Update()
    {
        if (Input.anyKey)
        {
            if (parametr == "city")
            {
                GameManager.instance.city = 1;
                SQL.UpdateInformation("tutorial", $"city = {1}", $"login = '{GameManager.instance.login}'");
                gameObject.SetActive(false);
            }
            else if (GameManager.instance.store == 0 && parametr == "store")
            {
                GameManager.instance.store = 1;
                SQL.UpdateInformation("tutorial", $"store = {1}", $"login = '{GameManager.instance.login}'");
                gameObject.SetActive(false);
            }
            else if (GameManager.instance.factory == 0 && parametr == "factory")
            {
                GameManager.instance.factory = 1;
                SQL.UpdateInformation("tutorial", $"factory = {1}", $"login = '{GameManager.instance.login}'");
                gameObject.SetActive(false);
            }
            else if (GameManager.instance.farm == 0 && parametr == "farm")
            {
                GameManager.instance.farm = 1;
                SQL.UpdateInformation("tutorial", $"farm = {1}", $"login = '{GameManager.instance.login}'");
                gameObject.SetActive(false);
            }
        }
    }
}
