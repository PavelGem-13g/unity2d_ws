﻿using Assets.Scripts;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using UnityEngine;

public static class SQL
{
    static MySqlConnection connection;
    // подключение к бд
    public static void Connect()
    {
        connection = new MySqlConnection(DBSettings.GetConnectionString());
    }
    // выполнение команды
    public static void NonQueryCommand(string command)
    {
        connection.Open();
        try
        {
            MySqlCommand mySqlCommand = connection.CreateCommand();
            mySqlCommand.CommandText = command;
            mySqlCommand.ExecuteNonQuery();
        }
        catch (Exception e)
        {
            Debug.Log("SQL - " + e.Message);
        }
        connection.Close();
    }
    // чтение из бд
    static DbDataReader ReaderCommand(string selectData, string table, string filter)
    {
        MySqlCommand mySqlCommand = connection.CreateCommand();
        mySqlCommand.CommandText = $"SELECT {selectData} FROM {table} WHERE {filter}";
        DbDataReader reader = mySqlCommand.ExecuteReader();
        return reader;
    }
    // чтение из бд через "один" ключ
    static DbDataReader JoinReaderCommand(string selectData, string table, string joinTable,string filter, string where)
    {
        MySqlCommand mySqlCommand = connection.CreateCommand();
        mySqlCommand.CommandText = $"SELECT {selectData} FROM {table} LEFT OUTER JOIN {joinTable} ON {filter} WHERE {where}";
        DbDataReader reader = mySqlCommand.ExecuteReader();
        return reader;
    }
    // ввод информации в бд
    public static void InsertIntoInformation(string table, string colums, string values)
    {
        NonQueryCommand($"INSERT INTO {table} ({colums}) VALUES({values})");
    }
    // добавление пользователя
    public static void AddPlayer(string login, string password, string name)
    {
        NonQueryCommand($"INSERT INTO users (login,password,name,crystasl,value,sday,ltake,catched) VALUES ('{login}','{password}','{name}',{0},{3000},{0},'{GameManager.DateTimeToString(new DateTime(DateTime.UtcNow.Year, DateTime.UtcNow.Month, DateTime.UtcNow.Day))}',{0})");
        InsertIntoInformation("storage", "login,yp,vp,rp,pig,cow,chicken,fish,m1,m2,m3", $"'{login}',{0},{0},{0},{0},{0},{0},{0},{0},{0},{0}");
        for (int i = 0; i < 9; i++)
        {
            InsertIntoInformation("beds", "login,number", $"'{login}',{i}");
        }
        for (int i = 0; i < 9; i++)
        {
            InsertIntoInformation("animals", "login,number,type,state,endTime", $"'{login}',{i},{0},{0},'{GameManager.DateTimeToString(DateTime.UtcNow)}'");
        }
        for (int i = 0; i < 9; i++)
        {
            InsertIntoInformation("fish", "login,number,type,state,endTime", $"'{login}',{i},{0},{0},'{GameManager.DateTimeToString(DateTime.UtcNow)}'");
        }
        for (int i = 0; i < 9; i++)
        {
            InsertIntoInformation("machine", "login,number,type", $"'{login}',{i},{0}");
        }
        InsertIntoInformation("tasks","login,number,lable,text,state,type",$"'{login}',{0},'Farm is farm','collect 5 rp, 5 vp, 5 5 yp',{0},{0}");
        InsertIntoInformation("tasks","login,number,lable,text,state,type",$"'{login}',{1},'Oh my animals','collect 1 cow, 1 chicken, 1 pig',{0},{1}");
        InsertIntoInformation("tasks","login,number,lable,text,state,type",$"'{login}',{2},'MONEY','collect 5 coins',{0},{2}");
        InsertIntoInformation("stuff","login,first,second,third",$"'{login}',{0},{0},{0}");
        InsertIntoInformation("tutorial","login,farm,factory,store,city",$"'{login}',{0},{0},{0},{0}");
        for (int i = 0; i < 10; i++)
        {
            InsertIntoInformation("customers","login,number,type,willingtype",$"'{login}',{i},{UnityEngine.Random.Range(0,3)},{UnityEngine.Random.Range(0, 3)}");
        }
    }
    // метод обнавления информации
    public static void UpdateInformation(string table, string set, string where)
    {
        NonQueryCommand($"UPDATE {table} SET {set} WHERE {where}");
    }
    // получение информации о пользователе
    public static List<string> GetUserInformation(string login, string password)
    {
        List<string> result = new List<string>();
        connection.Open();
        DbDataReader reader = ReaderCommand("*", "users", $"login = '{login}' AND password = '{password}'");
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                result.Add(reader.GetString(0));
                result.Add(reader.GetString(3));
                result.Add(reader.GetString(4));
                result.Add(reader.GetString(5));
                result.Add(reader.GetString(6));
                result.Add(reader.GetString(7));
                result.Add(reader.GetString(8));
            }
        }
        connection.Close();
        return result;
    }
    // получение информации о грядках
    public static List<string> GetBedsInformation(string login, int number)
    {
        List<string> result = new List<string>();
        connection.Open();
        DbDataReader reader2 = JoinReaderCommand("*", "beds ", "landing", "beds.idbeds = landing.idbeds",$"login = '{login}' and number = {number}");
        if (reader2.HasRows)
        {
            while (reader2.Read())
            {
                result.Add(reader2.GetValue(reader2.GetOrdinal("idbeds")).ToString());
                result.Add(reader2.GetValue(reader2.GetOrdinal("type")).ToString());
                result.Add(reader2.GetValue(reader2.GetOrdinal("state")).ToString());
                result.Add(reader2.GetValue(reader2.GetOrdinal("endTime")).ToString());
            }
        }
        connection.Close();
        return result;
    }
    // получение информации о посевах
    public static List<string> GetLandigsInformation(int idbeds)
    {
        List<string> result = new List<string>();
        connection.Open();
        DbDataReader reader = ReaderCommand("*", "landing", $"idbeds = {idbeds}");
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                result.Add(reader.GetString(2));
                result.Add(reader.GetString(3));
                result.Add(reader.GetString(4));
            }
        }
        else 
        {
            connection.Close();
            InsertIntoInformation("landing", "idbeds,type,state,endTime", $"{idbeds},{0},{0},'{GameManager.DateTimeToString(DateTime.UtcNow)}'");
            connection.Open();
            reader = ReaderCommand("*", "landing", $"idbeds = {idbeds}");
            while (reader.Read())
            {
                result.Add(reader.GetString(2));
                result.Add(reader.GetString(3));
                result.Add(reader.GetString(4));
            }
        }
        connection.Close();
        return result;
    }
    // получение информации о складе
    public static List<string> GetStorageInformation(string login)
    {
        List<string> result = new List<string>();
        connection.Open();
        DbDataReader reader = ReaderCommand("*", "storage", $"login = '{login}'");
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                result.Add(reader.GetString(2));
                result.Add(reader.GetString(3));
                result.Add(reader.GetString(4));
                result.Add(reader.GetString(5));
                result.Add(reader.GetString(6));
                result.Add(reader.GetString(7));
                result.Add(reader.GetString(8));
                result.Add(reader.GetString(9));
                result.Add(reader.GetString(10));
                result.Add(reader.GetString(11));
            }
        }
        connection.Close();
        return result;
    }
    // получение информации о живытных
    public static List<string> GetAnimalsInformation(string login, int number)
    {
        List<string> result = new List<string>();
        connection.Open();
        DbDataReader reader = ReaderCommand("*", "animals", $"login = '{login}' AND number = {number}");
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                result.Add(reader.GetString(0));
                result.Add(reader.GetString(3));
                result.Add(reader.GetString(4));
                result.Add(reader.GetString(5));
            }
        }
        connection.Close();
        return result;
    }
    // получение информации о рыбе
    public static List<string> GetFishInformation(string login, int number)
    {
        List<string> result = new List<string>();
        connection.Open();
        DbDataReader reader = ReaderCommand("*", "fish", $"login = '{login}' AND number = {number}");
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                result.Add(reader.GetString(0));
                result.Add(reader.GetString(3));
                result.Add(reader.GetString(4));
                result.Add(reader.GetString(5));
            }
        }
        connection.Close();
        return result;
    }
    // получение информации о тасках
    public static List<string> GetTaskInformation(string login, int number)
    {
        List<string> result = new List<string>();
        connection.Open();
        DbDataReader reader = ReaderCommand("*", "tasks", $"login = '{login}' AND number = {number}");
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                result.Add(reader.GetString(3));
                result.Add(reader.GetString(4));
                result.Add(reader.GetString(5));
                result.Add(reader.GetString(6));
            }
        }
        connection.Close();
        return result;
    }
    // получение информации о подарках
    public static List<string> GetGiftInformation(int day)
    {
        List<string> result = new List<string>();
        connection.Open();
        DbDataReader reader = ReaderCommand("*", "gift", $"day = {day}");
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                result.Add(reader.GetString(2));
            }
        }
        connection.Close();
        return result;
    }
    // получение информации о машинах
/*    public static List<string> GetMachineInformation(string login, int number)
    {
        List<string> result = new List<string>();
        connection.Open();
        DbDataReader reader = ReaderCommand("*", "machine", $"login = '{login}' AND number = {number}");
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                result.Add(reader.GetString(0));
                result.Add(reader.GetString(3));
                result.Add(reader.GetString(4));
                result.Add(reader.GetString(5));
            }
        }
        connection.Close();
        return result;
    }*/
    // получение информации о машинах
    public static List<string> GetMachineInformation(string login, int number)
    {
        List<string> result = new List<string>();
        connection.Open();
        DbDataReader reader2 = JoinReaderCommand("*", "machine", "machineorder", "machine.idmachine = machineorder.idmachine", $"login = '{login}' and number = {number}");
        if (reader2.HasRows)
        {
            while (reader2.Read())
            {
                result.Add(reader2.GetValue(reader2.GetOrdinal("idmachine")).ToString());
                result.Add(reader2.GetValue(reader2.GetOrdinal("type")).ToString());

                result.Add(reader2.GetValue(reader2.GetOrdinal("typeOne")).ToString());
                result.Add(reader2.GetValue(reader2.GetOrdinal("endTimeOne")).ToString());

                result.Add(reader2.GetValue(reader2.GetOrdinal("typeTwo")).ToString());
                result.Add(reader2.GetValue(reader2.GetOrdinal("endTimeTwo")).ToString());

                result.Add(reader2.GetValue(reader2.GetOrdinal("typeThree")).ToString());
                result.Add(reader2.GetValue(reader2.GetOrdinal("endTimeThree")).ToString());

                result.Add(reader2.GetValue(reader2.GetOrdinal("typeFour")).ToString());
                result.Add(reader2.GetValue(reader2.GetOrdinal("endTimeFour")).ToString());

                result.Add(reader2.GetValue(reader2.GetOrdinal("typeFive")).ToString());
                result.Add(reader2.GetValue(reader2.GetOrdinal("endTimeFive")).ToString());
            }
        }
        connection.Close();
        return result;
    }

    // получение информации о персонале
    public static List<string> GetStuffInformation(string login)
    {
        List<string> result = new List<string>();
        connection.Open();
        DbDataReader reader = ReaderCommand("*", "stuff", $"login = '{login}'");
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                result.Add(reader.GetString(2));
                result.Add(reader.GetString(3));
                result.Add(reader.GetString(4));
            }
        }
        connection.Close();
        return result;
    }
    // получение информации о покупателях
    public static List<string> GetCustomerInformation(string login, int number)
    {
        List<string> result = new List<string>();
        connection.Open();
        DbDataReader reader = ReaderCommand("*", "customers", $"login = '{login}' AND number = {number}");
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                result.Add(reader.GetString(3));
                result.Add(reader.GetString(4));
            }
        }
        connection.Close();
        return result;
    }
    // получение информации о туториалых
    public static List<string> GetTutorialInformation(string login)
    {
        List<string> result = new List<string>();
        connection.Open();
        DbDataReader reader = ReaderCommand("*", "tutorial", $"login = '{login}'");
        if (reader.HasRows)
        {
            while (reader.Read())
            {
                result.Add(reader.GetString(2));
                result.Add(reader.GetString(3));
                result.Add(reader.GetString(4));
                result.Add(reader.GetString(5));
            }
        }
        connection.Close();
        return result;
    }
}
