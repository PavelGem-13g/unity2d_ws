﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public int id;
    public string login;
    public string password;
    public string username;
    public int crystal;
    public int value;
    public int sday;
    public DateTime ltake;
    public int catched;

    public int yp;
    public int vp;
    public int rp;
    public int pig;
    public int cow;
    public int chicken;
    public int fish;
    public int m1;
    public int m2;
    public int m3;

    public int first;
    public int second;
    public int third;

    public int farm;
    public int factory;
    public int store;
    public int city;

    // инициализация
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        instance = this;
        SQL.Connect();
    }
    // получение информация
    public void Authrize(int id, string login, string password, string username, int crystal, int value, int sday, string ltake, int catched)
    {
        this.id = id;
        this.login = login;
        this.password = password;
        this.username = username;
        this.crystal = crystal;
        this.value = value;
        this.sday = sday;
        this.ltake = FromShortString(ltake);
        this.catched = catched;
        GetStorageInformation(login);
        GetStuffInformation(login);
        GetTutorialInformation(login);
        StartCoroutine(CycledUploaing());
    }
    // получение информации из бд о складе
    void GetStorageInformation(string login)
    {
        List<string> temp = SQL.GetStorageInformation(login);
        yp = Convert.ToInt32(temp[0]);
        vp = Convert.ToInt32(temp[1]);
        rp = Convert.ToInt32(temp[2]);
        pig = Convert.ToInt32(temp[3]);
        cow = Convert.ToInt32(temp[4]);
        chicken = Convert.ToInt32(temp[5]);
        fish = Convert.ToInt32(temp[6]);
        m1 = Convert.ToInt32(temp[7]);
        m2 = Convert.ToInt32(temp[8]);
        m3 = Convert.ToInt32(temp[9]);
    }
    // получение информации из бд о рабочих
    void GetStuffInformation(string login)
    {
        List<string> temp = SQL.GetStuffInformation(login);
        first = Convert.ToInt32(temp[0]);
        second = Convert.ToInt32(temp[1]);
        third = Convert.ToInt32(temp[2]);
    }
    // получение информации из бд о туториалах
    void GetTutorialInformation(string login)
    {
        List<string> temp = SQL.GetTutorialInformation(login);
        farm = Convert.ToInt32(temp[0]);
        factory = Convert.ToInt32(temp[1]);
        store = Convert.ToInt32(temp[2]);
        city = Convert.ToInt32(temp[3]);
    }
    IEnumerator CycledUploaing() 
    {
        while (true)
        {
            SQL.UpdateInformation("users",$"value = {value}", $"login = '{login}'");
            yield return new WaitForSeconds(5f);
        }
    }
    // расшифрование из строки в дату с временем
    public static DateTime FromString(string data)
    {
        string[] parsed = data.Split(new char[] { '.' });
        int[] result = new int[parsed.Length];
        if (result.Length > 0)
        {
            for (int i = 0; i < parsed.Length; i++)
            {
                result[i] = Convert.ToInt32(parsed[i]);
            }
            return new DateTime(result[2], result[1], result[0], result[3], result[4], result[5]);
        }
        return DateTime.UtcNow;
    }
    // расшифрование из строки в дату без времени
    public static DateTime FromShortString(string data)
    {
        string[] parsed = data.Split(new char[] { '.' });
        int[] result = new int[parsed.Length];
        if (result.Length > 0)
        {
            for (int i = 0; i < parsed.Length; i++)
            {
                result[i] = Convert.ToInt32(parsed[i]);
            }
            return new DateTime(result[2], result[1], result[0], 0, 0, 0);
        }
        return DateTime.UtcNow;
    }
    // зашифрование в строку даты с временем
    public static string DateTimeToString(DateTime dateTime)
    {
        return dateTime.Day + "." + dateTime.Month + "." + dateTime.Year + "." + dateTime.Hour + "." + dateTime.Minute + "." + dateTime.Second;
    }
    // зашифрование в строку даты без времени
    public static string DateTimeToShortString(DateTime dateTime)
    {
        return dateTime.Day + "." + dateTime.Month + "." + dateTime.Year;
    }
    // обновление верхней панели
    public void UpdateTopPanel()
    {
        foreach (var item in FindObjectsOfType<ShowUserInformation>())
        {
            item.SetInformation();
        }
    }
    // запуск анимации 
    public void TopPanelAnimation(int value)
    {
        foreach (var item in FindObjectsOfType<TopPanelAnim>())
        {
            item.Animate(value);
        }
    }
}
