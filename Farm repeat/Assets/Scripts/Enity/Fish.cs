﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fish : MonoBehaviour
{
    public int idfish;
    public int number;
    public int state;
    public int type;
    Image image;
    public Sprite fish;
    public Sprite net;
    DateTime endTime;
    public bool isGrowning = false;
    public GameObject chooseFishState;
    //иницализация
    private void Awake()
    {
        number = Convert.ToInt32(gameObject.name);
        image = GetComponent<Image>();
        GetFishInformation();
        SpriteUpdate();
    }
    //метод перед первым кадром
    void Start()
    {
        StartCoroutine(Timer());
    }
    //реактивация рыбы
    public void UnPause()
    {
        StartCoroutine(Timer());
    }
    // получение информации из бд
    void GetFishInformation()
    {
        List<string> temp = SQL.GetFishInformation(GameManager.instance.login, number);
        idfish = Convert.ToInt32(temp[0]);
        type = Convert.ToInt32(temp[1]);
        state = Convert.ToInt32(temp[2]);
        endTime = GameManager.FromString(temp[3]);
    }
    // внешнее изменение состояния
    public void ChangeStateValue()
    {
        state += 1;
        SQL.UpdateInformation("fish", $"state = {state}", $"idfish = {idfish}");
        SpriteUpdate();
    }
    // обработка нажатия на кнопку
    public void ChangeState()
    {
        if (!isGrowning)
        {
            if (state < 2)
            {
                chooseFishState.SetActive(true);
                ChooseFish.instance.localFish = this;
                ChooseFish.instance.Active();
            }
            else if (state == 2 && type == 1)
            {
                Clear();
            }
            else
            {
                chooseFishState.SetActive(true);
                ChooseFish.instance.localFish = this;
                ChooseFish.instance.Active();
            }
            SpriteUpdate();
        }
    }
    // обновление спрайта рыбы
    public void SpriteUpdate()
    {
        if (state == 2 && type==1)
        {
            image.sprite = fish;
        }
        else
        {
            image.sprite = net;
        }
    }
    // установка типа
    public void SetType(int type)
    {
        state += 1;
        int delta = 0;
        this.type = type;
        if (type == 1)
        {
            delta = 10;
        }
        endTime = DateTime.UtcNow + new TimeSpan(0, 0, delta);
        SQL.UpdateInformation("fish", $"endTime = '{GameManager.DateTimeToString(endTime)}'", $"idfish = {idfish}");
        StartCoroutine(Timer());
    }
    // зачистка после выращивания рыбы
    public void Clear()
    {
        GameManager.instance.crystal += type;
        SQL.UpdateInformation("users", $"crystasl = {GameManager.instance.value}", $"login = '{GameManager.instance.login}'");
        GameManager.instance.pig += 1;
        SQL.UpdateInformation("storage", $"fish = {GameManager.instance.pig}", $"login = '{GameManager.instance.login}'");
        GameManager.instance.TopPanelAnimation(type);
        GameManager.instance.UpdateTopPanel();
        type = 0;
        state = 0;
        isGrowning = false;
        SQL.UpdateInformation("fish", $"endTime = '{GameManager.DateTimeToString(DateTime.UtcNow)}'", $"idfish = {idfish}");
        SQL.UpdateInformation("fish", $"type = {type}", $"idfish = {idfish}");
        SQL.UpdateInformation("fish", $"state = {0}", $"idfish = {idfish}");
    }
    // обработка времени события
    IEnumerator Timer()
    {
        Animator animator= GetComponent<Animator>();
        while (DateTime.UtcNow <= endTime)
        {
            isGrowning = true;
            animator.Play("AnimalAnim");
            yield return new WaitForSeconds(0.1f);
        }
        isGrowning = false;
        SpriteUpdate();
    }
}
