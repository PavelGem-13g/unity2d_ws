﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Beds : MonoBehaviour
{
    public int idbeds;
    public int number;
    public int state;
    public int type;
    Image image;
    public Sprite[] states;
    public Sprite[] landing;
    DateTime endTime;
    public Sprite growningSprite;
    public bool isGrowning = false;
    public GameObject chooseBedsState;
    // вцелом как Fish
    private void Awake()
    {
        number = Convert.ToInt32(gameObject.name);
        image = GetComponent<Image>();
        GetBedsInformation();
        //GetLandingsInformation();
        SpriteUpdate();
    }
    void Start()
    {
        StartCoroutine(Timer());
    }
    public void UnPause()
    {
        StartCoroutine(Timer());
    }
    void GetBedsInformation()
    {
        List<string> temp = SQL.GetBedsInformation(GameManager.instance.login, number);
        idbeds = Convert.ToInt32(temp[0]);
        if (temp[1] == "")
        {
            type = 0;
            state = 0;
            endTime = DateTime.UtcNow;
            SQL.InsertIntoInformation("landing", "idbeds,type,state,endTime", $"{idbeds},{type},{state},'{GameManager.DateTimeToString(endTime)}'");
        }
        else
        {
            type = Convert.ToInt32(temp[1]);
            state = Convert.ToInt32(temp[2]);
            endTime = GameManager.FromString(temp[3]);
        }
    }
    /*    void GetLandingsInformation() 
        {
            List<string> temp = SQL.GetLandigsInformation(idbeds);
            type = Convert.ToInt32(temp[0]);
            state = Convert.ToInt32(temp[1]);
            endTime = GameManager.FromString(temp[2]);
        }*/
    public void ChangeStateValue()
    {
        state += 1;
        SQL.UpdateInformation("landing", $"state = {state}", $"idbeds = {idbeds}");
        SpriteUpdate();
    }
    public void ChangeState()
    {
        if (!isGrowning)
        {
            if (state < 2)
            {
                chooseBedsState.SetActive(true);
                ChooseBedsState.instance.localBeds = this;
                ChooseBedsState.instance.Active();
            }
            else if (state == 2 && type != 0 && !isGrowning)
            {
                Clear();
            }
            else
            {
                chooseBedsState.SetActive(true);
                ChooseBedsState.instance.localBeds = this;
                ChooseBedsState.instance.Active();
            }
            SpriteUpdate();
            SQL.UpdateInformation("landing", $"state = {state}", $"idbeds = {idbeds}");
        }
    }
    public void SpriteUpdate()
    {
        if (state == 2 && type != 0)
        {
            image.sprite = landing[type - 1];
        }
        else
        {
            image.sprite = states[state];
        }
    }
    public void SetType(int type)
    {
        int delta = 0;
        this.type = type;
        if (type == 1)
        {
            delta = 2;
        }
        if (type == 2)
        {
            delta = 10;
        }
        if (type == 3)
        {
            delta = 30;
        }
        endTime = DateTime.UtcNow + new TimeSpan(0, 0, delta);
        SQL.UpdateInformation("landing", $"endTime = '{GameManager.DateTimeToString(endTime)}'", $"idbeds = {idbeds}");
        SQL.UpdateInformation("landing", $"state = {state}", $"idbeds = {idbeds}");
        SQL.UpdateInformation("landing", $"type = {type}", $"idbeds = {idbeds}");
        StartCoroutine(Timer());
    }
    public void Clear()
    {
        GameManager.instance.value += type;
        SQL.UpdateInformation("users", $"value = {GameManager.instance.value}", $"login = '{GameManager.instance.login}'");
        if (type == 1)
        {
            GameManager.instance.yp += 1;
            SQL.UpdateInformation("storage", $"yp = {GameManager.instance.yp}", $"login = '{GameManager.instance.login}'");
        }
        if (type == 2)
        {
            GameManager.instance.vp += 1;
            SQL.UpdateInformation("storage", $"vp = {GameManager.instance.vp}", $"login = '{GameManager.instance.login}'");
        }
        if (type == 3)
        {
            GameManager.instance.rp += 1;
            SQL.UpdateInformation("storage", $"rp = {GameManager.instance.rp}", $"login = '{GameManager.instance.login}'");
        }
        foreach (var item in FindObjectsOfType<ShowUserInformation>())
        {
            item.SetInformation();
        }
        type = 0;
        state = 0;
        isGrowning = false;
        GameManager.instance.TopPanelAnimation(type);
        SQL.UpdateInformation("landing", $"endTime = '{GameManager.DateTimeToString(DateTime.UtcNow)}'", $"idbeds = {idbeds}");
        SQL.UpdateInformation("landing", $"state = {state}", $"idbeds = {idbeds}");
        SQL.UpdateInformation("landing", $"type = {type}", $"idbeds = {idbeds}");
    }
    IEnumerator Timer()
    {
        while (DateTime.UtcNow <= endTime)
        {
            image.sprite = growningSprite;
            isGrowning = true;
            state = 2;
            yield return new WaitForSeconds(0.1f);
        }
        SQL.UpdateInformation("landing", $"state = {state}", $"idbeds = {idbeds}");
        isGrowning = false;
        SpriteUpdate();
    }
}
