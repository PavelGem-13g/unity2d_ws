﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseProducts : MonoBehaviour
{
    public static ChooseProducts instance;
    [SerializeField] GameObject typeOne;
    [SerializeField] GameObject typeTwo;
    [SerializeField] GameObject sell;
    [SerializeField] GameObject order;
    [SerializeField] GameObject panel;
    public Machine localMachine;
    private void Awake()
    {
        instance = this;
        gameObject.SetActive(false);
    }
    public void SetType(int type)
    {
        localMachine.SetType(type);
        gameObject.SetActive(false);
    }
    public void Sell()
    {
        GameManager.instance.value += 1000;
        SQL.UpdateInformation("users", $"value = {GameManager.instance.value}", $"login = '{GameManager.instance.login}'");
        SQL.UpdateInformation("machine",$"type = {0}", $"idmachine = {localMachine.idmachine}");
        localMachine.type = 0;
        localMachine.SpriteUpdate();
        GameManager.instance.UpdateTopPanel();
        gameObject.SetActive(false);
    }
    public void Active()
    {
        StartCoroutine(Zoom());
        foreach (var item in FindObjectsOfType<Order>())
        {
            item.ShowInformation();
        }
        GetComponent<Chooser>().OpenOnlyOne();
        GetComponent<RectTransform>().transform.position = localMachine.GetComponent<RectTransform>().transform.position;
        transform.position = new Vector3(transform.position.x, transform.position.y + 1f);
        if (localMachine.type == 1)
        {
            panel.SetActive(true);
            typeOne.SetActive(true);
            typeTwo.SetActive(false);
        }
        if (localMachine.type == 2)
        {
            panel.SetActive(true);
            typeOne.SetActive(false);
            typeTwo.SetActive(true);
        }
        order.SetActive(true);
        sell.SetActive(true);
    }
    IEnumerator Zoom()
    {
        transform.localScale = new Vector3(0.75f,0.75f);
        while (transform.localScale.x < 1)
        {
            transform.localScale = new Vector3(transform.localScale.x + 0.01f, transform.localScale.y + 0.01f, transform.localScale.z + 0.01f);
            yield return new WaitForEndOfFrame();
        }
    }
}
