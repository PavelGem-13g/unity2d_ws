﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseMachine : MonoBehaviour
{
    public static ChooseMachine instance;
    public GameObject set;
    public Machine localMachine;
    private void Awake()
    {
        instance = this;
        gameObject.SetActive(false);
    }
    public void SetType(int type)
    {
        localMachine.SetMachineType(type);
        set.SetActive(false);
        gameObject.SetActive(false);
    }
    public void Active()
    {
        StartCoroutine(Zoom());
        GetComponent<Chooser>().OpenOnlyOne();
        GetComponent<RectTransform>().transform.position = localMachine.GetComponent<RectTransform>().transform.position;
        transform.position = new Vector3(transform.position.x - 1f, transform.position.y + 0.5f);
        if (localMachine.type == 0)
        {
            set.SetActive(true);
        }

    }
    IEnumerator Zoom()
    {
        transform.localScale = Vector3.zero;
        while (transform.localScale.x < 1)
        {
            transform.localScale = new Vector3(transform.localScale.x + 0.01f, transform.localScale.y + 0.01f, transform.localScale.z + 0.01f);
            yield return new WaitForEndOfFrame();
        }
    }

}
