﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// вцелом всё как в Fish
public class Animals : MonoBehaviour
{
    public int idanimals;
    public int number;
    public int state;
    public int type;
    Image image;
    public Sprite[] animals;
    DateTime endTime;
    public bool isGrowning = false;
    public GameObject chooseAnimalState;

    private void Awake()
    {   
        number = Convert.ToInt32(gameObject.name);
        image = GetComponent<Image>();
        GetAnimalsInformation();
        SpriteUpdate();
    }
    void Start()
    {
        StartCoroutine(Timer());
    }
    public void UnPause()
    {
        StartCoroutine(Timer());
    }
    void GetAnimalsInformation()
    {
        List<string> temp = SQL.GetAnimalsInformation(GameManager.instance.login, number);
        idanimals = Convert.ToInt32(temp[0]);
        type = Convert.ToInt32(temp[1]);
        state = Convert.ToInt32(temp[2]);
        endTime = GameManager.FromString(temp[3]);
    }
    public void ChangeStateValue()
    {
        state += 1;
        SQL.UpdateInformation("animals", $"state = {state}", $"idanimals = {idanimals}");
        SpriteUpdate();
    }
    public void ChangeState()
    {
        if (!isGrowning)
        {
            if (state < 1)
            {
                chooseAnimalState.SetActive(true);
                ChooseAnimals.instance.localAnimal = this;
                ChooseAnimals.instance.Active();
            }
            else if (state == 1 && type != 0 && !isGrowning)
            {
                Clear();
            }
            else
            {
                chooseAnimalState.SetActive(true);
                ChooseAnimals.instance.localAnimal = this;
                ChooseAnimals.instance.Active();
            }
            SpriteUpdate();
        }
    }
    public void SpriteUpdate()
    {
        image.sprite = animals[type];
    }
    public void SetType(int type)
    {
        int delta = 0;
        this.type = type;
        if (type == 1)
        {
            delta = 2;
        }
        if (type == 2)
        {
            delta = 10;
        }
        if (type == 3)
        {
            delta = 30;
        }
        SpriteUpdate();
        endTime = DateTime.UtcNow + new TimeSpan(0, 0, delta);
        SQL.UpdateInformation("animals", $"endTime = '{GameManager.DateTimeToString(endTime)}'", $"idanimals = {idanimals}");
        StartCoroutine(Timer());
    }
    public void Clear()
    {
        GameManager.instance.crystal += type;
        SQL.UpdateInformation("users", $"crystasl = {GameManager.instance.value}", $"login = '{GameManager.instance.login}'");
        if (type == 1)
        {
            GameManager.instance.pig += 1;
            SQL.UpdateInformation("storage", $"pig = {GameManager.instance.pig}", $"login = '{GameManager.instance.login}'");
        }
        if (type == 2)
        {
            GameManager.instance.chicken += 1;
            SQL.UpdateInformation("storage", $"chicken = {GameManager.instance.chicken}", $"login = '{GameManager.instance.login}'");
        }
        if (type == 3)
        {
            GameManager.instance.cow += 1;
            SQL.UpdateInformation("storage", $"cow = {GameManager.instance.cow}", $"login = '{GameManager.instance.login}'");
        }
        GameManager.instance.UpdateTopPanel();
        GameManager.instance.TopPanelAnimation(type);
        type = 0;
        state = 0;
        isGrowning = false;
        SQL.UpdateInformation("animals", $"endTime = '{GameManager.DateTimeToString(DateTime.UtcNow)}'", $"idanimals = {idanimals}");
        SQL.UpdateInformation("animals", $"type = {type}", $"idanimals = {idanimals}");
    }
    IEnumerator Timer()
    {
        while (DateTime.UtcNow <= endTime)
        {
            isGrowning = true;
            GetComponent<Animator>().Play("AnimalAnim");
            yield return new WaitForSeconds(0.1f);
        }
        isGrowning = false;
        SpriteUpdate();
    }
}
