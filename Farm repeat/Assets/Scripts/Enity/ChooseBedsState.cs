﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseBedsState : MonoBehaviour
{
    public static ChooseBedsState instance;
    public GameObject pour;
    public GameObject dig;
    public GameObject plants;
    public Beds localBeds;
    private void Awake()
    {
        instance = this;
        gameObject.SetActive(false);
    }
    public void Pour() 
    {
        localBeds.ChangeStateValue();
        pour.SetActive(false);
        gameObject.SetActive(false);
    }
    public void Dig()
    {
        localBeds.ChangeStateValue();
        dig.SetActive(false);
        gameObject.SetActive(false);
    }
    public void SetType(int type) 
    {
        localBeds.SetType(type);
        plants.gameObject.SetActive(false);
        gameObject.SetActive(false);
    }
    public void Active() 
    {
        StartCoroutine(Zoom());
        GetComponent<RectTransform>().transform.position = localBeds.GetComponent<RectTransform>().transform.position;
        GetComponent<Chooser>().OpenOnlyOne();
        transform.position = new Vector3(transform.position.x-1.5f, transform.position.y + 0.5f);
        if(localBeds.state==0) 
        {
            dig.SetActive(true);
            pour.SetActive(false);
            plants.SetActive(false);
        }
        if (localBeds.state == 1)
        {
            dig.SetActive(false);
            pour.SetActive(true);
            plants.SetActive(false);
        }
        if (localBeds.state==2)
        {
            dig.SetActive(false);
            pour.SetActive(false);
            plants.SetActive(true);
        }
    }
    IEnumerator Zoom() 
    {
        transform.localScale = Vector3.zero;
        while (transform.localScale.x<1)
        {
            transform.localScale = new Vector3(transform.localScale.x+0.01f, transform.localScale.y + 0.01f, transform.localScale.z + 0.01f);
            yield return new WaitForEndOfFrame();
        }
    }
}
