﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseAnimals : MonoBehaviour
{
    public static ChooseAnimals instance;
    public GameObject clear;
    public GameObject animals;
    public Animals localAnimal;
    private void Awake()
    {
        instance = this;
        gameObject.SetActive(false);
    }
    public void SetType(int type)
    {
        localAnimal.SetType(type);
        animals.SetActive(false);
        gameObject.SetActive(false);
    }
    public void Clear() 
    {
        localAnimal.ChangeStateValue();
        clear.SetActive(false);
        gameObject.SetActive(false);
    }
    public void Active()
    {
        StartCoroutine(Zoom());
        GetComponent<Chooser>().OpenOnlyOne();
        GetComponent<RectTransform>().transform.position = localAnimal.GetComponent<RectTransform>().transform.position;
        transform.position = new Vector3(transform.position.x - 1.5f, transform.position.y + 0.5f);
        if (localAnimal.state==0) 
        {
            clear.SetActive(true);
            animals.SetActive(false);
        }
        if (localAnimal.state == 1) 
        {
            clear.SetActive(false);
            animals.SetActive(true);
        }
    }
    IEnumerator Zoom()
    {
        transform.localScale = Vector3.zero;
        while (transform.localScale.x < 1)
        {
            transform.localScale = new Vector3(transform.localScale.x + 0.01f, transform.localScale.y + 0.01f, transform.localScale.z + 0.01f);
            yield return new WaitForEndOfFrame();
        }
    }
}
