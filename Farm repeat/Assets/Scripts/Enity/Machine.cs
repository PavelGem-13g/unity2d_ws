﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// вцелом как в Fish
public class Machine : MonoBehaviour
{
    public int idmachine;
    public int number;
    public int state;

    public int type;

    public int typeOne;
    public int typeTwo;
    public int typeThree;
    public int typeFour;
    public int typeFive;
    DateTime endTimeOne;
    DateTime endTimeTwo;
    DateTime endTimeThree;
    DateTime endTimeFour;
    DateTime endTimeFive;

    Image image;
    public Sprite[] machines;
    public Sprite[] states;
    public bool isGrowning = false;
    public GameObject chooseMachineState;
    [SerializeField] GameObject chooseProducts;
    public GameObject brekObject;
    Animator animator;
    void Start()
    {
        animator = GetComponent<Animator>();
        number = Convert.ToInt32(gameObject.name);
        image = GetComponent<Image>();
        GetMachineInformation();
        SpriteUpdate();
        Timer();
    }
    public void UnPause()
    {
        Timer();
    }

    public void ChangeStateValue()
    {
        state += 1;
        SQL.UpdateInformation("machineorder", $"state = {state}", $"idmachineorder = {idmachine}");
        SpriteUpdate();
    }
    void GetMachineInformation()
    {
        List<string> temp = SQL.GetMachineInformation(GameManager.instance.login, number);
        idmachine = Convert.ToInt32(temp[0]);
        type = Convert.ToInt32(temp[1]);
        if (temp[3] == "")
        {
            state = 0;
            endTimeOne = DateTime.UtcNow;
            typeOne = 0;
            endTimeTwo = DateTime.UtcNow;
            typeTwo = 0;
            endTimeThree = DateTime.UtcNow;
            typeThree = 0;
            endTimeFour = DateTime.UtcNow;
            typeFour = 0;
            endTimeFive = DateTime.UtcNow;
            typeFive = 0;
            SQL.InsertIntoInformation("machineorder",
                "idmachine,typeOne,endTimeOne,typeTwo,endTimeTwo,typeThree,endTimeThree,typeFour,endTimeFour,typeFive,endTimeFive",
                $"{idmachine},{typeOne},'{GameManager.DateTimeToString(endTimeOne)}',{typeTwo},'{GameManager.DateTimeToString(endTimeTwo)}',{typeThree},'{GameManager.DateTimeToString(endTimeThree)}',{typeFour},'{GameManager.DateTimeToString(endTimeFour)}',{typeFive},'{GameManager.DateTimeToString(endTimeFive)}'");
        }
        else
        {
            typeOne = Convert.ToInt32(temp[2]);
            endTimeOne = GameManager.FromString(temp[3]);

            typeTwo = Convert.ToInt32(temp[4]);
            endTimeTwo = GameManager.FromString(temp[5]);

            typeThree = Convert.ToInt32(temp[6]);
            endTimeThree = GameManager.FromString(temp[7]);

            typeFour = Convert.ToInt32(temp[8]);
            endTimeFour = GameManager.FromString(temp[9]);

            typeFive = Convert.ToInt32(temp[10]);
            endTimeFive = GameManager.FromString(temp[11]);
        }
    }
    public void ChangeState()
    {
        if (!isGrowning)
        {
            if (type == 0)
            {
                chooseMachineState.SetActive(true);
                ChooseMachine.instance.localMachine = this;
                ChooseMachine.instance.Active();
            }
            else
            {
                chooseProducts.SetActive(true);
                ChooseProducts.instance.localMachine = this;
                ChooseProducts.instance.Active();
            }
        }
    }
    public void SpriteUpdate()
    {
        if (type == 0)
        {
            image.sprite = states[1];
            transform.localScale = Vector3.one;
        }
        else
        {
            image.sprite = machines[type - 1];
            transform.localScale = Vector3.one * 2;
            brekObject.SetActive(false);
        }
        image.SetNativeSize();
    }
    public void SetMachineType(int type)
    {
        this.type = type;
        SQL.UpdateInformation("machine", $"type = {type}", $"idmachine = {idmachine}");
        SpriteUpdate();

    }
    public void SetType(int type)
    {
        int delta = 0;
        if (type == 1)
        {
            delta = 10;
        }
        if (type == 2)
        {
            delta = 20;
        }
        if (type == 3)
        {
            delta = 30;
        }
        Debug.Log(delta);
        if (typeOne == 0)
        {
            typeOne = type;
            Debug.Log(typeOne);
            endTimeOne = DateTime.UtcNow + new TimeSpan(0, 0, delta);
            SQL.UpdateInformation("machineorder", $"endTimeOne = '{GameManager.DateTimeToString(endTimeOne)}'", $"idmachine = {idmachine}");
            SQL.UpdateInformation("machineorder", $"typeOne = {typeOne}", $"idmachine = {idmachine}");
        }
        else if (typeTwo == 0)
        {
            typeTwo = type;
            Debug.Log(typeTwo);
            endTimeTwo = DateTime.UtcNow + new TimeSpan(0, 0, delta);
            SQL.UpdateInformation("machineorder", $"endTimeTwo = '{GameManager.DateTimeToString(endTimeTwo)}'", $"idmachine = {idmachine}");
            SQL.UpdateInformation("machineorder", $"typeTwo = {typeTwo}", $"idmachine = {idmachine}");
        }
        else if (typeThree == 0)
        {
            typeThree = type;
            Debug.Log(typeThree);
            endTimeThree = DateTime.UtcNow + new TimeSpan(0, 0, delta);
            SQL.UpdateInformation("machineorder", $"endTimeThree = '{GameManager.DateTimeToString(endTimeThree)}'", $"idmachine = {idmachine}");
            SQL.UpdateInformation("machineorder", $"typeThree = {typeThree}", $"idmachine = {idmachine}");
        }
        else if (typeFour == 0)
        {
            typeFour = type;
            Debug.Log(typeFour);
            endTimeFour = DateTime.UtcNow + new TimeSpan(0, 0, delta);
            SQL.UpdateInformation("machineorder", $"endTimeFour = '{GameManager.DateTimeToString(endTimeFour)}'", $"idmachine = {idmachine}");
            SQL.UpdateInformation("machineorder", $"typeFour = {typeFour}", $"idmachine = {idmachine}");
        }
        else if (typeFive == 0)
        {
            typeFive = type;
            Debug.Log(typeFive);
            endTimeFive = DateTime.UtcNow + new TimeSpan(0, 0, delta);
            SQL.UpdateInformation("machineorder", $"endTimeFive = '{GameManager.DateTimeToString(endTimeFive)}'", $"idmachine = {idmachine}");
            SQL.UpdateInformation("machineorder", $"typeFive = {typeFive}", $"idmachine = {idmachine}");
        }
        Timer();
    }
    public void Clear()
    {
        GameManager.instance.crystal += type;
        SQL.UpdateInformation("users", $"crystasl = {GameManager.instance.value}", $"login = '{GameManager.instance.login}'");
        GameManager.instance.UpdateTopPanel();
        GameManager.instance.TopPanelAnimation(typeOne);
        if (typeOne == 1)
        {
            GameManager.instance.m1 += 1;
            SQL.UpdateInformation("storage", $"m1 = {GameManager.instance.m1}", $"login = '{GameManager.instance.login}'");
        }
        if (typeOne == 2)
        {
            GameManager.instance.m2 += 1;
            SQL.UpdateInformation("storage", $"m2 = {GameManager.instance.m2}", $"login = '{GameManager.instance.login}'");
        }
        if (typeOne == 3)
        {
            GameManager.instance.m3 += 1;
            SQL.UpdateInformation("storage", $"m3 = {GameManager.instance.m3}", $"login = '{GameManager.instance.login}'");
        }
        isGrowning = false;
        typeOne = typeTwo;
        typeTwo = typeThree;
        typeThree = typeFour;
        typeFour = typeFive;
        typeFive = 0;
        endTimeOne = endTimeTwo;
        endTimeTwo = endTimeThree;
        endTimeThree = endTimeFour;
        endTimeFour = endTimeFive;
        endTimeFive = DateTime.UtcNow;

        SQL.UpdateInformation("machineOrder", $"endTimeOne = '{GameManager.DateTimeToString(DateTime.UtcNow)}'", $"idmachine = {idmachine}");
        SQL.UpdateInformation("machineOrder", $"typeOne = {typeOne}", $"idmachine = {idmachine}");

        SQL.UpdateInformation("machineOrder", $"endTimeTwo = '{GameManager.DateTimeToString(DateTime.UtcNow)}'", $"idmachine = {idmachine}");
        SQL.UpdateInformation("machineOrder", $"typeTwo = {typeTwo}", $"idmachine = {idmachine}");

        SQL.UpdateInformation("machineOrder", $"endTimeThree = '{GameManager.DateTimeToString(DateTime.UtcNow)}'", $"idmachine = {idmachine}");
        SQL.UpdateInformation("machineOrder", $"typeThree = {typeThree}", $"idmachine = {idmachine}");

        SQL.UpdateInformation("machineOrder", $"endTimeFour = '{GameManager.DateTimeToString(DateTime.UtcNow)}'", $"idmachine = {idmachine}");
        SQL.UpdateInformation("machineOrder", $"typeFour = {typeFour}", $"idmachine = {idmachine}");

        SQL.UpdateInformation("machineOrder", $"endTimeFive = '{GameManager.DateTimeToString(DateTime.UtcNow)}'", $"idmachine = {idmachine}");
        SQL.UpdateInformation("machineOrder", $"typeFive = {typeFive}", $"idmachine = {idmachine}");

        Timer();
    }
    void Timer()
    {
        SpriteUpdate();
        if ((endTimeOne - DateTime.UtcNow).Seconds > 0)
        {
            animator.Play("AnimalAnim");
            if (UnityEngine.Random.Range(0, 100) == 0)
            {
                Invoke("Break", (endTimeOne - DateTime.UtcNow).Seconds / 2);
            }
            else
            {
                Debug.Log("Seconds" + (endTimeOne - DateTime.UtcNow).Seconds);
                //isGrowning = true;
                Invoke("Wait", (endTimeOne - DateTime.UtcNow).Seconds);
            }
            
        }
    }
    void Wait()
    {
        isGrowning = false;
        animator.Play("State");
        Clear();
    }
    void Break()
    {
        SpriteUpdate();
    }
}
