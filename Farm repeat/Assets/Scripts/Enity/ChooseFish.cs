﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseFish : MonoBehaviour
{
    public static ChooseFish instance;
    public GameObject clear;
    public GameObject set;
    public Fish localFish;
    private void Awake()
    {
        instance = this;
        gameObject.SetActive(false);
    }
    public void SetType(int type)
    {
        localFish.SetType(type);
        set.SetActive(false);
        gameObject.SetActive(false);
    }
    public void Clear()
    {
        localFish.ChangeStateValue();
        clear.SetActive(false);
        gameObject.SetActive(false);
    }
    public void Active()
    {
        StartCoroutine(Zoom());
        GetComponent<Chooser>().OpenOnlyOne();
        GetComponent<RectTransform>().transform.position = localFish.GetComponent<RectTransform>().transform.position;
        transform.position = new Vector3(transform.position.x - 1.5f, transform.position.y + 0.5f);
        if (localFish.state == 0)
        {
            clear.SetActive(true);
            set.SetActive(false);
        }
        if (localFish.state == 1)
        {
            clear.SetActive(false);
            set.SetActive(true);
        }
    }
    IEnumerator Zoom()
    {
        transform.localScale = Vector3.zero;
        while (transform.localScale.x < 1)
        {
            transform.localScale = new Vector3(transform.localScale.x + 0.01f, transform.localScale.y + 0.01f, transform.localScale.z + 0.01f);
            yield return new WaitForEndOfFrame();
        }
    }

}
