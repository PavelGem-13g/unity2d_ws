﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Authrization : MonoBehaviour
{
    public InputField signUpName;
    public InputField signUpLogin;
    public InputField signUpPassword;
    public Text signUpError;
    public GameObject signUpObject;

    public InputField logInLogin;
    public InputField logInPassword;
    public Text loginInError;

    public void AddUser()
    {
        try
        {
            SQL.AddPlayer(signUpLogin.text, signUpPassword.text, signUpName.text);
            signUpObject.SetActive(false);

        }
        catch
        {
            signUpError.text = "Ошибка";
        }
    }
    public void Authrize() 
    {
/*        try
        {*/
            List<string> temp = SQL.GetUserInformation(logInLogin.text, logInPassword.text);
            if (temp.Count>0)
            {
                GameManager.instance.Authrize(Convert.ToInt32(temp[0]), logInLogin.text, logInPassword.text, temp[1], Convert.ToInt32(temp[2]), Convert.ToInt32(temp[3]), Convert.ToInt32(temp[4]), temp[5], Convert.ToInt32(temp[6]));
                loginInError.text = $"Hello {temp[1]}";
                SceneManager.LoadScene(1);
            }
            else 
            {
                loginInError.text = "Неверный логин или пароль";
            }
/*        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
            loginInError.text = $"Ошибка {e.Message:10}";
        }*/
    }
}
