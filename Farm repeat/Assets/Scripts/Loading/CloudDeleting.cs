﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudDeleting : MonoBehaviour
{
    public GameObject[] clouds;
    public GameObject[] cloudsPositions;
    // метод перед первым кадром
    private void Start()
    {
        StartCoroutine(Delete());    
    }
    // куратина, отвечающая за плавное удаление облаков
    IEnumerator Delete() 
    {
        while (clouds[0].GetComponent<SpriteRenderer>().color.a > 0)
        {
            for (int i = 0; i < clouds.Length; i++)
            {
                clouds[i].transform.position = Vector3.MoveTowards(clouds[i].transform.position, cloudsPositions[i].transform.position, 0.01f);
                clouds[i].GetComponent<SpriteRenderer>().color = new Color(
                    clouds[i].GetComponent<SpriteRenderer>().color.r, 
                    clouds[i].GetComponent<SpriteRenderer>().color.g,
                    clouds[i].GetComponent<SpriteRenderer>().color.b,
                    clouds[i].GetComponent<SpriteRenderer>().color.a-0.01f);
            yield return new WaitForEndOfFrame();
            }
        }
        foreach (var item in clouds)
        {
            Destroy(item);
        }
        foreach (var item in cloudsPositions)
        {
            Destroy(item);
        }
        Destroy(gameObject);
    }
}
