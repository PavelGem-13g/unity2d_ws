﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class LoadingBar : MonoBehaviour
{
    public UnityEvent endOfLoading;
    Image image;
    private void Awake()
    {
        image = GetComponent<Image>();
    }
    void Start()
    {
        StartCoroutine(Loading());
    }

    IEnumerator Loading() 
    {
        while (image.fillAmount<1f)
        {
            image.fillAmount += 0.01f;
            yield return new WaitForEndOfFrame();
        }
        endOfLoading.Invoke();
    }
}
