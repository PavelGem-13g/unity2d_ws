﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TouchToStart : MonoBehaviour
{
    public UnityEvent touchToStart;
    void Update()
    {
        if (Input.anyKey) 
        {
            touchToStart.Invoke();
        }
    }
}
