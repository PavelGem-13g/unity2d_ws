﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StuffController : MonoBehaviour
{
    public static StuffController instance;
    public Text stuffName;
    public Text cost;
    public int number;
    public GameObject AllPeopleHired;
    public Button hired;
    private void Awake()
    {
        instance = this;
        if (GameManager.instance.third != 0)
        {
            GameManager.instance.third = number;
            hired.interactable = false;
            AllPeopleHired.SetActive(true);
        }
    }
    public void SetStuff(string stuffName, int cost, int number) 
    {
        this.stuffName.text = stuffName;
        this.cost.text = "Cost: " + cost.ToString();
        this.number = number;
    }
    public void Hire() 
    {
        hired.interactable = true;
        if (GameManager.instance.first==0) 
        {
            GameManager.instance.first = number;
            SQL.UpdateInformation("stuff",$"first = {GameManager.instance.first}",$"login = '{GameManager.instance.login}'");
        }
        else if (GameManager.instance.second == 0)
        {
            GameManager.instance.second = number;
            SQL.UpdateInformation("stuff",$"second = {GameManager.instance.second}",$"login = '{GameManager.instance.login}'");
        }
        else if (GameManager.instance.third == 0)
        {
            GameManager.instance.third = number;
            SQL.UpdateInformation("stuff",$"third = {GameManager.instance.third}",$"login = '{GameManager.instance.login}'");
            hired.interactable = false;
            AllPeopleHired.SetActive(true);        
        }
    }
    public void SetDefaultStuff() 
    {
        SetStuff("nam", 10,1);
    }
}
