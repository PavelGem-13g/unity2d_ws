﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stuff : MonoBehaviour
{
    public string stuffName;
    public int value;
    public int number;
    public void SetStuff() 
    {
        StuffController.instance.SetStuff(stuffName, value, number);
    }
}
