﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomerController : MonoBehaviour
{
    public static CustomerController instance;
    public Customers[] customers;
    public Transform[] positions;
    public Button button;

    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        UpdateInformation();
    }
    public void UpdateInformation()
    {
        List<int> temp = new List<int>();
        temp.Add(GameManager.instance.first);
        temp.Add(GameManager.instance.second);
        temp.Add(GameManager.instance.third);
        bool flag = false;
        foreach (var item in temp)
        {
            if (item != 0)
            {
                flag = true;
                break;
            }
        }
        if (!flag)
        {
            button.GetComponentInChildren<Text>().text = "Error";
            button.interactable = false;
        }
        else 
        {
            button.interactable = true;
            if (customers[0].willingType == 0)
            {
                button.GetComponentInChildren<Text>().text = "vp";
            }
            else if (customers[0].willingType == 1)
            {
                button.GetComponentInChildren<Text>().text = "rp";
            }
            else if (customers[0].willingType == 2)
            {
                button.GetComponentInChildren<Text>().text = "yp";
            }
        }
    }
    public void NewCustomer()
    {
        for (int i = 0; i < customers.Length - 1; i++)
        {
            customers[i].type = customers[i + 1].type;
            customers[i].willingType = customers[i + 1].willingType;
            customers[i].SpriteUpdate();
        }
        customers[customers.Length - 1].type = Random.Range(0, customers[customers.Length - 1].mask.Length);
        customers[customers.Length - 1].willingType = Random.Range(0, 3);
        customers[customers.Length - 1].SpriteUpdate();
        if (customers[0].willingType == 0)
        {
            button.GetComponentInChildren<Text>().text = "vp";
        }
        else if (customers[0].willingType == 1)
        {
            button.GetComponentInChildren<Text>().text = "rp";
        }
        else if (customers[0].willingType == 2)
        {
            button.GetComponentInChildren<Text>().text = "yp";
        }
        foreach (var item in customers)
        {
            item.UpdateInformation();
        }
    }
    public void ButtonHandler()
    {
        if (customers[0].willingType == 0 && GameManager.instance.vp > 0)
        {
            GameManager.instance.vp -= 1;
            GameManager.instance.value += 10;
            GameManager.instance.TopPanelAnimation(10);
            NewCustomer();
        }
        if (customers[0].willingType == 1 && GameManager.instance.rp > 0)
        {
            GameManager.instance.rp -= 1;
            GameManager.instance.value += 10;
            GameManager.instance.TopPanelAnimation(10);
            NewCustomer();
        }
        if (customers[0].willingType == 2 && GameManager.instance.yp > 0)
        {
            GameManager.instance.yp -= 1;
            GameManager.instance.value += 10;
            GameManager.instance.TopPanelAnimation(10);
            NewCustomer();
        }
        foreach (var item in FindObjectsOfType<StorageElement>())
        {
            item.SetInformation();
        }
        GameManager.instance.UpdateTopPanel();
        SQL.UpdateInformation("users", $"value = {GameManager.instance.value}", $"login = '{GameManager.instance.login}'");
    }
}
