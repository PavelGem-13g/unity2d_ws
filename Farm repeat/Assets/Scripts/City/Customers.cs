﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Customers : MonoBehaviour
{
    public Transform targetTransform;
    public int type;
    public int willingType;
    public GameObject[] mask;
    private void Start()
    {
        transform.position = CustomerController.instance.positions[Convert.ToInt32(name)].position;
        GetCustomerInformation();
        SpriteUpdate();
    }
    void GetCustomerInformation()
    {
        List<string> temp = SQL.GetCustomerInformation(GameManager.instance.login, Convert.ToInt32(name));
        type = Convert.ToInt32(temp[0]);
        willingType = Convert.ToInt32(temp[1]);
    }
    public void SpriteUpdate()
    {
        foreach (var item in mask)
        {
            item.SetActive(false);
        }
        mask[type].SetActive(true);
    }
    public void UpdateInformation() 
    {
        SQL.UpdateInformation("customers",$"type = {type}",$"login = '{GameManager.instance.login}' AND number = {Convert.ToInt32(name)}");
        SQL.UpdateInformation("customers",$"willingtype = {willingType}",$"login = '{GameManager.instance.login}' AND number = {Convert.ToInt32(name)}");
    }
    IEnumerator ToEndPosion(Vector3 endPosition)
    {
        while (Vector3.Distance(transform.position, endPosition) > 0f)
        {
            transform.position = Vector3.MoveTowards(transform.position, endPosition, 0.1f);
            yield return new WaitForEndOfFrame();
        }
    }
}
