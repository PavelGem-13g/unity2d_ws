﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StuffViewer : MonoBehaviour
{
    public Sprite[] sprites;
    public int number;
    public GameObject error;
    private void Awake()
    {
        UpdateInfomation();
    }
    public void UpdateInfomation() 
    {
        List<int> temp = new List<int>();
        temp.Add(GameManager.instance.first);
        temp.Add(GameManager.instance.second);
        temp.Add(GameManager.instance.third);
        Image image = GetComponent<Image>();
        if (temp[number] != 0)
        {
            image.sprite = sprites[temp[number]];
            image.SetNativeSize();
            error.SetActive(false);
        }
        else 
        {
            error.SetActive(true);
        }    
    }
}
