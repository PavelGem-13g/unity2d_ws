﻿using System.Collections;
using UnityEngine;

public class Figure : MonoBehaviour
{
    public Cell[] cells;
    public Vector3 startPosition;
    private void Awake()
    {
        startPosition = transform.position;
    }
    private void OnMouseDrag()
    {
        if (!Controller.isEnd)
        {
            transform.localScale = new Vector3(1.1f, 1.1f, 1.1f);
            transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition) + new Vector3(0, 0, 10);
        }
    }

    private void OnMouseUp()
    {
        transform.localScale = Vector3.one;
        if (IsOnGround() && !Controller.isEnd)
        {
            Suicide();
        }
        else
        {
            StartCoroutine(ToStartPosition());
        }
    }
    public bool IsOnGround()
    {
        foreach (var item in cells)
        {
            if (!item.IsOnGround())
            {
                return false;
            }
        }
        return true;
    }
    void Suicide()
    {
        Controller.instance.countOfCells -= 1;
        if (Controller.instance.countOfCells == 0)
        {
            Controller.instance.CreateNewCells();
        }
        foreach (var item in cells)
        {
            item.ChangeParents();
        }
        Controller.instance.Clear();
        Destroy(gameObject);
    }
    IEnumerator ToStartPosition()
    {
        while (Vector3.Distance(transform.position, startPosition) > 0)
        {
            transform.position = Vector3.MoveTowards(transform.position, startPosition, 0.5f);
            yield return new WaitForSeconds(0.01f);
        }
        Controller.instance.IsGameOver();
    }
}
