﻿using UnityEngine;

public class Controller : MonoBehaviour
{
    public static Controller instance;

    public GridCell[,] gridCells;

    public GameObject gridCellPrefab;

    public int countOfCells;
    public GameObject[] figurePrefabs;
    public GameObject[] positions;

    public static bool isEnd;

    private void Awake()
    {
        instance = this;
        isEnd = false;
        gridCells = new GridCell[10, 10];
        for (int i = 0; i < 10; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                gridCells[i, j] = Instantiate(gridCellPrefab, new Vector3(i, j), Quaternion.identity, transform).GetComponent<GridCell>();
            }
        }
        CreateNewCells();
    }
    public void CreateNewCells()
    {
        countOfCells = 3;
        for (int i = 0; i < countOfCells; i++)
        {
            Instantiate(figurePrefabs[Random.Range(0, figurePrefabs.Length)], positions[i].transform.position, Quaternion.identity, transform.parent);
        }
    }
    public void Clear()
    {
        for (int i = 0; i < 10; i++)
        {
            int k = 0;
            for (int j = 0; j < 10; j++)
            {
                if (gridCells[i, j].cell)
                {
                    k++;
                }
            }
            if (k == 10)
            {
                for (int j = 0; j < 10; j++)
                {
                    StartCoroutine(gridCells[i, j].cell.Destroy());
                    gridCells[i, j].cell = null;
                }
            }
        }
        for (int i = 0; i < 10; i++)
        {
            int k = 0;
            for (int j = 0; j < 10; j++)
            {
                if (gridCells[j, i].cell)
                {
                    k++;
                }
            }
            if (k == 10)
            {
                for (int j = 0; j < 10; j++)
                {
                    StartCoroutine(gridCells[j, i].cell.Destroy());
                    gridCells[i, j].cell = null;
                }
            }
        }
    }
    public bool IsGameOver()
    {
        Figure[] figures = FindObjectsOfType<Figure>();
        foreach (var item in figures)
        {
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    item.transform.position = new Vector3(i, j);
                    if (item.IsOnGround())
                    {
                        item.transform.position = item.startPosition;
                        return false;
                    }
                }
            }
            item.transform.position = item.startPosition;
        }
        isEnd = true;
        return true;
    }
}
