﻿using System.Collections;
using UnityEngine;

public class Grid : MonoBehaviour
{
    public GameObject prefab;
    public GridCell[,] gridCell;

    public GameObject cellPrefab;
    private void Awake()
    {
        gridCell = new GridCell[7, 7];
    }
    void Start()
    {
        for (int i = 0; i < 7; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                GameObject temp = Instantiate(prefab, new Vector3(i, j) + transform.position, Quaternion.identity, transform);
                temp.GetComponent<SpriteRenderer>().sortingOrder = -1;
                gridCell[i, j] = temp.GetComponent<GridCell>();
            }
        }
        for (int i = 0; i < 7; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                if (i == 0 || i == 6 || j == 0 || j == 6)
                {
                    gridCell[i, j].gameObject.SetActive(false);
                }
            }
        }
        CreateNewCell();
    }

    public void CreateNewCell()
    {
        if (Check())
        {
            int i = Random.Range(2, 6), j = Random.Range(2, 6);
            while (gridCell[i, j].cell != null)
            {
                i = Random.Range(0, 5);
                j = Random.Range(0, 5);
            }
            GameObject temp = Instantiate(cellPrefab, gridCell[i, j].transform);
            gridCell[i, j].cell = temp;
            temp.GetComponent<SpriteRenderer>().sortingOrder = 0;
        }
    }
    public bool Check()
    {
        int k = 0;
        foreach (var item in gridCell)
        {
            if (!item.cell)
            {
                k++;
            }
        }
        if (k != 0)
        {
            return true;
        }
        else
        {
            Debug.Log("Game over");
            return false;
        }
    }
    public void ToRight()
    {
        StartCoroutine(ToRightPosition());
    }
    IEnumerator ToRightPosition()
    {
        for (int k = 0; k < 5; k++)
        {
            for (int i = 1; i < 6; i++)
            {
                for (int j = 1; j < 6; j++)
                {
                    int _i = i, _j = j;
                    if (gridCell[_i, _j].cell != null)
                    {
                        while (_i != 6 && _i + 1 != 6)
                        {
                            if (gridCell[_i + 1, _j].cell == null && gridCell[_i, _j].cell != null)
                            {
                                gridCell[_i, _j].cell.transform.parent = gridCell[_i + 1, _j].transform;
                                gridCell[_i + 1, _j].cell = gridCell[_i, _j].cell;
                                gridCell[_i + 1, _j].cell.transform.localPosition = Vector3.zero;
                                gridCell[_i, _j].cell = null;
                            }
                            if (gridCell[_i + 1, _j].cell && gridCell[_i, _j].cell)
                            {
                                if (gridCell[_i + 1, _j].cell.GetComponent<Cell>().value == gridCell[_i, _j].cell.GetComponent<Cell>().value)
                                {
                                    gridCell[_i + 1, _j].cell.GetComponent<Cell>().CellUpate();
                                    Destroy(gridCell[_i, _j].cell);
                                    gridCell[_i, _j].cell = null;
                                }
                            }
                            _i += 1;
                            yield return new WaitForSeconds(0.0001f);
                        }
                    }
                }
            }
        }


    }
    public void ToLeft()
    {
        StartCoroutine(ToLeftPosition());
    }
    IEnumerator ToLeftPosition()
    {
        for (int k = 0; k < 5; k++)
        {
            for (int i = 1; i < 6; i++)
            {
                for (int j = 1; j < 6; j++)
                {
                    int _i = i, _j = j;
                    if (gridCell[_i, _j].cell != null)
                    {
                        while (_i != 0 && _i - 1 != 0)
                        {
                            if (gridCell[_i - 1, _j].cell == null && gridCell[_i, _j].cell != null)
                            {
                                gridCell[_i, _j].cell.transform.parent = gridCell[_i - 1, _j].transform;
                                gridCell[_i - 1, _j].cell = gridCell[_i, _j].cell;
                                gridCell[_i - 1, _j].cell.transform.localPosition = Vector3.zero;
                                gridCell[_i, _j].cell = null;
                            }
                            if (gridCell[_i - 1, _j].cell && gridCell[_i, _j].cell)
                            {
                                if (gridCell[_i - 1, _j].cell.GetComponent<Cell>().value == gridCell[_i, _j].cell.GetComponent<Cell>().value)
                                {
                                    gridCell[_i - 1, _j].cell.GetComponent<Cell>().CellUpate();
                                    Destroy(gridCell[_i, _j].cell);
                                    gridCell[_i, _j].cell = null;
                                }
                            }
                            _i -= 1;
                            yield return new WaitForSeconds(0.0001f);
                        }
                    }
                }
            }
        }
    }
    public void ToTop()
    {
        StartCoroutine(ToTopPosition());
    }
    IEnumerator ToTopPosition()
    {
        for (int k = 0; k < 5; k++)
        {
            for (int i = 1; i < 6; i++)
            {
                for (int j = 1; j < 6; j++)
                {
                    int _i = i, _j = j;
                    if (gridCell[_i, _j].cell != null)
                    {
                        while (_j != 6 && _j + 1 != 6)
                        {
                            if (gridCell[_i, _j + 1].cell == null && gridCell[_i, _j].cell != null)
                            {
                                gridCell[_i, _j].cell.transform.parent = gridCell[_i, _j + 1].transform;
                                gridCell[_i, _j + 1].cell = gridCell[_i, _j].cell;
                                gridCell[_i, _j + 1].cell.transform.localPosition = Vector3.zero;
                                gridCell[_i, _j].cell = null;
                            }
                            if (gridCell[_i, _j + 1].cell && gridCell[_i, _j].cell)
                            {
                                if (gridCell[_i, _j + 1].cell.GetComponent<Cell>().value == gridCell[_i, _j].cell.GetComponent<Cell>().value)
                                {
                                    gridCell[_i, _j + 1].cell.GetComponent<Cell>().CellUpate();
                                    Destroy(gridCell[_i, _j].cell);
                                    gridCell[_i, _j].cell = null;
                                }
                            }
                            _j += 1;
                            yield return new WaitForSeconds(0.0001f);
                        }
                    }
                }
            }
        }

    }
    public void ToDown()
    {
        StartCoroutine(ToDownPosition());
    }
    IEnumerator ToDownPosition()
    {
        for (int k = 0; k < 5; k++)
        {
            for (int i = 1; i < 6; i++)
            {
                for (int j = 1; j < 6; j++)
                {
                    int _i = i, _j = j;
                    if (gridCell[_i, _j].cell != null)
                    {
                        while (_j != 0 && _j - 1 != 0)
                        {
                            if (gridCell[_i, _j - 1].cell == null && gridCell[_i, _j].cell != null)
                            {
                                gridCell[_i, _j].cell.transform.parent = gridCell[_i, _j - 1].transform;
                                gridCell[_i, _j - 1].cell = gridCell[_i, _j].cell;
                                gridCell[_i, _j - 1].cell.transform.localPosition = Vector3.zero;
                                gridCell[_i, _j].cell = null;
                            }
                            if (gridCell[_i, _j - 1].cell && gridCell[_i, _j].cell)
                            {
                                if (gridCell[_i, _j - 1].cell.GetComponent<Cell>().value == gridCell[_i, _j].cell.GetComponent<Cell>().value)
                                {
                                    gridCell[_i, _j - 1].cell.GetComponent<Cell>().CellUpate();
                                    Destroy(gridCell[_i, _j].cell);
                                    gridCell[_i, _j].cell = null;
                                }
                            }
                            _j -= 1;
                            yield return new WaitForSeconds(0.0001f);
                        }
                    }
                }
            }
        }
    }
}

