﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell : MonoBehaviour
{
    public int value = 0;

    public Sprite[] spriteRenderers;

    private void Start()
    {
        value = Random.Range(0,3); 
        GetComponent<SpriteRenderer>().sprite = spriteRenderers[value];
    }

    public void CellUpate() 
    {
        value += 1;
        GetComponent<SpriteRenderer>().sprite = spriteRenderers[value];
        PlayerPrefs.SetInt("score", PlayerPrefs.GetInt("score") + System.Convert.ToInt32(Mathf.Pow(2,value)));
        foreach (var item in FindObjectsOfType<TextUpdate>())
        {
            item.ScoreTextUpdate();
        }
    }
}
