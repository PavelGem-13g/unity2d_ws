﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextUpdate : MonoBehaviour
{
    Text text;
    public string prefsName;
    public string paramtrName;
    private void Awake()
    {
        text = GetComponent<Text>();
    }

    private void Start()
    {
        PlayerPrefs.SetInt("score", 0);
        ScoreTextUpdate();
    }

    public void ScoreTextUpdate()
    {
        if (PlayerPrefs.GetInt("best") < PlayerPrefs.GetInt("score")) 
        {
            PlayerPrefs.SetInt("best", PlayerPrefs.GetInt("score"));
        }
        text.text = paramtrName + " " + PlayerPrefs.GetInt(prefsName);
    }
}
