﻿using UnityEngine;

public class Swipe : MonoBehaviour
{
    //Touch touch;
    Grid grid;
    Vector2 firstPosition;
    Vector2 secondPosition;
    Vector2 swipe;

    private void Awake()
    {
        grid = FindObjectOfType<Grid>();
        firstPosition = Vector2.zero;
        secondPosition = Vector2.zero;
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            firstPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        }
        if (Input.GetMouseButtonUp(0))
        {
            secondPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            Check();
        }
    }
    void Check()
    {
        swipe = secondPosition - firstPosition;
        if (Mathf.Abs(swipe.x) > Mathf.Abs(swipe.y))
        {
            if (swipe.x>0) 
            {
                grid.ToRight();
            }
            if(swipe.x<0)
            {
                grid.ToLeft();
            }
        }
        else 
        {
            if (swipe.y > 0)
            {
                grid.ToTop();
            }
            if(swipe.y<0)
            {
                grid.ToDown();
            }
        }
        grid.CreateNewCell();
    }
}
